#!/bin/bash

function log() {
    local -r message="$1"
    echo "$message" >> $TEST_RESULTS_LOG
}

function teel() {
    local -r message="$1"
    log "$message"
    echo "$message"
}

function die() {
    local -r message="$1"
    teel "$message"
    exit 100
}

function teelingPrompt() {
    local -r prompt="$1"
    local input
    read -p "$prompt" input
    log "$prompt $input"
    echo $input
}

function generateLogFileName() {
    local -r testDir="test_logs"
    mkdir -p $testDir
    echo "$testDir/testResults_$(date +%Y%m%d_%H%M%S).log"
}

function isValidResult() {
    local isValid=0
    if [[ "$acResult" == "T" || "$acResult" == "P" || "$acResult" == "F" ]]; then
        isValid=1
    fi
    echo $isValid
}

function isNotationResult() {
    local isNotation=0
    if [[ "$acResult" == "T" || "$acResult" == "F" ]]; then
        isNotation=1
    fi
    echo $isNotation
}

function showSeparator() {
    teel "================================================================================"
}

function checkTestControllerExists() {
    local -r testName="$1"
    $TEST_PROGRAM $testName check_only > /dev/null 2>&1
    if [[ $? -eq 2 ]]; then
        die "$testName does not have a controller, exiting"
    fi
}

function collectConditionResult() {
    local -r acText="$1"
    local acResult=""
    while [[ $(isValidResult "$acResult") -eq 0 ]]; do
        local testNumber=$(printf %03d $acIndex)
        acResult=$(teelingPrompt "[$testNumber] $acText [P] > ")

        if [[ "$acResult" == "" ]]; then
            acResult="P"
        fi

        if [[ $(isValidResult "$acResult") -eq 0 ]]; then
            teel "Invalid result '$acResult'"
        fi
    done

    if [[ $(isNotationResult "$acResult") -eq 1 ]]; then
        teelingPrompt "Notation> " > /dev/null
    fi
}

function readTestCases() {
    echo "$(cat $TEST_DATA_FILE)"
}

function runTest() {
    local -r testData="$1"
    local testName=$(echo "$testData" | $JQ -r ".testName")

    checkTestControllerExists "$testName"

    if [[ $SKIP_TESTS -ne 1 ]]; then
        local instructions=$(echo "$testData" | $JQ -r ".instructions")
    fi

    teel "Test Name: $testName"

    if [[ $SKIP_TESTS -ne 1 ]]; then
        teel "$(echo "Instructions: $instructions" | fold -w 80 -s)"
    fi

    teelingPrompt "Press Enter to Run Test Program"

    $TEST_PROGRAM "$testName" > /dev/null 2>&1 &
    local testProgramPid=$!

    if [[ $SKIP_TESTS -ne 1 ]]; then
        local conditions=$(echo "$testData" | $JQ -r ".acceptanceConditions")
        local acCount=$(echo "$conditions" | $JQ -r ". | length")

        local acIndex
        for ((acIndex=0; acIndex < $acCount; acIndex++)); do
            local acText=$(echo "$conditions" | $JQ -r ".[$acIndex]")
            collectConditionResult "$acText"
        done
    fi

    teel "Please close the test application in order to continue"
    teel "Waiting..."
    wait $testProgramPid

    if [[ $? -ne 0 ]]; then
        die "$TEST_PROGRAM failed for $testName"
    fi
    showSeparator
}

function runAllTests() {
    local -r allTestData=$(readTestCases)
    local -r testCount=$(echo "$allTestData" | $JQ -r '. | length')
    local testIndex
    for ((testIndex=0; testIndex < $testCount; testIndex++)); do
        local testData=$(echo "$allTestData" | $JQ -r ".[$testIndex]")
        runTest "$testData"
        clear
    done
}

function runSingleTestByName() {
    local -r testName="$1"
    local -r allTestData=$(readTestCases)
    local -r matchingTestData=$(echo "$allTestData" | jq "map(select(.testName == \"$testName\"))")
    local -r testDataCount=$(echo "$matchingTestData" | jq "length")
    local -r testData=$(echo "$matchingTestData" | jq ".[0]")
    if [[ $testDataCount -ne 1 ]]; then
        die "Provided test case name was not declared in test case data"
    else
        runTest "$testData"
    fi
}

function checkPreconditionJq() {
    jq --help > /dev/null 2>&1
    if [[ $? -eq 127 ]]; then
        teel "$JQ must be in the path"
        teel ""
        showUsage 5
    fi
}

function showUsage() {
    local -r returnCode="$1"
    if [[ $ARG_COUNT -ne 2 ]]; then
        teel "USAGE: test_driver.sh [options]"
        teel "   -c required path test case data (i.e. tests.json)"
        teel "   -e required path testing executable (i.e. uitk_test)"
        teel "   -t optional name of specific test to run from test case data"
        teel "   -s just run each demo program, skip tests"
        teel "   -h display usage"
        teel ""
        teel "Additionally, this program requires 'jq' to be in the path"
    fi

    exit $returnCode
}

function checkPreconditionTestProgram() {
    if [[ "$TEST_PROGRAM" == "" ]]; then
        teel "Path to test program was not provided"
        teel ""
        showUsage 3
    fi

    $TEST_PROGRAM > /dev/null 2>&1
    if [[ $? -eq 127 ]]; then
        teel "Path to test program provided, but could not be found at path"
        teel ""
        showUsage 4
    fi
}

function checkPreconditionTestData() {
    if [[ ! -e "$TEST_DATA_FILE" ]]; then
        teel "Error: Test case data either not provided or does not exist at path"
        teel ""
        showUsage 2
    fi
}

function showInstructionsPrompt() {
    teel "UITK test_driver.sh"
    teel ""
    teel "Test programs will be loaded in order from $TEST_DATA_FILE"
    teel ""
    teel "For each program, test conditions will be presented to the user."
    teel "Each condition, will accept one of the following inputs:"
    teel ""
    teel "     'P' - Pass the test"
    teel "     'F' - Fail the test"
    teel "     'T' - Test case inaccurate"
    teel ""
    teel "If either 'P' or 'T', then a notation will be collected to capture why."
    teel "At any time, Ctrl+C may be used to exit the test suite."
    teel "All results will be collected in $TEST_RESULTS_LOG."
    teelingPrompt "Press Enter to Continue"
    showSeparator
}

declare -r TEST_RESULTS_LOG=$(generateLogFileName)

declare -r JQ="jq"
checkPreconditionJq

while getopts ':c:e:t:hs' opt; do
    case ${opt} in
        c ) declare -r TEST_DATA_FILE="$OPTARG"
            ;;
        e ) declare -r TEST_PROGRAM="$OPTARG"
            ;;
        t ) declare -r SINGLE_RUN_TEST="$OPTARG"
            ;;
        s ) declare -r SKIP_TESTS=1
            ;;
        h ) showUsage 0
            ;;
        \?) showUsage 1
            ;;
    esac
done

checkPreconditionTestProgram
checkPreconditionTestData

if [[ $SKIP_TESTS -ne 1 ]]; then
    clear
    showInstructionsPrompt
fi

clear
if [[ $SINGLE_RUN_TEST == "" ]]; then
    runAllTests
else
    runSingleTestByName "$SINGLE_RUN_TEST"
fi
