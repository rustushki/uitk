#include "uitk/type/Layout.hpp"

/**
 * Get the padding settings for the layout.
 * @return padding
 */
Padding& Layout::getPadding() {
    return padding;
}

/**
 * Get the padding settings for the layout. Const friendly.
 * @return padding
 */
const Padding& Layout::getPadding() const {
    return padding;
}

/**
 * Get the scale behavior for the layout.
 * @return scale behavior
 */
ScaleBehavior& Layout::getScaleBehavior() {
    return scaleBehavior;
}

/**
 * Get the scale behavior for the layout. Const friendly.
 * @return scale behavior
 */
const ScaleBehavior& Layout::getScaleBehavior() const {
    return scaleBehavior;
}

/**
 * Get the external alignment for the layout.
 * @return external alignment
 */
Alignment& Layout::getExternalAlignment() {
    return externalAlignment;
}

/**
 * Get the external alignment for the layout. Const friendly.
 * @return external alignment
 */
const Alignment& Layout::getExternalAlignment() const {
    return externalAlignment;
}

/**
 * Get the arrangement data for the layout.
 * @return arrangement data
 */
ArrangementData& Layout::getArrangementData() {
    return arrangementData;
}

/**
 * Get the arrangement data for the layout. Const friendly.
 * @return arrangement data
 */
const ArrangementData& Layout::getArrangementData() const {
    return arrangementData;
}

/**
 * Set the padding settings for the layout.
 * @param padding
 */
void Layout::setPadding(Padding padding) {
    this->padding = padding;
}

/**
 * Set the scale behavior for the layout.
 * @param scale behavior
 */
void Layout::setScaleBehavior(ScaleBehavior scaleBehavior) {
    this->scaleBehavior = scaleBehavior;
}

/**
 * Set the extenal alignment for the layout.
 * @param extenal alignment
 */
void Layout::setExternalAlignment(Alignment externalAlignment) {
    this->externalAlignment = externalAlignment;
}

/**
 * Set the arrangement data for the layout.
 * @param arrangement data
 */
void Layout::setArrangementData(ArrangementData arrangementData) {
    this->arrangementData = arrangementData;
}

/**
 * Determine if the provided layout is compositionally the same as this one.
 * @param that the other alignment
 * @return true if their equal
 */
bool Layout::isEqual(const Layout& that) const {
    if (this->scaleBehavior != that.scaleBehavior) {
        return false;
    }

    if (this->externalAlignment != that.externalAlignment) {
        return false;
    }

    if (this->padding != that.padding) {
        return false;
    }

    if (this->arrangementData != that.arrangementData) {
        return false;
    }

    return true;
}

/**
 * Overloaded equality operator which calls isEqual.
 * @param that the other one
 * @return see isEqual
 */
bool Layout::operator==(const Layout& that) const {
    return isEqual(that);
}

/**
 * Overloaded inequality operator which calls !isEqual.
 * @param that the other one
 * @return see isEqual
 */
bool Layout::operator!=(const Layout& that) const {
    return !isEqual(that);
}

