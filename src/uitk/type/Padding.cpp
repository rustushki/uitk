#include "uitk/type/Padding.hpp"

/**
 * Build a padding with equivalent values for top, left, right and bottom dimensions.
 * @param padding the value to set for all dimensions
 */
Padding::Padding(Measure padding) : Padding(padding, padding, padding, padding) {}

/**
 * Build a padding with specific values for top, left, right and bottom dimensions.
 * @param top the value for the top padding
 * @param right the value for the right padding
 * @param bottom the value for the bottom padding
 * @param left the value for the left padding
 */
Padding::Padding(Measure top, Measure right, Measure bottom, Measure left) {
    this->setTop(top);
    this->setRight(right);
    this->setBottom(bottom);
    this->setLeft(left);
}

/**
 * Get the top padding.
 * @return Measure
 */
Measure Padding::getTop() const {
    return top;
}

/**
 * Get the bottom padding.
 * @return Measure
 */
Measure Padding::getBottom() const {
    return bottom;
}

/**
 * Get the left padding.
 * @return Measure
 */
Measure Padding::getLeft() const {
    return left;
}

/**
 * Get the right padding.
 * @return Measure
 */
Measure Padding::getRight() const {
    return right;
}

/**
 * Set the top padding.
 * @param measure
 */
void Padding::setTop(Measure measure) {
    this->top = measure;
}

/**
 * Set the bottom padding.
 * @param measure
 */
void Padding::setBottom(Measure measure) {
    this->bottom = measure;
}

/**
 * Set the left padding.
 * @param measure
 */
void Padding::setLeft(Measure measure) {
    this->left = measure;
}

/**
 * Set the right padding.
 * @param measure
 */
void Padding::setRight(Measure measure) {
    this->right = measure;
}

/**
 * Determine if the provided padding is compositionally the same as this one.
 * @param that the other alignment
 * @return true if their equal
 */
bool Padding::isEqual(const Padding& that) const {
    if (this->left != that.left) {
        return false;
    }

    if (this->top != that.top) {
        return false;
    }

    if (this->right != that.right) {
        return false;
    }

    if (this->bottom != that.bottom) {
        return false;
    }

    return true;
}

/**
 * Overloaded equality operator which calls isEqual.
 * @param that the other one
 * @return see isEqual
 */
bool Padding::operator==(const Padding& that) const {
    return isEqual(that);
}

/**
 * Overloaded inequality operator which calls !isEqual.
 * @param that the other one
 * @return see isEqual
 */
bool Padding::operator!=(const Padding& that) const {
    return !isEqual(that);
}
