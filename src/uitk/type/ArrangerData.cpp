#include "uitk/type/ArrangerData.hpp"

/**
 * Get the number of horizontal slots in the grid.
 *
 * Applies to GridArranger only.
 * @return grid width
 */
int ArrangerData::getGridWidth() const {
    return gridWidth;
}

/**
 * Get the number of vertical slots in the grid.
 *
 * Applies to GridArranger only.
 * @return grid height
 */
int ArrangerData::getGridHeight() const {
    return gridHeight;
}

/**
 * Set the number of horizontal slots in the grid.
 *
 * Applies to GridArranger only.
 * @param gridWidth
 */
void ArrangerData::setGridWidth(int gridWidth) {
    this->gridWidth = gridWidth;
}

/**
 * Set the number of vertical slots in the grid.
 *
 * Applies to GridArranger only.
 * @param gridHeight
 */
void ArrangerData::setGridHeight(int gridHeight) {
    this->gridHeight = gridHeight;
}


