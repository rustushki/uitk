#include <cmath>
#include "uitk/type/Measure.hpp"
#include "uitk/utility/factory/MeasureFactory.hpp"
#include "uitk/utility/factory/UnitFactory.hpp"

/**
 * Constructor.
 * @param value distance in pixels
 */
Measure::Measure(double value) : value(value) {}

/**
 * Constructor
 * @param value distance in provided unit
 * @param unit the unit of the value
 */
Measure::Measure(double value, Unit unit) : value(value), unit(unit) {}

/**
 * Get the distance of the measure.
 * @return distance
 */
double Measure::getValue() const {
    return value;
}

/**
 * Set the distance of the measure.
 * @param value distance
 */
void Measure::setValue(double value) {
    this->value = value;
}

/**
 * Get the unit of the measure.
 * @return unit of the measure
 */
Unit Measure::getUnit() const {
    return unit;
}

/**
 * Set the unit of the measure.
 * @param unit unit of the measure
 */
void Measure::setUnit(Unit unit) {
    this->unit = unit;
}

/**
 * Assign the distance (as double) to the value of this measure.
 * @param rhs distance
 */
void Measure::operator=(double rhs) {
    this->value = rhs;
}

/**
 * Subtract the distance (as double) from the value of this measure, returning a new measure.
 * @param rhs distance
 */
Measure Measure::operator-(double rhs) {
    return Measure(value - rhs);
}

/**
 * Add the distance (as double) from the value of this measure, returning a new measure.
 * @param rhs distance
 */
Measure Measure::operator+(double rhs) {
    return Measure(value + rhs);
}

/**
 * Divide the distance (as double) into the value of this measure, returning a new measure.
 * @param rhs distance
 */
Measure Measure::operator/(double rhs) {
    return Measure(value / rhs);
}

/**
 * Multiply the distance (as double) to the value of this measure, returning a new measure.
 * @param rhs distance
 */
Measure Measure::operator*(double rhs) {
    return Measure(value * rhs);
}

/**
 * Subtract the distance (as double) from the value of this measure.
 * @param rhs distance
 */
void Measure::operator-=(double rhs) {
    this->value -= rhs;
}

/**
 * Add the distance (as double) to the value of this measure.
 * @param rhs distance
 */
void Measure::operator+=(double rhs) {
    this->value += rhs;
}

/**
 * Divide the distance (as double) into the value of this measure.
 * @param rhs distance
 */
void Measure::operator/=(double rhs) {
    this->value /= rhs;
}

/**
 * Multiply the distance (as double) to the value of this measure.
 * @param rhs distance
 */
void Measure::operator*=(double rhs) {
    this->value *= rhs;
}

/**
 * Parse a string into a Measure as an assignment.
 * See MeasureFactory for details on string format.
 * @param rhs the string
 */
void Measure::operator=(const std::string& rhs) {
    MeasureFactory measureFactory;
    Measure measure = measureFactory.build(rhs);
    setValue(measure.getValue());
    setUnit(measure.getUnit());
}

/**
 * Return true if the right-hand side measure is equal to this one.
 * Unit and distance must be the same.
 * @param that the right hand side
 */
bool Measure::operator==(const Measure& that) const {
    return isEqual(that);
}

/**
 * Return true if the right-hand side measure is not equal to this one.
 * Unit and distance must be the same.
 * @param that the right hand side
 */
bool Measure::operator!=(const Measure& that) const {
    return !isEqual(that);
}

/**
 * Return true if the provided measure is equal to this one.
 * Unit and distance must be the same.
 * @param that the right hand side
 */
bool Measure::isEqual(const Measure& that) const {
    return that.getValue() == getValue() && that.getUnit() == getUnit();
}
