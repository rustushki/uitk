#include "uitk/type/Alignment.hpp"

/**
 * Return horizontal alignment setting.
 * @return horizontal alignment
 */
HorizontalAlignment Alignment::getHorizontal() const {
    return horizontal;
}

/**
 * Return vertical alignment setting.
 * @return vertical alignment
 */
VerticalAlignment Alignment::getVertical() const {
    return vertical;
}

/**
 * Set horizontal alignment setting.
 * @param horizontal horizontal alignment
 */
void Alignment::setHorizontal(HorizontalAlignment horizontal) {
    this->horizontal = horizontal;
}

/**
 * Set vertical alignment setting.
 * @param vertical vertical alignment
 */
void Alignment::setVertical(VerticalAlignment vertical) {
    this->vertical = vertical;
}

/**
 * Determine if the provided alignment is compositionally the same as this one.
 * @param that the other alignment
 * @return true if their equal
 */
bool Alignment::isEqual(const Alignment& that) const {
    if (this->vertical != that.vertical) {
        return false;
    }

    if (this->horizontal != that.horizontal) {
        return false;
    }

    return true;
}

/**
 * Overloaded equality operator which calls isEqual.
 * @param that the other one
 * @return see isEqual
 */
bool Alignment::operator==(const Alignment& that) const {
    return isEqual(that);
}

/**
 * Overloaded inequality operator which calls !isEqual.
 * @param that the other one
 * @return see isEqual
 */
bool Alignment::operator!=(const Alignment& that) const {
    return !isEqual(that);
}
