#include "uitk/type/ArrangementData.hpp"

/**
 * X value of placement of the Widget.
 *
 * Used by Absolute Arrangement.
 * @return int
 */
int ArrangementData::getX() const {
    return x;
}

/**
 * Y value of placement of the Widget.
 *
 * Used by Absolute Arrangement.
 * @return int
 */
int ArrangementData::getY() const {
    return y;
}

/**
 * Width of the placement of the Widget.
 *
 * Used by Absolute Arrangement.
 * @return int
 */
int ArrangementData::getWidth() const {
    return width;
}

/**
 * Height of the placement of the Widget.
 *
 * Used by Absolute Arrangement.
 * @return int
 */
int ArrangementData::getHeight() const {
    return height;
}

/**
 * Set the X placement of the widget.
 *
 * Used by Absolute Arrangement.
 */
void ArrangementData::setX(int x) {
    this->x = x;
}

/**
 * Set the Y placement of the widget.
 *
 * Used by Absolute Arrangement.
 */
void ArrangementData::setY(int y) {
    this->y = y;
}

/**
 * Set the Width placement of the widget.
 *
 * Used by Absolute Arrangement.
 */
void ArrangementData::setWidth(int width) {
    this->width = width;
}

/**
 * Set the Height placement of the widget.
 *
 * Used by Absolute Arrangement.
 */
void ArrangementData::setHeight(int height) {
    this->height = height;
}

/**
 * Get the X position within a grid arrangement.
 *
 * Used by GridArranger.
 */
int ArrangementData::getGridX() const {
    return gridX;
}

/**
 * Get the Y position within a grid arrangement.
 *
 * Used by GridArranger.
 */
int ArrangementData::getGridY() const {
    return gridY;
}

/**
 * Set the X position within a grid arrangement.
 *
 * Used by GridArranger.
 */
void ArrangementData::setGridX(int gridX) {
    this->gridX = gridX;
}

/**
 * Set the Y position within a grid arrangement.
 *
 * Used by GridArranger.
 */
void ArrangementData::setGridY(int gridY) {
    this->gridY = gridY;
}

/**
 * Set the measure of space to fill within the parent widget's arranged space.
 *
 * VerticalArranger and HorizontalArranger treat this as vertical or horizontal measure (respectively).
 * @param fill measure of parent's arranged space the widget should occupy
 */
void ArrangementData::setFill(Measure fill) {
    this->fill = fill;
}

/**
 * Return the measure of space this is filled within the parent widget's arranged space.
 *
 * VerticalArranger and HorizontalArranger treat this as vertical or horizontal measure (respectively).
 * @return measure of parent's arranged space the widget should occupy
 */
Measure ArrangementData::getFill() const {
    return fill;
}

/**
 * Set the vertical alignment.
 *
 * Used by VerticalArranger to determine where within the arranged space a widget should be drawn.
 * @param verticalAlignment align the widget according to this value
 */
void ArrangementData::setVerticalAlignment(VerticalAlignment verticalAlignment) {
    this->verticalAlignment = verticalAlignment;
}

/**
 * Get the vertical alignment.
 *
 * Used by VerticalArranger to determine where within the arranged space a widget should be drawn.
 * @return align the widget according to this value
 */
VerticalAlignment ArrangementData::getVerticalAlignment() const {
    return verticalAlignment;
}

/**
 * Set the horizontal alignment.
 *
 * Used by HorizontalArranger to determine where within the arranged space a widget should be drawn.
 * @param horizontalAlignment align the widget according to this value
 */
void ArrangementData::setHorizontalAlignment(HorizontalAlignment horizontalAlignment) {
    this->horizontalAlignment = horizontalAlignment;
}

/**
 * Get the horizontal alignment.
 *
 * Used by HorizontalArranger to determine where within the arranged space a widget should be drawn.
 * @return align the widget according to this value
 */
HorizontalAlignment ArrangementData::getHorizontalAlignment() const {
    return horizontalAlignment;
}

/**
 * Get the maximum count of pixels that the child widget may occupy when the fill property is applied.
 *
 * A value of 0 means no limit.
 *
 * Used by VerticalArranger and HorizontalArranger treat this as vertical or horizontal space (respectively).
 * @return maximum fill of pixels
 */
int ArrangementData::getMaxFill() const {
    return maxFill;
}

/**
 * Set the maximum count of pixels that the child widget may occupy when the fill property is applied.
 * A value of 0 means no limit.
 *
 * Used by VerticalArranger and HorizontalArranger treat this as vertical or horizontal space (respectively).
 * @param maxFill minimum fill of pixels
 */
void ArrangementData::setMaxFill(int maxFill) {
    this->maxFill = maxFill;
}

/**
 * Get the minimum count of pixels that the child widget may occupy when the fill property is applied.
 *
 * A value of 0 means no limit.
 *
 * Used by VerticalArranger and HorizontalArranger treat this as vertical or horizontal space (respectively).
 * @return minimum fill of pixels
 */
int ArrangementData::getMinFill() const {
    return minFill;
}

/**
 * Set the minimum count of pixels that the child widget may occupy when the fill property is applied.
 *
 * A value of 0 means no limit.
 *
 * Used by VerticalArranger and HorizontalArranger treat this as vertical or horizontal space (respectively).
 * @param minFill minimum fill of pixels
 */
void ArrangementData::setMinFill(int minFill) {
    this->minFill = minFill;
}

/**
 * Determine if the provided arrangement data is compositionally the same as this one.
 * @param that the other alignment
 * @return true if their equal
 */
bool ArrangementData::isEqual(const ArrangementData& that) const {
    if (this->x != that.x) {
        return false;
    }

    if (this->y != that.y) {
        return false;
    }

    if (this->width != that.width) {
        return false;
    }

    if (this->height != that.height) {
        return false;
    }

    if (this->gridX != that.gridX) {
        return false;
    }

    if (this->gridY != that.gridY) {
        return false;
    }

    if (this->maxFill != that.maxFill) {
        return false;
    }

    if (this->minFill != that.minFill) {
        return false;
    }

    if (this->fill != that.fill) {
        return false;
    }

    if (this->verticalAlignment != that.verticalAlignment) {
        return false;
    }

    if (this->horizontalAlignment != that.horizontalAlignment) {
        return false;
    }

    return true;
}

/**
 * Overloaded equality operator which calls isEqual.
 * @param that the other one
 * @return see isEqual
 */
bool ArrangementData::operator==(const ArrangementData& that) const {
    return isEqual(that);
}

/**
 * Overloaded inequality operator which calls !isEqual.
 * @param that the other one
 * @return see isEqual
 */
bool ArrangementData::operator!=(const ArrangementData& that) const {
    return !isEqual(that);
}
