#include <SDL2/SDL_image.h>
#include "uitk/type/SpriteSheet.hpp"

/**
 * Constructor.
 * <p>
 * Given a filename, load a sprite sheet into memory with the assumed given frame size.
 * @param file path to sprite sheet
 */
SpriteSheet::SpriteSheet(const std::filesystem::path& file) {
    std::string path = file;
    this->surface = IMG_Load(path.c_str());
    if (this->surface == nullptr) {
        throw std::logic_error("Unable to load SpriteSheet. Path = " + path + ". Error = " + IMG_GetError());
    }
}

/**
 * Destructor.
 * <p>
 * Free renderers' textures and the source surface for this sprite sheet.
 */
SpriteSheet::~SpriteSheet() {
    for (auto pair : this->rendererToTextureMap) {
        SDL_DestroyTexture(pair.second);
    }
    SDL_FreeSurface(this->surface);
}

/**
 * Find the still within the sprite sheet denoted by x, y and draw it to the provided SDL_Rect on the screen.
 *
 * @param renderer used to draw to the back buffer
 * @param source where in the sprite sheet image to draw from
 * @param destination where on the renderer to draw to
 */
void SpriteSheet::drawStill(SDL_Renderer* renderer, SDL_Rect& source, SDL_Rect& destination) {
    if (rendererToTextureMap.find(renderer) == rendererToTextureMap.end()) {
        rendererToTextureMap[renderer] = SDL_CreateTextureFromSurface(renderer, this->surface);
    }

    SDL_Texture* texture = rendererToTextureMap[renderer];

    // Draw the given still of the sprite sheet to the back buffer
    SDL_RenderCopy(renderer, texture, &source, &destination);
}
