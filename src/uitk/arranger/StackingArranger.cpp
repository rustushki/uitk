#include "uitk/arranger/StackingArranger.hpp"

/**
 * Determine the placement of the child widget given the parent widget.
 *
 * Stacking arrangers give each child the same arranged space so that they stack atop each other.
 * @param parent composite
 * @param child to arrange
 * @return SDL_Rect
 */
SDL_Rect StackingArranger::determineChildPlacement(Composite* parent, Widget* child) const {
	return parent->getContentSpace();
}

/**
 * Retrieve the type of arranger.
 *
 * See ArrangerFactory for builtin type constants.
 * @return arranger type
 */
std::string StackingArranger::getType() {
    return "stacking";
}
