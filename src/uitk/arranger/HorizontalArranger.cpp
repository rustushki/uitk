#include "uitk/arranger/HorizontalArranger.hpp"
#include "uitk/type/HorizontalAlignment.hpp"
#include "uitk/utility/PixelConverter.hpp"

/**
 * Determine a rectangle for the provided child.
 *
 * Each child will receive an arranged space matching the following dimensions:
 *  - width equal to the fill property (relative to the parent's width)
 *  - height equal to the height of the parent
 *  - x equal to the parent's x + the amount of horizontal space occupied by other children so far
 *  - y equal to the parent's y
 *
 * The parent's children should be determined in the same order each time they're arranged.
 * @param parent composite
 * @param child to arrange
 * @return arranged space
 */
SDL_Rect HorizontalArranger::determineChildPlacement(Composite* parent, Widget* child) const {
    SDL_Rect rect;
    rect.x = getChildX(parent, child);
    rect.w = getChildWidth(parent, child);
    rect.h = parent->getContentHeight();
    rect.y = parent->getContentY();
    return rect;
}

/**
 * Given a parent and a child, determine the X location of the child.
 * @param parent the parent
 * @param child the child
 * @return x location of the child
 */
int HorizontalArranger::getChildX(Composite* parent, const Widget* child) const {
    int x{getStartingPoint(parent, child)};
    HorizontalAlignment childAlignment = child->getLayout().getArrangementData().getHorizontalAlignment();
    for (const auto& sibling : parent->getChildren()) {
        if (sibling->getId() == child->getId()) {
            break;
        }

        HorizontalAlignment siblingAlignment = sibling->getLayout().getArrangementData().getHorizontalAlignment();
        if (siblingAlignment == childAlignment) {
            x += getChildWidth(parent, sibling.get());
        }
    }

    return x;
}

/**
 * Determine the x position of the given child.
 * @param parent the parent of the provided child
 * @param child determine the starting point for this widget
 * @return x position in pixels
 */
int HorizontalArranger::getStartingPoint(Composite* parent, const Widget* child) const {
    int x{parent->getContentX()};
    HorizontalAlignment alignment = child->getLayout().getArrangementData().getHorizontalAlignment();
    if (alignment == HorizontalAlignment::CENTER) {
        x += (parent->getContentWidth() - getFillPixelTotal(parent, alignment)) / 2;
    } else if (alignment == HorizontalAlignment::RIGHT) {
        x += parent->getContentWidth() - getFillPixelTotal(parent, alignment);
    } else {
        x += 0;
    }
    return x;
}

/**
 * Determine the width of the given child.
 * @param parent the parent of the provided child
 * @param child determine the width for this widget
 * @return width in pixels
 */
int HorizontalArranger::getChildWidth(Composite* parent, const Widget* child) const {
    PixelConverter pixelConverter;
    int width = pixelConverter.convert(child->getLayout().getArrangementData().getFill(), parent->getContentWidth());

    int minFill = child->getLayout().getArrangementData().getMinFill();
    if (minFill > 0 && width < minFill) {
        width = minFill;
    }

    int maxFill = child->getLayout().getArrangementData().getMaxFill();
    if (maxFill > 0 && width > maxFill) {
        width = maxFill;
    }

    return width;
}

/**
 * Total the count of horizontal pixels filled within the parent by all the children having the given alignment.
 * @param parent the parent of the children occupying space
 * @param alignment the alignment of children to consider
 * @return total number of horizontal pixels of filled space
 */
int HorizontalArranger::getFillPixelTotal(Composite* parent, HorizontalAlignment alignment) const {
    int total{0};
    for (const auto& sibling : parent->getChildren()) {
        if (sibling->getLayout().getArrangementData().getHorizontalAlignment() == alignment) {
            total += getChildWidth(parent, sibling.get());
        }
    }
    return total;
}

/**
 * Retrieve the type of arranger.
 *
 * See ArrangerFactory for builtin type constants.
 * @return arranger type
 */
std::string HorizontalArranger::getType() {
    return "horizontal";
}
