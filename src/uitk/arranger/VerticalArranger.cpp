#include "uitk/arranger/VerticalArranger.hpp"
#include "uitk/type/VerticalAlignment.hpp"
#include "uitk/utility/PixelConverter.hpp"

/**
 * Determine a rectangle for the provided child.
 *
 * Each child will receive an arranged space matching the following dimensions:
 *  - width equal to the width of the parent
 *  - height equal to the fill property (relative to the parent's height)
 *  - x equal to the parent's x
 *  - y equal to the parent's y + the amount of vertical space occupied by other children so far
 *
 * The parent's children should be determined in the same order each time they're arranged.
 * @param parent composite
 * @param child to arrange
 * @return arranged space
 */
SDL_Rect VerticalArranger::determineChildPlacement(Composite* parent, Widget* child) const {
    SDL_Rect rect;
    rect.x = parent->getContentX();
    rect.w = parent->getContentWidth();
    rect.h = getChildHeight(parent, child);
    rect.y = getChildY(parent, child);
    return rect;
}

/**
 * Given a parent and a child, determine the Y location of the child.
 * @param parent the parent
 * @param child the child
 * @return y location of the child
 */
int VerticalArranger::getChildY(Composite* parent, const Widget* child) const {
    int y{getStartingPoint(parent, child)};
    VerticalAlignment childAlignment = child->getLayout().getArrangementData().getVerticalAlignment();
    for (const auto& sibling : parent->getChildren()) {
        if (sibling->getId() == child->getId()) {
            break;
        }

        VerticalAlignment siblingAlignment = sibling->getLayout().getArrangementData().getVerticalAlignment();
        if (siblingAlignment == childAlignment) {
            y += getChildHeight(parent, sibling.get());
        }
    }

    return y;
}

/**
 * Determine the y position of the given child.
 * @param parent the parent of the provided child
 * @param child determine the starting point for this widget
 * @return y position in pixels
 */
int VerticalArranger::getStartingPoint(Composite* parent, const Widget* child) const {
    int y{parent->getContentY()};
    VerticalAlignment alignment = child->getLayout().getArrangementData().getVerticalAlignment();
    if (alignment == VerticalAlignment::CENTER) {
        y += (parent->getContentHeight() - getFillPixelTotal(parent, alignment)) / 2;
    } else if (alignment == VerticalAlignment::BOTTOM) {
        y += parent->getContentHeight() - getFillPixelTotal(parent, alignment);
    } else {
        y += 0;
    }
    return y;
}

/**
 * Determine the height of the given child.
 * @param parent the parent of the provided child
 * @param child determine the width for this widget
 * @return height in pixels
 */
int VerticalArranger::getChildHeight(Composite* parent, const Widget* child) const {
    PixelConverter pixelConverter;
    int height = pixelConverter.convert(child->getLayout().getArrangementData().getFill(), parent->getContentHeight());

    int minFill = child->getLayout().getArrangementData().getMinFill();
    if (minFill > 0 && height < minFill) {
        height = minFill;
    }

    int maxFill = child->getLayout().getArrangementData().getMaxFill();
    if (maxFill > 0 && height > maxFill) {
        height = maxFill;
    }

    return height;
}

/**
 * Total the count of vertical pixels filled within the parent by all the children having the given alignment.
 * @param parent the parent of the children occupying space
 * @param alignment the alignment of children to consider
 * @return total number of vertical pixels of filled space
 */
int VerticalArranger::getFillPixelTotal(Composite* parent, VerticalAlignment alignment) const {
    int total{0};
    for (const auto& sibling : parent->getChildren()) {
        if (sibling->getLayout().getArrangementData().getVerticalAlignment() == alignment) {
            total += getChildHeight(parent, sibling.get());
        }
    }
    return total;
}

/**
 * Retrieve the type of arranger.
 *
 * See ArrangerFactory for builtin type constants.
 * @return arranger type
 */
std::string VerticalArranger::getType() {
    return "vertical";
}
