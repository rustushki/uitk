#include "uitk/arranger/GridArranger.hpp"
#include "uitk/type/ArrangementData.hpp"
#include "uitk/widget/BoxWidget.hpp"

/**
 * Determine a rectangle for the provided child.
 *
 * Each child will receive a rectangle which represents a slot in the grid. Each slot of the grid will be equally
 * sized. The grid position corresponding to the rectangle will be according to the gridX and gridY properties of the
 * arrangement data. The width and height of the grid are available in the arranger data.
 * @param parent composite
 * @param child to arrange
 * @return SDL_Rect
 */
SDL_Rect GridArranger::determineChildPlacement(Composite* parent, Widget* child) const {
    ArrangementData childArrangementData = child->getLayout().getArrangementData();

    int gridSlotSizeWidth{0};
    int slotX{0};
    int gridWidth = parent->getArrangerData().getGridWidth();
    if (gridWidth > 0) {
        gridSlotSizeWidth = parent->getContentWidth() / gridWidth;
        slotX = modulo(childArrangementData.getGridX(), gridWidth);
    }

    int gridSlotSizeHeight{0};
    int slotY{0};
    int gridHeight = parent->getArrangerData().getGridHeight();
    if (gridHeight > 0) {
        gridSlotSizeHeight = parent->getContentHeight() / gridHeight;
        slotY = modulo(childArrangementData.getGridY(), gridHeight);
    }

    SDL_Rect rect;
    rect.x = parent->getContentX() + slotX * gridSlotSizeWidth;
    rect.y = parent->getContentY() + slotY * gridSlotSizeHeight;
    rect.w = gridSlotSizeWidth;
    rect.h = gridSlotSizeHeight;

    return rect;
}

/**
 * An implementation of module.
 * <p>
 * This is here because the % operator in C++ is actually a remainder operator (which is slightly different that the
 * modulo operator.
 * @param x
 * @param n
 * @return int
 */
int GridArranger::modulo(int x, int n) const {
    return (x % n + n) % n;
}

/**
 * Retrieve the type of arranger.
 *
 * See ArrangerFactory for builtin type constants.
 * @return arranger type
 */
std::string GridArranger::getType() {
    return "grid";
}
