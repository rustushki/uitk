#include "uitk/arranger/AbsoluteArranger.hpp"

/**
 * Determine the placement of the child widget given the parent widget.
 * <p>
 * Absolute arrangement will return an SDL_Rect based on the X, Y, Width and
 * Height fields of the Arrangement Data.
 * @param parent composite
 * @param child to arrange
 * @return SDL_Rect
 */
SDL_Rect AbsoluteArranger::determineChildPlacement(Composite* parent, Widget* child) const {
	SDL_Rect rect;
	rect.x = child->getLayout().getArrangementData().getX();
	rect.y = child->getLayout().getArrangementData().getY();
	rect.w = child->getLayout().getArrangementData().getWidth();
	rect.h = child->getLayout().getArrangementData().getHeight();
	return rect;
}

/**
 * Retrieve the type of arranger.
 *
 * See ArrangerFactory for builtin type constants.
 * @return arranger type
 */
std::string AbsoluteArranger::getType() {
    return "absolute";
}
