#include "uitk/core/Context.hpp"
#include "uitk/utility/PathUtility.hpp"
#include "uitk/utility/factory/AnimationFactory.hpp"
#include "uitk/utility/factory/SpriteSheetRegistryFactory.hpp"

/**
 * Constructor
 * <p>
 * Build the context given the path to the resources directory.
 */
Context::Context(const std::filesystem::path& resourcesDirectory) {
    this->pathUtility = std::make_shared<PathUtility>(resourcesDirectory);

    SpriteSheetRegistryFactory factory;
    this->spriteSheetRegistry = factory.build(pathUtility->getImagesPath(),
            pathUtility->getRegistriesPath() / "spriteSheets.json");

    this->animationFactory = std::make_shared<AnimationFactory>(pathUtility->getRegistriesPath(), spriteSheetRegistry);
}

/**
 * Get the object which holds the paths to various system resources.
 * @return std::shared_ptr<PathUtility>
 */
std::shared_ptr<PathUtility> Context::getPathUtility() {
    return this->pathUtility;
}

/**
 * Get the object which can build animation instances based on a name.
 * @return std::shared_ptr<AnimationFactory>
 */
std::shared_ptr<AnimationFactory> Context::getAnimationFactory() {
    return this->animationFactory;
}

/**
 * Get the object which holds the registered sprite sheets.
 * @return std::shared_ptr<SpriteSheetRegistry>
 */
std::shared_ptr<SpriteSheetRegistry> Context::getSpriteSheetRegistry() {
    return this->spriteSheetRegistry;
}

/**
 * Get the object which can build widget instances.
 * @return widget factory
 */
std::shared_ptr<WidgetFactory> Context::getWidgetFactory() {
    return this->widgetFactory;
}

/**
 * Set the object which can build widget instances.
 * @param widgetFactory the object which builds widgets
 */
void Context::setWidgetFactory(const std::shared_ptr<WidgetFactory>& widgetFactory) {
    this->widgetFactory = widgetFactory;
}
