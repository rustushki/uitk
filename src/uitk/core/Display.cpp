#include <iostream>
#include <fstream>
#include "uitk/core/Display.hpp"
#include "uitk/utility/PathUtility.hpp"

/**
 * Constructor.
 * <p>
 * Builds the application window and sets its title.
 * @param context
 * @param windowTitle
 */
Display::Display(std::shared_ptr<Context> context, const std::string& windowTitle) {
    this->initializeDependencies();
    this->context = std::move(context);
    this->window = createWindow(windowTitle);
    this->renderer = createRenderer(window);
    this->widgetTree = std::make_shared<WidgetTree>();
    this->context->setWidgetFactory(std::make_shared<WidgetFactory>(this->context, this->widgetTree));
}

/**
 * Destructor.
 */
Display::~Display() {
    this->reset();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
}

/**
 * Initializes UITK's internal dependencies.
 * <p>
 * These include SDL and SDL_ttf.
 */
void Display::initializeDependencies() const {
    // Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::stringstream errorMessage;
        errorMessage << "Couldn't initialize SDL: "<< SDL_GetError();
        throw std::runtime_error(errorMessage.str());
    }

    // Initialize SDL_ttf
    if (TTF_Init() == -1) {
        std::stringstream errorMessage;
        errorMessage << "Error initializing SDL_ttf: " << TTF_GetError();
        throw std::runtime_error(errorMessage.str());
    }

    // Ensure SDL_Quit is called when the program exits
    atexit(SDL_Quit);
}

/**
 * Re-draw each Widget onto the back buffer, then present it.
 */
void Display::draw() const {
    // Clear the back frame to black
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);
    SDL_RenderClear(renderer);

    // Redraw the tree of widgets from the root element down
    SDL_Rect rect;
    rect.x = 0;
    rect.y = 0;
    SDL_GetWindowSize(window, &rect.w, &rect.h);
    if (widgetTree->isRootWidgetSet()) {
        widgetTree->getRootWidget()->applyArrangedSpace(rect);
        widgetTree->getRootWidget()->draw(renderer);
    }

    // Present the back frame
    SDL_RenderPresent(renderer);
}

/**
 * Get the height of the Display's window.
 * @return height of window
 */
uint16_t Display::getHeight() const {
    int height;
    SDL_GetWindowSize(window, nullptr, &height);
    return (uint16_t) height;
}

/**
 * Get the width of the Display's window.
 * @return width of window
 */
uint16_t Display::getWidth() const {
    int width;
    SDL_GetWindowSize(window, &width, nullptr);
    return (uint16_t) width;
}

/**
 * Advance the animations of every widget in the tree of widgets starting at the root.
 */
void Display::advanceAnimations() {
    if (widgetTree->isRootWidgetSet()) {
        widgetTree->getRootWidget()->advanceAnimations();
    }
}

/**
 * Get the set of widgets in the tree of widgets which intersects with the provided coordinate.
 *
 * @param x coordinate
 * @param y coordinate
 * @return std::shared_ptr<Widget>
 */
std::set<std::shared_ptr<Widget>> Display::getWidgetsUnderCoordinate(uint32_t x, uint32_t y) const {
    return widgetTree->getWidgetsUnderCoordinate(x, y);
}

/**
 * Find the widget on the display that has the given ID and return it.
 *
 * This will recursively check all children of the root widget for a widget with the given ID. If there exists no such
 * widget, then nullptr will be returned.
 * @param id
 * @return Widget*
 */
std::shared_ptr<Widget> Display::getWidgetById(const std::string& id) const {
    return widgetTree->getWidgetById(id);
}

/**
 * Set the view which should be considered in focus.
 * @param viewName
 */
void Display::setFocus(std::string viewId) {
    this->focusedWidgetId = std::move(viewId);
}

/**
 * Get the view which should be considered in focus.
 * @return string
 */
std::string Display::getFocus() const {
    return this->focusedWidgetId;
}

/**
 * Reset the display back to a clean state.
 * <p>
 * This will remove focus from any widgets and delete all of them.
 */
void Display::reset() {
    if (widgetTree->isRootWidgetSet()) {
        widgetTree->clearRootWidget();
    }
    setFocus("");
}

/**
 * Create the SDL_Window* for the game, setting it's title.
 * @param windowTitle
 * @return SDL_Window*
 */
SDL_Window* Display::createWindow(const std::string& windowTitle) {
    SDL_Window* window = SDL_CreateWindow(windowTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 500, 300,
                                          SDL_WINDOW_HIDDEN | SDL_WINDOW_RESIZABLE);

    // Throw an exception if the Window was not created
    if (window == nullptr) {
        std::stringstream errorMessage;
        errorMessage << "Couldn't create window: "<< SDL_GetError();
        throw std::runtime_error(errorMessage.str());
    }

    SDL_ShowWindow(window);
    //SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);

    return window;
}

/**
 * Create a renderer for the SDL_Window.
 * @param window
 * @return SDL_Renderer*
 */
SDL_Renderer* Display::createRenderer(SDL_Window* window) {
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == nullptr) {
        std::stringstream errorMessage;
        errorMessage << "Couldn't create renderer: "<< SDL_GetError();
        throw std::runtime_error(errorMessage.str());
    }

    return renderer;
}

/**
 * Load a View by its ID into the given display.
 * @param viewId
 */
void Display::load(const std::string& viewId) {
    std::ifstream inputStream(context->getPathUtility()->getViewsPath() / (viewId + ".json"));
    json view;
    inputStream >> view;

    JsonAccessor accessor;
    this->reset();
    widgetTree->setRootWidget(context->widgetFactory->build(accessor.require<json>(view, KEY_NAME_ROOT_WIDGET)));

    auto defaultFocusId = accessor.optional<std::string>(view, KEY_NAME_DEFAULT_FOCUS_ID, KEY_DEFAULT_DEFAULT_FOCUS_ID);
    if (!defaultFocusId.empty()) {
        setFocus(defaultFocusId);
    }

    hoverWidgetIds.clear();
}

/**
 * Set the function to be called each time a frame is drawn to the screen.
 * <p>
 * This function is run unconditionally. It should seek to return as soon as possible to allow for a maximum framerate.
 * @param onFrameCallback
 */
void Display::setOnFrameCallback(const std::function<bool()> &onFrameCallback) {
    this->onFrameCallback = onFrameCallback;
}

/**
 * Presents the root widget and its children onto the screen as a frame up to 60 times per second. In between each
 * frame, input in processed and the on-frame callback is called. Any extra time available between frames is spent
 * sleeping in order to keep the CPU free.
 * @return true signifies that the user wants to exit
 */
bool Display::present() {
    bool isQuitting{false};
    // Limit frames per second
    uint32_t fps = 60;
    uint32_t maxDelay = 1000 / fps;

    // Iterate frames until the exit conditions are met
    bool exitPresentLoop = false;
    while (!isQuitting && !exitPresentLoop) {
        // Begin preparing the frame
        uint32_t workStart = SDL_GetTicks();

        // Advance the animations because we're about to compose the frame
        advanceAnimations();

        // Build and present the frame
        draw();

        // Collect input from the user
        isQuitting = processInput();

        if (!isQuitting) {
            exitPresentLoop = false;
            if (onFrameCallback != nullptr) {
                exitPresentLoop = onFrameCallback();
            }

            if (!exitPresentLoop) {
                // Determine how much time we have left after doing work
                uint32_t workEnd = SDL_GetTicks();
                uint32_t workDuration = workEnd - workStart;
                int remainingTime = maxDelay - workDuration;

                // Sleep any remaining time so that we don't hog the CPU
                if (remainingTime > 0) {
                    SDL_Delay((uint32_t) remainingTime);
                }
            }
        }
    }

    return isQuitting;
}

/**
 * Polls for events and handles them.
 * <p>
 * Generally, handling works like this:
 * <ul>
 * <li>Key events are delegated to the focused widget</li>
 * <li>Mouse events are delegated to the top-most widget under the mouse</li>
 * <li>Quit events always cause this function to return true</li>
 * <ul>
 * @return true signifies that the quit event was received
 */
bool Display::processInput() {
    bool isQuitting{false};
    SDL_Event event = {};
    while (SDL_PollEvent(&event) == 1) {
        // Key Down Events are passed to focused view. Do not pass repeat keydown events
        if (event.type == SDL_KEYDOWN) {
            if (event.key.repeat == 0) {
                std::string focusedWidgetId = getFocus();
                std::shared_ptr<Widget> focusedWidget = getWidgetById(focusedWidgetId);
                if (focusedWidget != nullptr) {
                    focusedWidget->onKeyDown(event);
                }
            }

        // Key Up Events are passed to focused view
        } else if (event.type == SDL_KEYUP) {
            std::string focusedWidgetId = getFocus();
            std::shared_ptr<Widget> focusedWidget = getWidgetById(focusedWidgetId);
            if (focusedWidget != nullptr) {
                focusedWidget->onKeyUp(event);
            }

        // Quit Events will cause the game to exit
        } else if (event.type == SDL_QUIT) {
            isQuitting = true;
            break;

        // Delegate Mouse Up and Down to the widget as they intersect with the cursor (disregarding focus)
        } else if (event.type == SDL_MOUSEBUTTONUP || event.type == SDL_MOUSEBUTTONDOWN) {
            auto x = (uint32_t) event.button.x;
            auto y = (uint32_t) event.button.y;
            for (const auto& widget : getWidgetsUnderCoordinate(x, y)) {
                if (widget != nullptr) {
                    if (event.type == SDL_MOUSEBUTTONUP) {
                        widget->onMouseUp(event);
                    } else if (event.type == SDL_MOUSEBUTTONDOWN) {
                        widget->onMouseDown(event);
                    }
                }
            }

        // Delegate Mouse Hover to widgets as they intersect with the cursor (disregarding focus)
        // Mouse Hover Out events are delegated when the mouse no longer hovers over a widget
        } else if (event.type == SDL_MOUSEMOTION) {
            auto x = (uint32_t) event.motion.x;
            auto y = (uint32_t) event.motion.y;

            std::set<std::string> newHoverWidgetIds;
            for (const auto& widget : getWidgetsUnderCoordinate(x, y)) {
                newHoverWidgetIds.insert(widget->getId());
            }

            // Mouse Hover
            for (const auto& id : newHoverWidgetIds) {
                if (hoverWidgetIds.find(id) != hoverWidgetIds.end()) {
                    std::shared_ptr<Widget> widget = getWidgetById(id);
                    if (widget != nullptr) {
                        widget->onMouseHover(event);
                    }
                }
            }

            // Mouse Hover Out
            for (const auto& id : hoverWidgetIds) {
                if (newHoverWidgetIds.find(id) == newHoverWidgetIds.end()) {
                    std::shared_ptr<Widget> widget = getWidgetById(id);
                    if (widget != nullptr) {
                        widget->onMouseHoverOut(event);
                    }
                }
            }

            hoverWidgetIds = newHoverWidgetIds;
        }
    }

    return isQuitting;
}
