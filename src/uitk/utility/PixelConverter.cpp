#include "uitk/utility/PixelConverter.hpp"

/**
 * Convert the given measure into an integer pixel count relative to the provided integer.
 * This calculation varies per unit type:
 *   - PIXEL - return the value as is
 *   - PERCENT_POINT - multiply the value by the provided relative value
 */
int PixelConverter::convert(const Measure& measure, int relative) const {
    int pixels{0};
    if (measure.getUnit() == PERCENT_POINT) {
        pixels = convertRelative(measure, relative);
    } else {
        pixels = convertAbsolute(measure);
    }

    return pixels;
}

/**
 * Helper method. Casts value as integer.
 * @return value as integer pixels
 */
int PixelConverter::convertAbsolute(const Measure& measure) const {
    return static_cast<int>(measure.getValue());
}

/**
 * Helper method. Converts relative measure to integer pixel measure.
 * @return value as integer pixels
 */
int PixelConverter::convertRelative(const Measure& measure, int relative) const {
    int relativePixels{0};

    if (measure.getUnit() == PERCENT_POINT) {
        relativePixels = static_cast<int>(measure.getValue() / 100.0 * static_cast<double>(relative));
    }

    return relativePixels;
}
