#include "uitk/utility/PathUtility.hpp"

/**
 * Constructor.
 * @param resourcesPathString
 */
PathUtility::PathUtility(std::string resourcesPathString) {
    this->resourcesPath = resourcesPathString;
    this->viewsPath = resourcesPath / "views";
    this->imagesPath = resourcesPath / "images";
    this->fontsPath = resourcesPath / "fonts";
    this->registriesPath = resourcesPath / "registries";
}

/**
 * Get the path to the application resources directory.
 * @return std::filesystem::path
 */
std::filesystem::path PathUtility::getResourcesPath() const {
    return resourcesPath;
}

/**
 * Get the path to the application views directory.
 * <p>
 * This is where the application's view JSON files live.
 * @return std::filesystem::path
 */
std::filesystem::path PathUtility::getViewsPath() const {
    return viewsPath;
}

/**
 * Get the path to the application images directory.
 * <p>
 * This is where the source images for the spritesheets live.
 * @return std::filesystem::path
 */
std::filesystem::path PathUtility::getImagesPath() const {
    return imagesPath;
}

/**
 * Get the path to the application fonts directory.
 * <p>
 * This is where the fonts which can be referenced by widgets' font name property live.
 * @return std::filesystem::path
 */
std::filesystem::path PathUtility::getFontsPath() const {
    return fontsPath;
}

/**
 * Get the path to the application registries directory.
 * <p>
 * This is where the spritesheets.json and animations.json files live.
 * @return std::filesystem::path
 */
std::filesystem::path PathUtility::getRegistriesPath() const {
    return registriesPath;
}


