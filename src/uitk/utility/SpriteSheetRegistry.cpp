#include "uitk/utility/SpriteSheetRegistry.hpp"

/**
 * Register the provided sprite sheet with the given ID.
 * @param id
 * @param spriteSheet
 */
void SpriteSheetRegistry::registerSpriteSheet(std::string id, std::shared_ptr<SpriteSheet> spriteSheet) {
    idToSpriteSheetMap[id] = spriteSheet;
}

/**
 * Retrieve a sprite sheet by its ID.
 * @return std::shared_ptr<SpriteSheet>
 */
std::shared_ptr<SpriteSheet> SpriteSheetRegistry::getSpriteSheet(std::string id) const {
    return idToSpriteSheetMap.at(id);
}

