#include "uitk/utility/factory/LayoutFactory.hpp"

/**
 * Given a JSON element, try to build an object representing the layout.
 * @param element the JSON data
 * @return the built layout
 */
Layout LayoutFactory::build(const json& element) const {
    Layout layout;

    layout.setArrangementData(arrangementDataFactory.build(
        accessor.require<json>(element, KEY_NAME_ARRANGEMENT_DATA))
    );

    layout.setPadding(paddingFactory.build(
        accessor.optional<json>(element, KEY_NAME_PADDING, KEY_DEFAULT_PADDING)
    ));

    layout.setExternalAlignment(alignmentFactory.build(
        accessor.optional<json>(element, KEY_NAME_EXTERNAL_ALIGNMENT, KEY_DEFAULT_EXTERNAL_ALIGNMENT)
    ));

    layout.setScaleBehavior(enumFactory.buildScaleBehavior(
        accessor.optional<std::string>(element, KEY_NAME_SCALE_BEHAVIOR, KEY_DEFAULT_SCALE_BEHAVIOR))
    );

    return layout;
}

