#include "uitk/utility/factory/EnumFactory.hpp"

/**
 * Build a VerticalAlignment given a string.
 *
 * Defaults to CENTER.
 * @param source string source data
 * @return vertical alignment
 */
VerticalAlignment EnumFactory::buildVerticalAlignment(const std::string& source) const {
    VerticalAlignment verticalAlignment{VerticalAlignment::CENTER};
    if (source == KEY_VALUE_VERTICAL_ALIGNMENT_CENTER) {
        verticalAlignment = VerticalAlignment::CENTER;
    } else if (source == KEY_VALUE_VERTICAL_ALIGNMENT_TOP) {
        verticalAlignment = VerticalAlignment::TOP;
    } else if (source == KEY_VALUE_VERTICAL_ALIGNMENT_BOTTOM) {
        verticalAlignment = VerticalAlignment::BOTTOM;
    }
    return verticalAlignment;
}

/**
 * Build a HorizontalAlignment given a string.
 *
 * Defaults to CENTER.
 * @param source string source data
 * @return horizontal alignment
 */
HorizontalAlignment EnumFactory::buildHorizontalAlignment(const std::string& source) const {
    HorizontalAlignment horizontalAlignment{HorizontalAlignment::CENTER};
    if (source == KEY_VALUE_HORIZONTAL_ALIGNMENT_CENTER) {
        horizontalAlignment = HorizontalAlignment::CENTER;
    } else if (source == KEY_VALUE_HORIZONTAL_ALIGNMENT_LEFT) {
        horizontalAlignment = HorizontalAlignment::LEFT;
    } else if (source == KEY_VALUE_HORIZONTAL_ALIGNMENT_RIGHT) {
        horizontalAlignment = HorizontalAlignment::RIGHT;
    }
    return horizontalAlignment;
}

/**
 * Build an Orientation given a string.
 *
 * Defaults to HORIZONTAL.
 * @param source string source data
 * @return orientation
 */
Orientation EnumFactory::buildOrientation(const std::string& source) const {
    Orientation orientation{Orientation::HORIZONTAL};
    if (source == KEY_VALUE_ORIENTATION_HORIZONTAL) {
        orientation = Orientation::HORIZONTAL;
    } else if (source == KEY_VALUE_ORIENTATION_VERTICAL) {
        orientation = Orientation::VERTICAL;
    }
    return orientation;
}

/**
 * Build a ScaleBehavior given a string.
 *
 * Defaults to SCALE_NONE
 * @param source string source data
 * @return scale behavior
 */
ScaleBehavior EnumFactory::buildScaleBehavior(const std::string& source) const {
    ScaleBehavior scaleBehavior{SCALE_NONE};
    if (source == KEY_VALUE_SCALE_BEHAVIOR_SCALE) {
        scaleBehavior = ScaleBehavior::SCALE;
    } else if (source == KEY_VALUE_SCALE_BEHAVIOR_SCALE_NONE) {
        scaleBehavior = ScaleBehavior::SCALE_NONE;
    } else if (source == KEY_VALUE_SCALE_BEHAVIOR_STRETCH_HORIZONTAL) {
        scaleBehavior = ScaleBehavior::STRETCH_HORIZONTAL;
    } else if (source == KEY_VALUE_SCALE_BEHAVIOR_STRETCH_VERTICAL) {
        scaleBehavior = ScaleBehavior::STRETCH_VERTICAL;
    } else if (source == KEY_VALUE_SCALE_BEHAVIOR_STRETCH_BOTH) {
        scaleBehavior = ScaleBehavior::STRETCH_BOTH;
    }
    return scaleBehavior;
}

/**
 * Build a PointContainmentBehavior given a string.
 *
 * Defaults to BOTH
 * @param source string source data
 * @return point containment behavior 
 */
PointContainmentBehavior EnumFactory::buildPointContainmentBehavior(const std::string& source) const {
    PointContainmentBehavior pointContainmentBehavior{BOTH};
    if (source == KEY_VALUE_POINT_CONTAINMENT_BEHAVIOR_PARENT_ONLY) {
        pointContainmentBehavior = PointContainmentBehavior::PARENT_ONLY;
    } else if (source == KEY_VALUE_POINT_CONTAINMENT_BEHAVIOR_CHILDREN_ONLY) {
        pointContainmentBehavior = PointContainmentBehavior::CHILDREN_ONLY;
    } else if (source == KEY_VALUE_POINT_CONTAINMENT_BEHAVIOR_BOTH) {
        pointContainmentBehavior = PointContainmentBehavior::BOTH;
    }
    return pointContainmentBehavior;
}
