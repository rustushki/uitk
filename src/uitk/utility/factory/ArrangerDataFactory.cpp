#include "uitk/utility/factory/ArrangerDataFactory.hpp"

/**
 * Build an arranger data object given a JSON.
 *
 * All fields are optional. These fields are describe the arranged space itself whereas arrangement data describes how a
 * widget will fit into the arranged space.
 * @param type
 * @return arranger data
 */
ArrangerData ArrangerDataFactory::build(const json& element) const {
    ArrangerData arrangerData;
    JsonAccessor accessor;

    arrangerData.setGridWidth(
        accessor.optional<int>(element, KEY_NAME_GRID_WIDTH, KEY_DEFAULT_GRID_WIDTH)
    );

    arrangerData.setGridHeight(
        accessor.optional<int>(element, KEY_NAME_GRID_HEIGHT, KEY_DEFAULT_GRID_HEIGHT)
    );

    return arrangerData;
}
