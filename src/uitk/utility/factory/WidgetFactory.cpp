#include <functional>
#include "uitk/arranger/AbsoluteArranger.hpp"
#include "uitk/utility/factory/LayoutFactory.hpp"
#include "uitk/utility/factory/WidgetFactory.hpp"
#include "uitk/widget/BoxWidget.hpp"
#include "uitk/widget/ImageWidget.hpp"
#include "uitk/widget/LabelWidget.hpp"
#include "uitk/widget/MenuWidget.hpp"
#include "uitk/widget/Widget.hpp"

/**
 * Constructor.
 * <p>
 * Registers the built in widgets that can be built.
 * @param context
 */
WidgetFactory::WidgetFactory(const std::shared_ptr<Context>& context, const std::weak_ptr<WidgetTree>& widgetTree) {
    this->widgetTree = widgetTree;
    registerWidgetType(TYPE_BOX, [context] () { return std::make_shared<BoxWidget>(context); });
    registerWidgetType(TYPE_BUTTON, [context] () { return std::make_shared<ButtonWidget>(context); });
    registerWidgetType(TYPE_IMAGE, [context] () { return std::make_shared<ImageWidget>(context); });
    registerWidgetType(TYPE_LABEL, [context] () { return std::make_shared<LabelWidget>(context); });
    registerWidgetType(TYPE_MENU, [context] () { return std::make_shared<MenuWidget>(context); });
    registerWidgetType(TYPE_RECTANGLE, [context] () { return std::make_shared<RectangleWidget>(context); });
}

/**
 * Build a widget given a type.
 * 
 * All properties are defaulted. The ID is set to a random, and unique string.
 * @return the widget
 */
std::shared_ptr<Widget> WidgetFactory::build(const std::string& type) const {
    return build(type, widgetTree.lock()->generateId());
}

/**
 * Build a widget given a type and assign the given ID.
 * 
 * All properties are defaulted. If the ID is not unique a runtime_error will be thrown.
 * @return the widget
 */
std::shared_ptr<Widget> WidgetFactory::build(const std::string& type, const std::string& id) const {
    auto builder = builderMap.at(type);
    auto widget = builder();
    widget->setId(id);
    return widget;
}

/**
 * Build a widget given a JSON element.
 *
 * Widget::decorate() will be called on the JSON element. A runtime_error can be thrown if a required field is not
 * specified or if the ID is not unique.
 * @param widgetData
 * @return the widget
 */
std::shared_ptr<Widget> WidgetFactory::build(const json& widgetData) const {
    JsonAccessor accessor;
    auto type = accessor.require<std::string>(widgetData, KEY_NAME_TYPE);
    auto id = accessor.require<std::string>(widgetData, KEY_NAME_ID);
    auto widget = build(type, id);

    LayoutFactory layoutFactory;
    widget->setLayout(layoutFactory.build(
        accessor.require<json>(widgetData, KEY_NAME_LAYOUT)
    ));

    widget->decorate(accessor.require<json>(widgetData, KEY_NAME_PROPERTIES));

    if (widget->getCompositeType() == READ_ONLY_COMPOSITE || widget->getCompositeType() == READ_WRITE_COMPOSITE) {
        auto composite = std::dynamic_pointer_cast<Composite>(widget);
        JsonAccessor accessor;
        auto children = accessor.optional<json>(widgetData, KEY_NAME_CHILDREN, json());
        for (const json& child : children) {
            composite->addChildHelper(this->build(child));
        }
    }

    return widget;
}

/**
 * Register a new widget type that can be built given a type string and a builder function.
 * @param typeName
 * @param builder
 */
void WidgetFactory::registerWidgetType(const std::string& typeName, std::function<std::shared_ptr<Widget>()> builder) {
    builderMap[typeName] = std::move(builder);
}

/**
 * Return true if there exists a widget instance with the given ID.
 *
 * This is provided to enable callers to determine if an ID is available before attempting to build a widget with it.
 * @param id the id to search for
 * @return is the ID taken?
 */
bool WidgetFactory::isWidgetIdTaken(const std::string& id) const {
    return this->widgetTree.lock()->isWidgetIdTaken(id);
}
