#include "uitk/utility/factory/ArrangementDataFactory.hpp"
#include "uitk/utility/factory/MeasureFactory.hpp"

/**
 * Given a JSON element, try to build an object representing the arrangement data.
 * @param element the JSON data
 * @return the built arrangement data
 */
ArrangementData ArrangementDataFactory::build(const json& element) const {
    MeasureFactory measureFactory;
    ArrangementData arrangementData;
    arrangementData.setX(accessor.optional<int>(element, KEY_NAME_X, 0));
    arrangementData.setY(accessor.optional<int>(element, KEY_NAME_Y, 0));
    arrangementData.setWidth(accessor.optional<int>(element, KEY_NAME_WIDTH, 0));
    arrangementData.setHeight(accessor.optional<int>(element, KEY_NAME_HEIGHT, 0));
    arrangementData.setGridX(accessor.optional<int>(element, KEY_NAME_GRID_X, 0));
    arrangementData.setGridY(accessor.optional<int>(element, KEY_NAME_GRID_Y, 0));
    arrangementData.setMaxFill(accessor.optional<int>(element, KEY_NAME_MAX_FILL, 0));
    arrangementData.setMinFill(accessor.optional<int>(element, KEY_NAME_MIN_FILL, 0));
    arrangementData.setFill(measureFactory.build(
        accessor.optional<std::string>(element, KEY_NAME_FILL, KEY_DEFAULT_FILL)
    ));

    arrangementData.setVerticalAlignment(enumFactory.buildVerticalAlignment(
            accessor.optional<std::string>(element, KEY_NAME_VERTICAL_ALIGNMENT, KEY_DEFAULT_VERTICAL_ALIGNMENT)
    ));

    arrangementData.setHorizontalAlignment(enumFactory.buildHorizontalAlignment(
            accessor.optional<std::string>(element, KEY_NAME_HORIZONTAL_ALIGNMENT, KEY_DEFAULT_HORIZONTAL_ALIGNMENT)
    ));

    return arrangementData;
}

