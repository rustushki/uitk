#include <fstream>
#include <memory>
#include "nlohmann/json.hpp"
#include "uitk/core/Context.hpp"
#include "uitk/utility/SpriteSheetRegistry.hpp"
#include "uitk/utility/factory/AnimationFactory.hpp"

using json = nlohmann::json;

/**
 * Constructor.
 * @param registriesPath
 * @param spriteSheetRegistry
 */
AnimationFactory::AnimationFactory(std::filesystem::path registriesPath,
        std::shared_ptr<SpriteSheetRegistry> spriteSheetRegistry) {
    this->registriesPath = std::move(registriesPath);
    this->spriteSheetRegistry = std::move(spriteSheetRegistry);
}

/**
 * Build an animation based on a provided name.
 * @param animationName
 * @return std::unique_ptr<Animation>
 */
std::unique_ptr<Animation> AnimationFactory::build(const std::string& animationName) const {
    std::ifstream inputStream(registriesPath / "animations.json");
    json animationsJson;
    inputStream >> animationsJson;
    json animations = accessor.require<json>(animationsJson, FIELD_ANIMATIONS);
    std::unique_ptr<Animation> animation;
    for (const json& animationJson : animations) {
        if (accessor.require<std::string>(animationJson, FIELD_ID) == animationName) {
            auto spriteSheet = spriteSheetRegistry->getSpriteSheet(
                    accessor.require<std::string>(animationJson, FIELD_SPRITESHEET)
            );
            auto frameList = accessor.require<std::vector<int>>(animationJson, FIELD_FRAME_LIST);
            auto stillsPerSecond = accessor.require<int>(animationJson, FIELD_STILLS_PER_SECOND);
            auto width = accessor.require<int>(animationJson, FIELD_FRAME_WIDTH);
            auto height = accessor.require<int>(animationJson, FIELD_FRAME_HEIGHT);
            animation = std::make_unique<Animation>(spriteSheet, frameList, stillsPerSecond, width, height);
            break;
        }
    }

    if (animation == nullptr) {
        throw std::logic_error("Animation could not be built. Name = " + animationName);
    }

    animation->play();
    return animation;
}
