#include "uitk/utility/factory/MeasureFactory.hpp"
#include "uitk/utility/factory/UnitFactory.hpp"

/**
 * Parse a string into a Measure.
 *
 * The format will be $value$unit, where value is a decimal and $unit is a string compatible with the Unit class.
 * Examples: 20px, 40%, 41.5%
 *
 * See UnitFactory for unit strings.
 * @param string the string
 * @return measure
 */
Measure MeasureFactory::build(const std::string& string) const {
    int index{0};

    std::string valuePart;
    for (; index < string.size() && isdigit(string[index]); index++) {
        if (isdigit(string[index])) {
            valuePart += string[index];
        }
    }

    if (string[index] == '.') {

        valuePart += string[index];

        index++;

        for (; index < string.size() && isdigit(string[index]); index++) {
            if (isdigit(string[index])) {
                valuePart += string[index];
            }
        }
    }

    UnitFactory unitFactory;
    Unit unit = unitFactory.build(string.substr(index, std::string::npos));
    double value = atof(valuePart.c_str());

    return Measure(value, unit);
}
