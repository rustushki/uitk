#include <fstream>
#include "nlohmann/json.hpp"
#include "uitk/utility/factory/SpriteSheetRegistryFactory.hpp"

using json = nlohmann::json;

/**
 * Given the path to the source images and the path to the registries, build the SpriteSheetRegistry.
 * @param imagesPath
 * @param registryPath
 * @return std::shared_ptr<SpriteSheetRegistry>
 */
std::shared_ptr<SpriteSheetRegistry> SpriteSheetRegistryFactory::build(const std::filesystem::path& imagesPath,
        const std::filesystem::path& registryPath) {

	std::ifstream inputStream(registryPath);
	json registryJson;
	inputStream >> registryJson;

    auto spriteSheetRegistry = std::make_shared<SpriteSheetRegistry>();
    auto spriteSheetsJson = accessor.require<json>(registryJson, FIELD_SPRITE_SHEETS);
    for (json spriteSheetJson : spriteSheetsJson) {
        auto id = accessor.require<std::string>(spriteSheetJson, FIELD_ID);
        auto path = imagesPath / accessor.require<std::string>(spriteSheetJson, FIELD_FILE_NAME);
        auto spriteSheet = std::make_shared<SpriteSheet>(path);
        spriteSheetRegistry->registerSpriteSheet(id, spriteSheet);
    }

    return spriteSheetRegistry;
}
