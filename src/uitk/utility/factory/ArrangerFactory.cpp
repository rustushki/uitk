#include "uitk/arranger/AbsoluteArranger.hpp"
#include "uitk/arranger/GridArranger.hpp"
#include "uitk/arranger/HorizontalArranger.hpp"
#include "uitk/arranger/StackingArranger.hpp"
#include "uitk/arranger/VerticalArranger.hpp"
#include "uitk/utility/factory/ArrangerFactory.hpp"

using json = nlohmann::json;

/**
 * Build the factory and registers the built in arrangers that can be built by the factory.
 */
ArrangerFactory::ArrangerFactory() {
    registerArrangerType(TYPE_ABSOLUTE, [] () { return std::make_shared<AbsoluteArranger>(); });
    registerArrangerType(TYPE_GRID, [] () { return std::make_shared<GridArranger>(); });
    registerArrangerType(TYPE_HORIZONTAL, [] () { return std::make_shared<HorizontalArranger>(); });
    registerArrangerType(TYPE_STACKING, [] () { return std::make_shared<StackingArranger>(); });
    registerArrangerType(TYPE_VERTICAL, [] () { return std::make_shared<VerticalArranger>(); });
}

/**
 * Build an arranger by its type name.
 * @param type
 * @return std::shared_ptr<Arranger>
 */
std::shared_ptr<Arranger> ArrangerFactory::build(const std::string& type) const {
    return builderMap.at(type)();
}

/**
 * Register a new builder for a given type name.
 * @param type
 * @param builder
 */
void ArrangerFactory::registerArrangerType(const std::string& type,
        std::function<std::shared_ptr<Arranger>()> builder) {
    builderMap[type] = std::move(builder);
}
