#include <algorithm>
#include "uitk/utility/factory/UnitFactory.hpp"

/**
 * Parse a string into a Unit.
 *  - PERCENT_POINT - '%'
 *  - PIXEL - 'px'
 *
 * An unrecognized unit will be treated as PIXEL.
 *
 * @param string the string
 * @return unit
 */
Unit UnitFactory::build(const std::string& string) const {
    int index{0};
    std::string unitPart;
    if (isalpha(string[index])) {
        for (; index < string.size() && isalpha(string[index]); index++) {
            if (isalpha(string[index])) {
                unitPart += string[index];
            }
        }
    } else if (string[index] == '%') {
        unitPart += string[index];
    }

    std::transform(unitPart.begin(), unitPart.end(), unitPart.begin(), [](unsigned char c) {
        return std::tolower(c);
    });

    Unit unit{PIXEL};
    if (unitPart == "%") {
        unit = PERCENT_POINT;

    } else if (unitPart == "px" || unitPart.empty()) {
        unit = PIXEL;
    }
    return unit;
}

