#include "uitk/utility/factory/AlignmentFactory.hpp"

/**
 * Given a JSON element, try to build an object representing an alignment.
 * @param element the JSON data
 * @return the built alignment
 */
Alignment AlignmentFactory::build(const json& element) const {
    Alignment alignment;

    alignment.setHorizontal(enumFactory.buildHorizontalAlignment(
        accessor.optional<std::string>(element, KEY_NAME_HORIZONTAL, KEY_DEFAULT_HORIZONTAL)
    ));

    alignment.setVertical(enumFactory.buildVerticalAlignment(
        accessor.optional<std::string>(element, KEY_NAME_VERTICAL, KEY_DEFAULT_VERTICAL)
    ));

    return alignment;
}

