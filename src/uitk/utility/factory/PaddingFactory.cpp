#include "uitk/utility/factory/MeasureFactory.hpp"
#include "uitk/utility/factory/PaddingFactory.hpp"

/**
 * Given a JSON element, try to build an object representing padding settings.
 * @param element the JSON data
 * @return the built padding object
 */
Padding PaddingFactory::build(const json& element) const {
    Padding padding;
    MeasureFactory measureFactory;

    padding.setTop(measureFactory.build(
        accessor.optional<std::string>(element, KEY_NAME_TOP, KEY_DEFAULT_TOP)
    ));
    padding.setBottom(measureFactory.build(
        accessor.optional<std::string>(element, KEY_NAME_BOTTOM, KEY_DEFAULT_BOTTOM)
    ));
    padding.setLeft(measureFactory.build(
        accessor.optional<std::string>(element, KEY_NAME_LEFT, KEY_DEFAULT_LEFT)
    ));
    padding.setRight(measureFactory.build(
        accessor.optional<std::string>(element, KEY_NAME_RIGHT, KEY_DEFAULT_RIGHT)
    ));

    return padding;
}

