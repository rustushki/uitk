#include "uitk/utility/WidgetTree.hpp"
#include "uitk/widget/ReadWriteComposite.hpp"
#include "uitk/widget/ReadOnlyComposite.hpp"

/**
 * Find the widget in the tree that has the given ID and return it.
 *
 * This will recursively check all children of the root widget for a widget with the given ID. If there exists no such
 * widget, then nullptr will be returned.
 * @param id search for
 * @return matching widget
 */
std::shared_ptr<Widget> WidgetTree::getWidgetById(const std::string& id) const {
    std::shared_ptr<Widget> match{nullptr};

    if (rootWidget != nullptr) {
        CompositeType compositeType = rootWidget->getCompositeType();
        if (rootWidget->getId() == id) {
            match = getRootWidget();
        } else if (compositeType == READ_WRITE_COMPOSITE) {
            auto readWriteCompositeRootWidget = std::dynamic_pointer_cast<ReadWriteComposite>(getRootWidget());
            match = readWriteCompositeRootWidget->getDescendantById(id);
        }
    }

    return match;
}

/**
 * Like getWidgetById(), but also includes widgets internally managed by ReadOnlyComposites.
 */
std::shared_ptr<const Widget> WidgetTree::getWidgetByIdInternal(const std::string& id) const {
    std::shared_ptr<const Widget> match{nullptr};

    if (rootWidget != nullptr) {
        CompositeType compositeType = rootWidget->getCompositeType();
        if (rootWidget->getId() == id) {
            match = getRootWidget();
        } else if (compositeType == READ_WRITE_COMPOSITE) {
            auto readWriteCompositeRootWidget = std::dynamic_pointer_cast<ReadWriteComposite>(getRootWidget());
            match = readWriteCompositeRootWidget->getDescendantById(id);
        } else if (compositeType == READ_ONLY_COMPOSITE) {
            auto readOnlyCompositeRootWidget = std::dynamic_pointer_cast<ReadOnlyComposite>(getRootWidget());
            match = readOnlyCompositeRootWidget->getDescendantById(id);
        }
    }

    return match;
}

/**
 * Return true if there exists a widget in the tree with the given ID.
 * @param id the id to search for
 * @return is there a widget in the tree with the id?
 */
bool WidgetTree::isWidgetIdTaken(const std::string& id) const {
    return getWidgetById(id) != nullptr;
}

/**
 * Return the instance of the root widget.
 * @return the root widget
 */
std::shared_ptr<Widget> WidgetTree::getRootWidget() const {
    return rootWidget;
}

/**
 * Determine if the root widget is currently set.
 * @return true if the root widget is set
 */
bool WidgetTree::isRootWidgetSet() const {
    return getRootWidget() != nullptr;
}

/**
 * Clear the root widget.
 *
 * Has the effect of clearing the whole tree of widgets, assuming there are no retained external references.
 */
void WidgetTree::clearRootWidget() {
    rootWidget = nullptr;
}

/**
 * Set the root widget onto the display, replacing and clearing any previously set root widgets.
 * @param rootWidget
 */
void WidgetTree::setRootWidget(const std::shared_ptr<Widget>& rootWidget) {
    this->rootWidget = rootWidget;
}

/**
 * Generate an ID that is guaranteed to be unique, given the current tree.
 * @return unique id
 */
std::string WidgetTree::generateId() const {
    std::string id;
    while (id.empty() || isWidgetIdTaken(id)) {
        std::stringstream idStream;
        idStream << rand();
        id = idStream.str();
    }
    return id;
}

/**
 * Get any widget in the tree of widgets which intersects with the provided coordinate.
 *
 * @param x coordinate
 * @param y coordinate
 * @return set of widgets under coordinate
 */
std::set<std::shared_ptr<Widget>> WidgetTree::getWidgetsUnderCoordinate(uint32_t x, uint32_t y) const {
    std::set<std::shared_ptr<Widget>> widgetsUnderCoordinate;
    if (isRootWidgetSet() && getRootWidget()->containsPoint(x, y)) {
        CompositeType compositeType = getRootWidget()->getCompositeType();
        if (compositeType == READ_WRITE_COMPOSITE) {
            auto readWriteCompositeRootWidget = std::dynamic_pointer_cast<ReadWriteComposite>(getRootWidget());
            for (const auto& widgetUnderCoordinate : readWriteCompositeRootWidget->getIntersectingDescendants(x, y)) {
                widgetsUnderCoordinate.insert(widgetUnderCoordinate);
            }

            widgetsUnderCoordinate.insert(getRootWidget());
        }
    }

    return widgetsUnderCoordinate;
}

/**
 * Like getWidgetsUnderCoordinate, but also includes widgets internally managed by ReadOnlyComposites.
 */
std::set<std::shared_ptr<const Widget>> WidgetTree::getWidgetsUnderCoordinateInternal(uint32_t x, uint32_t y) const {
    std::set<std::shared_ptr<const Widget>> widgetsUnderCoordinate;
    if (isRootWidgetSet() && getRootWidget()->containsPoint(x, y)) {
        CompositeType compositeType = getRootWidget()->getCompositeType();
        if (compositeType == READ_WRITE_COMPOSITE || compositeType == READ_ONLY_COMPOSITE) {
            auto compositeRootWidget = std::dynamic_pointer_cast<Composite>(getRootWidget());
            for (const auto& widgetUnderCoordinate : compositeRootWidget->getIntersectingDescendants(x, y)) {
                widgetsUnderCoordinate.insert(widgetUnderCoordinate);
            }

            widgetsUnderCoordinate.insert(getRootWidget());
        }
    }

    return widgetsUnderCoordinate;
}

