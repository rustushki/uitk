#include <sstream>
#include <iomanip>
#include "uitk/utility/ColorUtility.hpp"

/**
 * Given a color and a channel ID, return the value at that channel.
 * <p>
 * See ColorUtility's COLOR_CHANNEL_* values for valid channel IDs.
 * @param color
 * @param channelId
 * @return color channel value
 */
int ColorUtility::getChannel(long color, int channelId) const {
    constexpr long maskingValue = CHANNEL_MAX_VALUE;
    long shift = channelId * BITS_PER_BYTE;
    long mask = maskingValue << shift;
    return (color & mask) >> shift;
}

/**
 * Build a color long value given the four channel values (ARGB).
 * @param alpha the alpha channel value
 * @param red the red color channel value
 * @param green the green color channel value
 * @param blue the blue color channel value
 * @return the color as a long
 */
long ColorUtility::buildColorFromChannels(int alpha, int red, int green, int blue) const {
    int color{CHANNEL_MIN_VALUE};
    color += alpha << (COLOR_CHANNEL_ALPHA * BITS_PER_BYTE);
    color += red << (COLOR_CHANNEL_RED * BITS_PER_BYTE);
    color += green << (COLOR_CHANNEL_GREEN * BITS_PER_BYTE);
    color += blue << (COLOR_CHANNEL_BLUE * BITS_PER_BYTE);
    return color;
}

/**
 * Replace a color channel's value within a provided color.
 * @param color the color in which to replace a channel value
 * @param channel the channel to change
 * @param newValue the new color value
 * @return return the new resulting color
 */
long ColorUtility::replaceColorChannel(long color, int channel, int newValue) const {
    if (newValue > CHANNEL_MAX_VALUE) {
        newValue = CHANNEL_MAX_VALUE;
    } else if (newValue < CHANNEL_MIN_VALUE) {
        newValue = CHANNEL_MIN_VALUE;
    }

    int alpha = getChannel(color, COLOR_CHANNEL_ALPHA);
    int red = getChannel(color, COLOR_CHANNEL_RED);
    int green = getChannel(color, COLOR_CHANNEL_GREEN);
    int blue = getChannel(color, COLOR_CHANNEL_BLUE);

    if (channel == COLOR_CHANNEL_ALPHA) {
        alpha = newValue;
    } else if (channel == COLOR_CHANNEL_RED) {
        red = newValue;
    } else if (channel == COLOR_CHANNEL_GREEN) {
        green = newValue;
    } else if (channel == COLOR_CHANNEL_BLUE) {
        blue = newValue;
    }

    return buildColorFromChannels(alpha, red, green, blue);
}

/**
 * Given a string holding a hexadecimal color, convert it to an long.
 *
 * The color string will be in one of two forms:
 *  - "0xAARRBBGG"
 *  - "0xRRBBGG"
 *
 * Where:
 *  - AA = alpha channel
 *  - RR = red channel
 *  - BB = blue channel
 *  - GG = green channel
 *
 * And each is a hexadecimal number ranging from 00 (fully translucent) to FF (fully opaque).
 *
 * In second form, AA is assumed to be FF.
 * @param color string expressing color
 * @return long expressing color
 */
long ColorUtility::convertColorStringToLong(const std::string& colorString) const {
    // Generates an invalid argument if the string is not a hexadecimal number
    long color = std::stol(colorString, nullptr, BASE_HEXADECIMAL);

    // If format == 0xRRGGBB (8 chars), then fully opaque
    // Otherwise, the transparency must be specified in the string
    if (colorString.length() <= 8) {
        color += 0xFF000000L;
    }

    color &= 0xFFFFFFFFL;

    return color;
}

/**
 * Given a long, convert it into a color string.
 * NOTE: the string will always be in the format 0xAARRBBGG (i.e. including an explicit AA).
 * @param color the color represented as a long
 * @return the color string
 */
std::string ColorUtility::convertColorLongToString(long color) const {
    std::stringstream stringstream;
    stringstream << "0x";
    stringstream << std::setfill('0') << std::setw(2) << std::hex << getChannel(color, COLOR_CHANNEL_ALPHA);
    stringstream << std::setfill('0') << std::setw(2) << std::hex << getChannel(color, COLOR_CHANNEL_RED);
    stringstream << std::setfill('0') << std::setw(2) << std::hex << getChannel(color, COLOR_CHANNEL_GREEN);
    stringstream << std::setfill('0') << std::setw(2) << std::hex << getChannel(color, COLOR_CHANNEL_BLUE);
    return stringstream.str();
}

/**
 * Given a color, invert it while preserving the alpha channel.
 * @param color the color
 * @return the inverted color
 */
long ColorUtility::invert(long color) const {
    int alpha = getChannel(color, COLOR_CHANNEL_ALPHA);
    long colorSansAlpha = color & 0xFFFFFFL;
    long invertedColor = ~colorSansAlpha;
    return replaceColorChannel(invertedColor, COLOR_CHANNEL_ALPHA, alpha);
}
