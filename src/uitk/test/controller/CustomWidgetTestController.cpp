#include "uitk/test/controller/CustomWidgetTestController.hpp"

MyCustomLabelWidget::MyCustomLabelWidget(std::shared_ptr<Context> context) : LabelWidget(std::move(context)) {}

void MyCustomLabelWidget::draw(SDL_Renderer* renderer) {
    // Text color is always overriden to be green
    setTextColor(0xFF00FF00);

    LabelWidget::draw(renderer);
}

CustomWidgetTestController::CustomWidgetTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void CustomWidgetTestController::execute() {
    std::shared_ptr<Context> context = getContext();
    context->getWidgetFactory()->registerWidgetType("custom_label", [context] () -> std::shared_ptr<Widget> {
        return std::make_shared<MyCustomLabelWidget>(context);
    });

    getDisplay()->load("testCustomView");

    getDisplay()->present();
}
