#include "uitk/test/controller/ButtonTestController.hpp"

ButtonTestController::ButtonTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void ButtonTestController::execute() {
    getDisplay()->load("testButtonView");

    auto buttonWithText = std::dynamic_pointer_cast<ButtonWidget>(
        getDisplay()->getWidgetById("buttonWithText")
    );
    bool clicked = false;
    buttonWithText->setOnMouseUpCallback([buttonWithText, &clicked] (SDL_Event) mutable {
        if (!clicked) {
            buttonWithText->setText("Clicked");
        }
    });

    auto buttonInvertToggle = std::dynamic_pointer_cast<ButtonWidget>(
        getDisplay()->getWidgetById("buttonInvertToggle")
    );
    buttonInvertToggle->setOnMouseDownCallback([buttonInvertToggle] (SDL_Event) mutable {
        bool invertOnMouseDown = !buttonInvertToggle->isInvertOnMouseDown();

        std::string text = "Color Inversion Enabled";
        if (!invertOnMouseDown) {
            text = "Color Inversion Disabled";
        }
        buttonInvertToggle->setText(text);
        buttonInvertToggle->setInvertOnMouseDown(invertOnMouseDown);
    });

    getDisplay()->present();
}

