#include "uitk/test/controller/AbsoluteArrangerTestController.hpp"

AbsoluteArrangerTestController::AbsoluteArrangerTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void AbsoluteArrangerTestController::execute() {
    getDisplay()->load("testAbsoluteArrangerView");

    int delta = 9;
    auto label = std::dynamic_pointer_cast<LabelWidget>(getDisplay()->getWidgetById("myLabel"));
    label->setOnKeyDownCallback([label, delta] (SDL_Event event) {
        if (event.key.keysym.sym == SDLK_UP) {
            ArrangementData& arrangementData = label->getLayout().getArrangementData();
            arrangementData.setY(arrangementData.getY() - delta);
        }

        if (event.key.keysym.sym == SDLK_DOWN) {
            ArrangementData& arrangementData = label->getLayout().getArrangementData();
            arrangementData.setY(arrangementData.getY() + delta);
        }

        if (event.key.keysym.sym == SDLK_LEFT) {
            ArrangementData& arrangementData = label->getLayout().getArrangementData();
            arrangementData.setX(arrangementData.getX() - delta);
        }

        if (event.key.keysym.sym == SDLK_RIGHT) {
            ArrangementData& arrangementData = label->getLayout().getArrangementData();
            arrangementData.setX(arrangementData.getX() + delta);
        }
    });

    int timesClicked{0};
    label->setOnMouseDownCallback([label, timesClicked] (SDL_Event event) mutable {
        std::stringstream text;
        text << ++timesClicked << " - " << "Label Clicked";
        label->setText(text.str());

    });

    getDisplay()->present();
}

