#include <algorithm>
#include "uitk/arranger/StackingArranger.hpp"
#include "uitk/test/controller/WidgetLayoutTestController.hpp"

WidgetLayoutTestController::WidgetLayoutTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), context) {
    this->context = std::move(context);
}

void WidgetLayoutTestController::execute() {
    getDisplay()->load(VIEW_ID);

    auto box = std::dynamic_pointer_cast<BoxWidget>(getDisplay()->getWidgetById(WIDGET_ID_BOX));
    box->setOnKeyDownCallback([this] (SDL_Event event) {
        if (event.key.keysym.sym == SDLK_RETURN) {
            createWidgetTestCases(event);
        }
    });

    getDisplay()->present();
}

void WidgetLayoutTestController::createWidgetTestCases(SDL_Event event) {
    getDisplay()->load(VIEW_ID);
    auto box = std::dynamic_pointer_cast<BoxWidget>(getDisplay()->getWidgetById(WIDGET_ID_BOX));
    box->setDebugBorderVisible(true);

    box->setOnKeyDownCallback([this, box] (SDL_Event event) {
        int currentWidget = this->currentWidgetType - 1;
        if (currentWidget < WIDGET_TYPE_BOX) {
            currentWidget = WIDGET_TYPE_COUNT - 1;
        }

        if (event.key.keysym.sym == SDLK_RETURN) {
            createWidgetTestCases(event);
        } else if (event.key.keysym.sym == SDLK_KP_PLUS) {
            for (const auto& widget : box->getChildren()) {
                adjustPadding(widget->getLayout().getPadding(), 5);
            }
        } else if (event.key.keysym.sym == SDLK_KP_MINUS) {
            for (const auto& widget : box->getChildren()) {
                adjustPadding(widget->getLayout().getPadding(), -5);
            }
        } else if (event.key.keysym.sym == SDLK_s) {
            if (currentWidget == WIDGET_TYPE_LABEL) {
                for (const auto& widget : box->getChildren()) {
                    auto label = std::dynamic_pointer_cast<LabelWidget>(widget);
                    label->setScaleFont(!label->isScaleFont());
                }
            } else if (currentWidget == WIDGET_TYPE_IMAGE) {
                for (const auto& widget : box->getChildren()) {
                    auto image = std::dynamic_pointer_cast<ImageWidget>(widget);
                    image->setStretch(!image->isStretch());
                }
            } else if (currentWidget == WIDGET_TYPE_BUTTON) {
                for (const auto& widget : box->getChildren()) {
                    auto button = std::dynamic_pointer_cast<ButtonWidget>(widget);
                    button->setScaleFont(!button->isScaleFont());
                }
            } else if (currentWidget == WIDGET_TYPE_MENU) {
                for (const auto& widget : box->getChildren()) {
                    auto menu = std::dynamic_pointer_cast<MenuWidget>(widget);
                    menu->setOptionScaleFont(!menu->isOptionScaleFont());
                }
            }
        } else if (event.key.keysym.sym == SDLK_UP) {
            if (currentWidget == WIDGET_TYPE_BOX) {
                for (const auto& widget : box->getChildren()) {
                    auto box = std::dynamic_pointer_cast<BoxWidget>(widget);
                    box->setIntrinsicHeightPercent(std::min(box->getIntrinsicHeightPercent() + 0.10, 1.0));
                }
            }
        } else if (event.key.keysym.sym == SDLK_DOWN) {
            if (currentWidget == WIDGET_TYPE_BOX) {
                for (const auto& widget : box->getChildren()) {
                    auto box = std::dynamic_pointer_cast<BoxWidget>(widget);
                    box->setIntrinsicHeightPercent(std::max(box->getIntrinsicHeightPercent() - 0.10, 0.0));
                }
            }
        } else if (event.key.keysym.sym == SDLK_RIGHT) {
            if (currentWidget == WIDGET_TYPE_BOX) {
                for (const auto& widget : box->getChildren()) {
                    auto box = std::dynamic_pointer_cast<BoxWidget>(widget);
                    box->setIntrinsicWidthPercent(std::min(box->getIntrinsicWidthPercent() + 0.10, 1.0));
                }
            }
        } else if (event.key.keysym.sym == SDLK_LEFT) {
            if (currentWidget == WIDGET_TYPE_BOX) {
                for (const auto& widget : box->getChildren()) {
                    auto box = std::dynamic_pointer_cast<BoxWidget>(widget);
                    box->setIntrinsicWidthPercent(std::max(box->getIntrinsicWidthPercent() - 0.10, 0.0));
                }
            }
        }
    });

    for (int gridY = ROW_SCALE_BEHAVIOR_STRETCH_HORIZONTAL; gridY < ROW_SCALE_BEHAVIOR_COUNT; gridY++) {
        for (int gridX = COL_EXTERNAL_ALIGNMENT_0; gridX < COL_EXTERNAL_ALIGNMENT_COUNT; gridX++) {
            std::stringstream id;
            id << currentWidgetType << gridX << gridY;
            std::shared_ptr<Widget> widget = buildWidget(currentWidgetType, id.str());
            applyLayout(widget, gridX, gridY);
            box->addChild(widget);
        }
    }

    if (++currentWidgetType >= WIDGET_TYPE_COUNT) {
        currentWidgetType = WIDGET_TYPE_BOX;
    }
}

std::shared_ptr<Widget> WidgetLayoutTestController::buildWidget(int widgetType, const std::string& id) const {
    std::shared_ptr<Widget> widget;
    switch (widgetType) {
        case WIDGET_TYPE_BOX:
            widget = buildBox();
            break;
        case WIDGET_TYPE_RECTANGLE:
            widget = buildRectangle();
            break;
        case WIDGET_TYPE_IMAGE:
            widget = buildImage();
            break;
        case WIDGET_TYPE_LABEL:
            widget = buildLabel();
            break;
        case WIDGET_TYPE_BUTTON:
            widget = buildButton();
            break;
        case WIDGET_TYPE_MENU:
            widget = buildMenu();
            break;
    }

    widget->setId(id);
    widget->show();
    return widget;
}

void WidgetLayoutTestController::applyLayout(const std::shared_ptr<Widget>& widget, int gridX, int gridY) const {
    Layout layout = widget->getLayout();
    layout.setPadding(Padding(LAYOUT_VALUE_PADDING));
    if (gridY == ROW_SCALE_BEHAVIOR_STRETCH_HORIZONTAL) {
        layout.setScaleBehavior(ScaleBehavior::STRETCH_HORIZONTAL);
    } else if (gridY == ROW_SCALE_BEHAVIOR_STRETCH_VERTICAL) {
        layout.setScaleBehavior(ScaleBehavior::STRETCH_VERTICAL);
    } else if (gridY == ROW_SCALE_BEHAVIOR_STRETCH_BOTH) {
        layout.setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
    } else if (gridY == ROW_SCALE_BEHAVIOR_SCALE) {
        layout.setScaleBehavior(ScaleBehavior::SCALE);
    } else if (gridY == ROW_SCALE_BEHAVIOR_SCALE_NONE) {
        layout.setScaleBehavior(ScaleBehavior::SCALE_NONE);
    }

    if (gridX == COL_EXTERNAL_ALIGNMENT_0) {
        layout.getExternalAlignment().setHorizontal(HorizontalAlignment::LEFT);
        layout.getExternalAlignment().setVertical(VerticalAlignment::TOP);
    } else if (gridX == COL_EXTERNAL_ALIGNMENT_1) {
        layout.getExternalAlignment().setHorizontal(HorizontalAlignment::CENTER);
        layout.getExternalAlignment().setVertical(VerticalAlignment::TOP);
    } else if (gridX == COL_EXTERNAL_ALIGNMENT_2) {
        layout.getExternalAlignment().setHorizontal(HorizontalAlignment::RIGHT);
        layout.getExternalAlignment().setVertical(VerticalAlignment::TOP);
    } else if (gridX == COL_EXTERNAL_ALIGNMENT_3) {
        layout.getExternalAlignment().setHorizontal(HorizontalAlignment::LEFT);
        layout.getExternalAlignment().setVertical(VerticalAlignment::TOP);
    } else if (gridX == COL_EXTERNAL_ALIGNMENT_4) {
        layout.getExternalAlignment().setHorizontal(HorizontalAlignment::LEFT);
        layout.getExternalAlignment().setVertical(VerticalAlignment::CENTER);
    } else if (gridX == COL_EXTERNAL_ALIGNMENT_5) {
        layout.getExternalAlignment().setHorizontal(HorizontalAlignment::LEFT);
        layout.getExternalAlignment().setVertical(VerticalAlignment::BOTTOM);
    }

    layout.getArrangementData().setGridX(gridX);
    layout.getArrangementData().setGridY(gridY);
    widget->setLayout(layout);
}

std::shared_ptr<Widget> WidgetLayoutTestController::buildBox() const {
    auto widget = std::make_shared<BoxWidget>(context);
    widget->setBackgroundColor(PROPERTY_VALUE_BACKGROUND_COLOR);
    widget->setIntrinsicWidthPercent(1.0);
    widget->setIntrinsicHeightPercent(1.0);
    return widget;
}

std::shared_ptr<Widget> WidgetLayoutTestController::buildRectangle() const {
    auto widget = std::make_shared<RectangleWidget>(context);
    widget->setBackgroundColor(PROPERTY_VALUE_BACKGROUND_COLOR);
    widget->setRectangleWidth(25);
    widget->setRectangleHeight(25);
    return widget;
}

std::shared_ptr<Widget> WidgetLayoutTestController::buildImage() const {
    auto widget = std::make_shared<ImageWidget>(context);
    widget->setBackgroundColor(PROPERTY_VALUE_BACKGROUND_COLOR);
    widget->setAnimationName("animation_button_icon");
    widget->setStretch(PROPERTY_VALUE_SCALE_STRETCH);
    return widget;
}

std::shared_ptr<Widget> WidgetLayoutTestController::buildLabel() const {
    auto widget = std::make_shared<LabelWidget>(context);
    widget->setBackgroundColor(PROPERTY_VALUE_BACKGROUND_COLOR);
    widget->setTextColor(PROPERTY_VALUE_TEXT_COLOR);
    widget->setFontName(PROPERTY_VALUE_FONT_NAME);
    widget->setFontSize(PROPERTY_VALUE_FONT_SIZE);
    widget->setScaleFont(PROPERTY_VALUE_SCALE_STRETCH);
    widget->setText("Label");
    return widget;
}

std::shared_ptr<Widget> WidgetLayoutTestController::buildButton() const {
    auto widget = std::make_shared<ButtonWidget>(context);
    widget->setBackgroundColor(PROPERTY_VALUE_BACKGROUND_COLOR);
    widget->setTextColor(PROPERTY_VALUE_TEXT_COLOR);
    widget->setFontName(PROPERTY_VALUE_FONT_NAME);
    widget->setFontSize(PROPERTY_VALUE_FONT_SIZE);
    widget->setScaleFont(PROPERTY_VALUE_SCALE_STRETCH);
    widget->setIntrinsicWidthPercent(0.50);
    widget->setIntrinsicHeightPercent(0.50);
    widget->setText("Click");
    return widget;
}

std::shared_ptr<Widget> WidgetLayoutTestController::buildMenu() const {
    auto widget = std::make_shared<MenuWidget>(context);
    widget->setOptionFontSize(PROPERTY_VALUE_FONT_SIZE);
    widget->setOptionAlignment(HorizontalAlignment::CENTER);
    widget->setOptionBackgroundColor(PROPERTY_VALUE_BACKGROUND_COLOR);
    widget->setOptionTextColor(PROPERTY_VALUE_TEXT_COLOR);
    widget->setOptionCursorTextColor(PROPERTY_VALUE_OPTION_TEXT_COLOR);
    widget->setOptionFontName(PROPERTY_VALUE_FONT_NAME);
    widget->setOptionScaleFont(PROPERTY_VALUE_SCALE_STRETCH);
    widget->setBackgroundColor(PROPERTY_VALUE_BACKGROUND_COLOR);
    widget->setOrientation(Orientation::VERTICAL);
    widget->addOption("option1", "Option 1");
    widget->addOption("option2", "Option 2");
    widget->addOption("option3", "Option 3");
    widget->addOption("option4", "Option 4");
    widget->addOption("option5", "Option 5");
    widget->setDownArrowImage("animation_down_arrow");
    widget->setUpArrowImage("animation_up_arrow");
    widget->setRightArrowImage("animation_right_arrow");
    widget->setLeftArrowImage("animation_left_arrow");
    widget->setVisibleOptionCount(3);
    widget->setIntrinsicWidthPercent(0.50);
    widget->setIntrinsicHeightPercent(0.50);
    return widget;
}

void WidgetLayoutTestController::adjustPadding(Padding& padding, int amount) {
    padding.setTop(padding.getTop() + amount);
    padding.setBottom(padding.getBottom() + amount);
    padding.setLeft(padding.getLeft() + amount);
    padding.setRight(padding.getRight() + amount);
}
