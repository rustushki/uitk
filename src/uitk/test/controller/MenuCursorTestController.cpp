#include "uitk/test/controller/MenuCursorTestController.hpp"

MenuCursorTestController::MenuCursorTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void MenuCursorTestController::execute() {
    getDisplay()->load("testMenuCursorView");

    auto menu = std::dynamic_pointer_cast<MenuWidget>(getDisplay()->getWidgetById("myMenu"));

    menu->setOnOptionSelect("yellowOption", [menu](SDL_Event event) {
        menu->setOptionTextColor(0xFFFFFF00);
    });

    menu->setOnOptionSelect("redOption", [menu](SDL_Event event) {
        menu->setOptionTextColor(0xFFFF0000);
    });

    menu->setOnOptionSelect("greenOption", [menu](SDL_Event event) {
        menu->setOptionTextColor(0xFF00FF00);
    });

    menu->setOnOptionSelect("blueOption", [menu](SDL_Event event) {
        menu->setOptionTextColor(0xFF0000FF);
    });

    menu->setOnOptionSelect("purpleOption", [menu](SDL_Event event) {
        menu->setOptionTextColor(0xFFFF00FF);
    });

    getDisplay()->present();
}
