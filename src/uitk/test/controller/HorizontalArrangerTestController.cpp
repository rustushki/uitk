#include "uitk/test/controller/HorizontalArrangerTestController.hpp"
#include "uitk/type/HorizontalAlignment.hpp"

HorizontalArrangerTestController::HorizontalArrangerTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void HorizontalArrangerTestController::execute() {
    getDisplay()->load("testHorizontalArrangerView");

    int state{0};
    std::shared_ptr<Display> display = getDisplay();
    auto box = std::dynamic_pointer_cast<BoxWidget>(getDisplay()->getWidgetById("horizontalArrangerBox"));
    box->setOnKeyUpCallback([&state, display] (SDL_Event) mutable {
        if (state == 0) {
            state++;
            display->getWidgetById("rectangleRed")->getLayout().getArrangementData()
                .setHorizontalAlignment(HorizontalAlignment::LEFT);
            display->getWidgetById("rectangleGreen")->getLayout().getArrangementData()
                .setHorizontalAlignment(HorizontalAlignment::LEFT);
            display->getWidgetById("rectangleBlue")->getLayout().getArrangementData()
                .setHorizontalAlignment(HorizontalAlignment::LEFT);
        } else if (state == 1) {
            state++;
            display->getWidgetById("rectangleRed")->getLayout().getArrangementData()
                .setHorizontalAlignment(HorizontalAlignment::CENTER);
            display->getWidgetById("rectangleGreen")->getLayout().getArrangementData()
                .setHorizontalAlignment(HorizontalAlignment::CENTER);
            display->getWidgetById("rectangleBlue")->getLayout().getArrangementData()
                .setHorizontalAlignment(HorizontalAlignment::CENTER);
        } else if (state == 2) {
            state++;
            display->getWidgetById("rectangleRed")->getLayout().getArrangementData()
                .setHorizontalAlignment(HorizontalAlignment::RIGHT);
            display->getWidgetById("rectangleGreen")->getLayout().getArrangementData()
                .setHorizontalAlignment(HorizontalAlignment::RIGHT);
            display->getWidgetById("rectangleBlue")->getLayout().getArrangementData()
                .setHorizontalAlignment(HorizontalAlignment::RIGHT);
        }
    });

    getDisplay()->present();
}



