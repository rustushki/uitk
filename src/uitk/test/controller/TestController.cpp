#include "uitk/test/controller/TestController.hpp"

TestController::TestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context) {
    this->display = display;
    this->context = context;
}

const std::shared_ptr<Display> TestController::getDisplay() const {
    return display;
}

const std::shared_ptr<Context> TestController::getContext() const {
    return context;
}
