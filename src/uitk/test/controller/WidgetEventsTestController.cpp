#include "uitk/test/controller/WidgetEventsTestController.hpp"

WidgetEventsTestController::WidgetEventsTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void WidgetEventsTestController::execute() {
    getDisplay()->load("testWidgetEventsView");

    box = std::dynamic_pointer_cast<BoxWidget>(
        getDisplay()->getWidgetById("box")
    );

    rectangle = std::dynamic_pointer_cast<RectangleWidget>(
        getDisplay()->getWidgetById("rectangle")
    );

    boxForLabel = std::dynamic_pointer_cast<BoxWidget>(
        getDisplay()->getWidgetById("boxForLabel")
    );

    hoveredIdsLabel = std::dynamic_pointer_cast<LabelWidget>(
        getDisplay()->getWidgetById("hoveredIdsLabel")
    );

    pointContainmentBehaviorLabel = std::dynamic_pointer_cast<LabelWidget>(
        getDisplay()->getWidgetById("pointContainmentBehaviorLabel")
    );

	rectangle->setOnKeyDownCallback([this] (SDL_Event) {
        rectangle->setBackgroundColor(0xFFFF0000);
	});

	rectangle->setOnKeyUpCallback([this] (SDL_Event) {
        rectangle->setBackgroundColor(0xFF555555);
	});

	rectangle->setOnMouseDownCallback([this] (SDL_Event) {
        rectangle->setBackgroundColor(0xFF00FF00);
	});

	rectangle->setOnMouseUpCallback([this] (SDL_Event) {
        rectangle->setBackgroundColor(0xFF555555);
	});

	rectangle->setOnMouseHoverCallback([this] (SDL_Event) {
        rectangle->setBackgroundColor(0xFF0000FF);
        addHoverId(rectangle);
	});

	rectangle->setOnMouseHoverOutCallback([this] (SDL_Event) {
        rectangle->setBackgroundColor(0xFF555555);
        deleteHoverId(rectangle);
	});

    hoveredIdsLabel->setOnMouseHoverCallback([this] (SDL_Event) {
        addHoverId(hoveredIdsLabel);
    });

    hoveredIdsLabel->setOnMouseHoverOutCallback([this] (SDL_Event) {
        deleteHoverId(hoveredIdsLabel);
    });

    box->setOnMouseHoverCallback([this] (SDL_Event) {
        addHoverId(box);
    });

    box->setOnMouseHoverOutCallback([this] (SDL_Event) {
        deleteHoverId(box);
    });

    boxForLabel->setOnMouseHoverCallback([this] (SDL_Event) {
        addHoverId(boxForLabel);
    });

    boxForLabel->setOnMouseHoverOutCallback([this] (SDL_Event) {
        deleteHoverId(boxForLabel);
    });

    pointContainmentBehaviorLabel->setOnMouseHoverCallback([this] (SDL_Event) {
        addHoverId(pointContainmentBehaviorLabel);
    });

    pointContainmentBehaviorLabel->setOnMouseHoverOutCallback([this] (SDL_Event) {
        deleteHoverId(pointContainmentBehaviorLabel);
    });

    pointContainmentBehaviorLabel->setOnMouseDownCallback([this] (SDL_Event) {
        PointContainmentBehavior current = boxForLabel->getPointContainmentBehavior();
        PointContainmentBehavior next = cyclePointContainmentBehavior(current);
        boxForLabel->setPointContainmentBehavior(next);
    });

    getDisplay()->setOnFrameCallback([this] () -> bool {
        updateHoveredIdsLabelText();
        updatePointContainmentBehaviorLabelText();
        return false;
    });


    getDisplay()->present();
}

void WidgetEventsTestController::addHoverId(const std::shared_ptr<Widget>& widget) {
    hoveredIds.insert(widget->getId());
}

void WidgetEventsTestController::deleteHoverId(const std::shared_ptr<Widget>& widget) {
    hoveredIds.erase(widget->getId());
}

void WidgetEventsTestController::updateHoveredIdsLabelText() {
    std::string text{"HIDs: "};
    bool first = true;
    for (const auto& id : hoveredIds) {
        if (!first) {
            text += ", ";
        } else {
            first = false;
        }
        text += id;
    }
    hoveredIdsLabel->setText(text);
}

void WidgetEventsTestController::updatePointContainmentBehaviorLabelText() {
    PointContainmentBehavior behavior = boxForLabel->getPointContainmentBehavior();
    pointContainmentBehaviorLabel->setText("PCB: " + convertBehaviorToString(behavior));
}

PointContainmentBehavior WidgetEventsTestController::cyclePointContainmentBehavior(PointContainmentBehavior current) {
    PointContainmentBehavior next = BOTH;
    if (current == BOTH) {
        next = PARENT_ONLY;
    } else if (current == PARENT_ONLY) {
        next = CHILDREN_ONLY;
    } else if (current == CHILDREN_ONLY) {
        next = BOTH;
    }
    return next;
}

std::string WidgetEventsTestController::convertBehaviorToString(PointContainmentBehavior pointContainmentBehavior) {
    std::string asString{""};
    if (pointContainmentBehavior == BOTH) {
        asString = "both";
    } else if (pointContainmentBehavior == PARENT_ONLY) {
        asString = "parent_only";
    } else if (pointContainmentBehavior == CHILDREN_ONLY) {
        asString = "children_only";
    }
    return asString;
}
