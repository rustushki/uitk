#include "uitk/test/controller/DisplayTestController.hpp"

DisplayTestController::DisplayTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void DisplayTestController::execute() {
    getDisplay()->load("testDisplayView");
    auto label = std::dynamic_pointer_cast<LabelWidget>(getDisplay()->getWidgetById("myLabel"));
    getDisplay()->setOnFrameCallback([label] () -> bool {
        int redness = (rand() % (0xFF - 0x99)) + 0x99;
        label->setTextColor((redness << 16) + 0x3333);
        return false;
    });

    label->setOnKeyDownCallback([label] (SDL_Event) {
        label->setText("I must have focus");
    });

    getDisplay()->present();
}

