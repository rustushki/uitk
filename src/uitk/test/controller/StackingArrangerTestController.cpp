#include "uitk/test/controller/StackingArrangerTestController.hpp"

StackingArrangerTestController::StackingArrangerTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void StackingArrangerTestController::execute() {
    getDisplay()->load("testStackingArrangerView");
    getDisplay()->present();
}
