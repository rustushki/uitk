#include "uitk/test/controller/ColorTestController.hpp"

ColorTestController::ColorTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void ColorTestController::execute() {
    getDisplay()->load("testColorView");

    auto colorChannelLabel = std::dynamic_pointer_cast<LabelWidget>(getDisplay()->getWidgetById("colorChannelLabel"));
    auto rectangle = std::dynamic_pointer_cast<RectangleWidget>(getDisplay()->getWidgetById("rectangle"));
    auto colorValueLabel = std::dynamic_pointer_cast<LabelWidget>(getDisplay()->getWidgetById("colorValueLabel"));

    // Set accurate initial values for the labels
    colorChannelLabel->setText(buildLabelTextFromChannel(currentChannel));
    colorValueLabel->setText(colorUtility.convertColorLongToString(rectangle->getBackgroundColor()));

    // Adjust the color channel when the rectangle is clicked
    rectangle->setOnMouseUpCallback([this, colorChannelLabel] (SDL_Event) {
        currentChannel = getNextChannel(currentChannel);
        colorChannelLabel->setText(buildLabelTextFromChannel(currentChannel));
    });

    // Adjust the color when + or - is pressed
    rectangle->setOnKeyUpCallback([this, rectangle, colorValueLabel] (SDL_Event event) {
        // Determine the amount, positive or negative, to adjust the select color channel
        int net{50};
        int amount{0};
        if (event.key.keysym.sym == SDLK_KP_PLUS) {
            amount = net;
        } else if (event.key.keysym.sym == SDLK_KP_MINUS) {
            amount = -net;
        }

        // Adjust the color channel by the amount and obtain the resulting color
        int currentColor = rectangle->getBackgroundColor();
        int channelValue = colorUtility.getChannel(currentColor, currentChannel);
        channelValue += amount;
        long newColor = colorUtility.replaceColorChannel(currentColor, currentChannel, channelValue);

        // Set the new color as the background color of the rectangle and the text value of the color label
        rectangle->setBackgroundColor(newColor);
        colorValueLabel->setText(colorUtility.convertColorLongToString(rectangle->getBackgroundColor()));
    });

    getDisplay()->present();
}

int ColorTestController::getNextChannel(int currentChannel) const {
    int newChannel{ColorUtility::COLOR_CHANNEL_ALPHA};

    if (currentChannel == ColorUtility::COLOR_CHANNEL_RED) {
        newChannel = ColorUtility::COLOR_CHANNEL_GREEN;
    } else if (currentChannel == ColorUtility::COLOR_CHANNEL_GREEN) {
        newChannel = ColorUtility::COLOR_CHANNEL_BLUE;
    } else if (currentChannel == ColorUtility::COLOR_CHANNEL_BLUE) {
        newChannel = ColorUtility::COLOR_CHANNEL_ALPHA;
    } else if (currentChannel == ColorUtility::COLOR_CHANNEL_ALPHA) {
        newChannel = ColorUtility::COLOR_CHANNEL_RED;
    }

    return newChannel;
}

std::string ColorTestController::buildLabelTextFromChannel(int currentChannel) const {
    std::string text = "invalid channel ID";

    if (currentChannel == ColorUtility::COLOR_CHANNEL_RED) {
        text = "red";
    } else if (currentChannel == ColorUtility::COLOR_CHANNEL_GREEN) {
        text = "green";
    } else if (currentChannel == ColorUtility::COLOR_CHANNEL_BLUE) {
        text = "blue";
    } else if (currentChannel == ColorUtility::COLOR_CHANNEL_ALPHA) {
        text = "alpha";
    }

    return text;
}
