#include "uitk/test/controller/MenuOptionsTestController.hpp"

MenuOptionsTestController::MenuOptionsTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void MenuOptionsTestController::execute() {
    getDisplay()->load("testMenuOptionsView");

    auto menu = std::dynamic_pointer_cast<MenuWidget>(getDisplay()->getWidgetById("myMenu"));

    menu->setOnKeyUpCallback([menu](SDL_Event event) {
        HorizontalAlignment currentOptionAlignment = menu->getOptionAlignment();
        HorizontalAlignment newOptionAlignment{currentOptionAlignment};

        if (event.key.keysym.sym == SDLK_RIGHT) {
            if (currentOptionAlignment == HorizontalAlignment::CENTER) {
                newOptionAlignment = HorizontalAlignment::RIGHT;
            } else if (currentOptionAlignment == HorizontalAlignment::LEFT) {
                newOptionAlignment = HorizontalAlignment::CENTER;
            }
        } else if (event.key.keysym.sym == SDLK_LEFT) {
            if (currentOptionAlignment == HorizontalAlignment::CENTER) {
                newOptionAlignment = HorizontalAlignment::LEFT;
            } else if (currentOptionAlignment == HorizontalAlignment::RIGHT) {
                newOptionAlignment = HorizontalAlignment::CENTER;
            }
        } else if (event.key.keysym.sym == SDLK_h) {
            menu->setOrientation(Orientation::HORIZONTAL);
        } else if (event.key.keysym.sym == SDLK_v) {
            menu->setOrientation(Orientation::VERTICAL);
        } else if (event.key.keysym.sym == SDLK_s) {
            menu->setOptionScaleFont(!menu->isOptionScaleFont());
            menu->setOptionScaleIcon(!menu->isOptionScaleIcon());
        }

        menu->setOptionAlignment(newOptionAlignment);
    });

    menu->setOnOptionSelect("option1", [menu](SDL_Event event) {
        menu->setOptionBackgroundColor(0xFFFF0000);
    });

    menu->setOnOptionSelect("option2", [menu](SDL_Event event) {
        menu->setOptionBackgroundColor(0xFF0000FF);
    });

    menu->setOnOptionSelect("option3", [menu](SDL_Event event) {
        menu->setBackgroundColor(0xFF999999);
        menu->setOptionBackgroundColor(0xFF999999);
    });

    menu->setOnOptionSelect("option4", [menu](SDL_Event event) {
        menu->setOptionBackgroundColor(0xFF00FF00);
    });

    menu->setOnOptionSelect("option5", [menu](SDL_Event event) {
        menu->setBackgroundColor(0xFF000000);
        menu->setOptionBackgroundColor(0xFF000000);
    });

    getDisplay()->present();
}
