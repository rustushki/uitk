#include "uitk/test/controller/LabelTestController.hpp"

LabelTestController::LabelTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void LabelTestController::execute() {
    getDisplay()->load("testLabelView");

    auto label = std::dynamic_pointer_cast<LabelWidget>(getDisplay()->getWidgetById("myLabel"));

    int state{0};
    label->setOnKeyUpCallback([label, &state] (SDL_Event) mutable {
        if (state == 0) {
            state++;
            label->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
            label->setScaleFont(true);
            label->setText("scaled font size");
        } else if (state == 1) {
            state++;
            label->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
            label->setScaleFont(false);
            label->setText("-1pt font size");
            label->setFontSize(-1);
        } else if (state == 2) {
            state++;
            label->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
            label->setScaleFont(false);
            label->setText("1000pt font size");
            label->setFontSize(1000);
        } else if (state == 3) {
            state++;
            label->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
            label->setScaleFont(false);
            label->setText("zero font size");
            label->setFontSize(0);
        } else if (state == 4) {
            state++;
            label->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
            label->setScaleFont(true);
            label->setText("press and hold any mouse button");
        } else if (state == 8) {
            state++;
            label->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
            label->setScaleFont(true);
            label->setText("scaled text with excessive padding");
            label->getLayout().getPadding().setRight(10000);
            label->getLayout().getPadding().setLeft(10000);
        }
    });

    label->setOnMouseDownCallback([label, &state] (SDL_Event) mutable {
        if (state == 5) {
            state++;
            label->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
            label->setScaleFont(true);
            label->setText("release the mouse button");
        }
    });

    label->setOnMouseUpCallback([label, &state] (SDL_Event) mutable {
        if (state == 6) {
            state++;
            label->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
            label->setScaleFont(true);
            label->setText("press and hold any keyboard button");
        }
    });

    label->setOnKeyDownCallback([label, &state] (SDL_Event) mutable {
        if (state == 7) {
            state++;
            label->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
            label->setScaleFont(true);
            label->setText("release the keyboard button");
        }
    });

    getDisplay()->present();
}
