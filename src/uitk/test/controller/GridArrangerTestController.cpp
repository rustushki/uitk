#include "uitk/arranger/GridArranger.hpp"
#include "uitk/test/controller/GridArrangerTestController.hpp"

GridArrangerTestController::GridArrangerTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void GridArrangerTestController::execute() {
    getDisplay()->load("testGridArrangerView");

    auto gridArrangerBox = std::dynamic_pointer_cast<BoxWidget>(getDisplay()->getWidgetById("gridArrangerBox"));
    auto tricolorBox = std::dynamic_pointer_cast<BoxWidget>(getDisplay()->getWidgetById("tricolorBox"));
    gridArrangerBox->setOnKeyUpCallback([gridArrangerBox, tricolorBox] (SDL_Event) mutable {
        int newGridX{tricolorBox->getLayout().getArrangementData().getGridX()};
        int newGridY{tricolorBox->getLayout().getArrangementData().getGridY()};

        newGridX++;
        if (newGridX >= gridArrangerBox->getArrangerData().getGridWidth()) {
            newGridX = 0; 
            newGridY++;
        }

        if (newGridY >= gridArrangerBox->getArrangerData().getGridHeight()) {
            newGridY = 0;
        }

        tricolorBox->getLayout().getArrangementData().setGridX(newGridX);
        tricolorBox->getLayout().getArrangementData().setGridY(newGridY);
        
    });

    getDisplay()->present();
}
