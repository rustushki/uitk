#include "uitk/test/controller/VerticalArrangerTestController.hpp"
#include "uitk/type/VerticalAlignment.hpp"

VerticalArrangerTestController::VerticalArrangerTestController(std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) : TestController(std::move(display), std::move(context)) {}

void VerticalArrangerTestController::execute() {
    getDisplay()->load("testVerticalArrangerView");

    int state{0};
    std::shared_ptr<Display> display = getDisplay();
    auto box = std::dynamic_pointer_cast<BoxWidget>(getDisplay()->getWidgetById("verticalArrangerBox"));
    box->setOnKeyUpCallback([&state, display] (SDL_Event) mutable {
        if (state == 0) {
            state++;
            display->getWidgetById("rectangleRed")->getLayout().getArrangementData()
                .setVerticalAlignment(VerticalAlignment::TOP);
            display->getWidgetById("rectangleGreen")->getLayout().getArrangementData()
                .setVerticalAlignment(VerticalAlignment::TOP);
            display->getWidgetById("rectangleBlue")->getLayout().getArrangementData()
                .setVerticalAlignment(VerticalAlignment::TOP);
        } else if (state == 1) {
            state++;
            display->getWidgetById("rectangleRed")->getLayout().getArrangementData()
                .setVerticalAlignment(VerticalAlignment::CENTER);
            display->getWidgetById("rectangleGreen")->getLayout().getArrangementData()
                .setVerticalAlignment(VerticalAlignment::CENTER);
            display->getWidgetById("rectangleBlue")->getLayout().getArrangementData()
                .setVerticalAlignment(VerticalAlignment::CENTER);
        } else if (state == 2) {
            state++;
            display->getWidgetById("rectangleRed")->getLayout().getArrangementData()
                .setVerticalAlignment(VerticalAlignment::BOTTOM);
            display->getWidgetById("rectangleGreen")->getLayout().getArrangementData()
                .setVerticalAlignment(VerticalAlignment::BOTTOM);
            display->getWidgetById("rectangleBlue")->getLayout().getArrangementData()
                .setVerticalAlignment(VerticalAlignment::BOTTOM);
        }
    });

    getDisplay()->present();
}


