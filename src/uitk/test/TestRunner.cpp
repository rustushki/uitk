#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <thread>
#include <vector>
#include "nlohmann/json.hpp"
#include "uitk/test/controller/AbsoluteArrangerTestController.hpp"
#include "uitk/test/controller/ButtonTestController.hpp"
#include "uitk/test/controller/ColorTestController.hpp"
#include "uitk/test/controller/Controller.hpp"
#include "uitk/test/controller/CustomWidgetTestController.hpp"
#include "uitk/test/controller/DisplayTestController.hpp"
#include "uitk/test/controller/GridArrangerTestController.hpp"
#include "uitk/test/controller/HorizontalArrangerTestController.hpp"
#include "uitk/test/controller/LabelTestController.hpp"
#include "uitk/test/controller/MenuCursorTestController.hpp"
#include "uitk/test/controller/MenuOptionsTestController.hpp"
#include "uitk/test/controller/StackingArrangerTestController.hpp"
#include "uitk/test/controller/VerticalArrangerTestController.hpp"
#include "uitk/test/controller/WidgetEventsTestController.hpp"
#include "uitk/test/controller/WidgetLayoutTestController.hpp"
#include "uitk/utility/JsonAccessor.hpp"

using json = nlohmann::json;

static const std::string FIELD_TEST_NAME = "testName";
static const std::string FIELD_INSTRUCTIONS = "instructions";
static const std::string FIELD_ACCEPTANCE_CONDITIONS = "acceptanceConditions";

std::unique_ptr<Controller> buildController(std::string testName, std::shared_ptr<Display> display,
        std::shared_ptr<Context> context) {
    std::unique_ptr<Controller> controller = nullptr;
    if (testName == "display") {
        controller = std::make_unique<DisplayTestController>(display, context);
    } else if (testName == "widget_events") {
        controller = std::make_unique<WidgetEventsTestController>(display, context);
    } else if (testName == "widget_layout") {
        controller = std::make_unique<WidgetLayoutTestController>(display, context);
    } else if (testName == "menu_cursor") {
        controller = std::make_unique<MenuCursorTestController>(display, context);
    } else if (testName == "menu_options") {
        controller = std::make_unique<MenuOptionsTestController>(display, context);
    } else if (testName == "label") {
        controller = std::make_unique<LabelTestController>(display, context);
    } else if (testName == "custom") {
        controller = std::make_unique<CustomWidgetTestController>(display, context);
    } else if (testName == "absolute_arranger") {
        controller = std::make_unique<AbsoluteArrangerTestController>(display, context);
    } else if (testName == "grid_arranger") {
        controller = std::make_unique<GridArrangerTestController>(display, context);
    } else if (testName == "vertical_arranger") {
        controller = std::make_unique<VerticalArrangerTestController>(display, context);
    } else if (testName == "horizontal_arranger") {
        controller = std::make_unique<HorizontalArrangerTestController>(display, context);
    } else if (testName == "stacking_arranger") {
        controller = std::make_unique<StackingArrangerTestController>(display, context);
    } else if (testName == "button") {
        controller = std::make_unique<ButtonTestController>(display, context);
    } else if (testName == "color") {
        controller = std::make_unique<ColorTestController>(display, context);
    }
    return controller;
}

int main(int argc, char** argv) {
    if (argc < 2) {
        std::cerr << "USAGE: uitk_test [[<test_name>] [check_only]]" << std::endl;
        exit(1);
    }

    bool checkOnly{false};
    if (argc >= 3) {
        std::string checkOnlyArgument{argv[2]};
        if (checkOnlyArgument == "check_only") {
            checkOnly = true;
        }
    }

    std::string testName{argv[1]};

    std::shared_ptr<Display> display = nullptr;
    std::shared_ptr<Context> context = nullptr;

    if (!checkOnly) {
        auto resources = std::filesystem::path("resources");
        context = std::make_shared<Context>(resources);
        display = std::make_shared<Display>(context, "UITK Menu Demo");
    }

    std::shared_ptr<Controller> controller = buildController(testName, display, context);

    if (controller == nullptr) {
        if (!checkOnly) {
            std::stringstream message;
            message << "No controller declared for test " << testName;
            std::cerr << message.str() << std::endl;
        }
        exit(2);
    }

    if (!checkOnly) {
        controller->execute();
    }

    return 0;
}
