#include "uitk/widget/NonComposite.hpp"

/**
 * Constructor.
 * @param context
 */
NonComposite::NonComposite(std::shared_ptr<Context> context) : BackgroundColorWidget(std::move(context)) {}

/**
 * Returns NON_COMPOSITE because this widget has no children.
 * @return NON_COMPOSITE
 */
CompositeType NonComposite::getCompositeType() const {
    return CompositeType::NON_COMPOSITE;
}
