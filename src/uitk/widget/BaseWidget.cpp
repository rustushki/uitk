#include <iostream>
#include "uitk/type/Animation.hpp"
#include "uitk/utility/factory/ArrangementDataFactory.hpp"
#include "uitk/utility/factory/WidgetFactory.hpp"
#include "uitk/widget/BaseWidget.hpp"

/**
 * Constructor.
 * @param context
 */
BaseWidget::BaseWidget(std::shared_ptr<Context> context) {
    this->visible = false;
    this->context = std::move(context);
}

/**
 * Get the ID of the widget.
 * @return id of widget
 */
std::string BaseWidget::getId() const {
    return this->id;
}

/**
 * Set the ID of the widget.
 *
 * Will throw a runtime_error if the provided ID has already been taken by another widget.
 * @param id
 */
void BaseWidget::setId(std::string id) {
    if (this->getContext()->getWidgetFactory()->isWidgetIdTaken(id)) {
        std::stringstream errorMessage;
        errorMessage << UNIQUE_ID_EXPECTED << id << std::endl;
        throw std::runtime_error(errorMessage.str());
    }

    this->id = id;
}

/**
 * Make this widget visible.
 */
void BaseWidget::show() {
    this->setIsVisible(true);
}

/**
 * Make this widget invisible.
 */
void BaseWidget::hide() {
    this->setIsVisible(false);
}

/**
 * Is this widget visible?
 * @return boolean
 */
bool BaseWidget::isVisible() const {
    return this->visible;
}

/**
 * Set whether this widget is visible or not.
 * @param isVisible
 */
void BaseWidget::setIsVisible(bool isVisible) {
    this->visible = isVisible;
}

/**
 * Get the vector of Animations which need to be displayed in this View.
 * @return std::vector<std::unique_ptr<Animation>>&
 */
std::vector<std::unique_ptr<Animation>>& BaseWidget::getAnimations() {
    return animations;
}

/**
 * Add and play an Animation to the vector of those that need to be animated in this View.
 * @param animation
 */
void BaseWidget::addAnimation(std::unique_ptr<Animation> animation) {
    animation->play();
    animations.push_back(std::move(animation));
}

/**
 * Invoke the callback function which handles the mouse up event.
 * @param event
 */
void BaseWidget::onMouseUp(SDL_Event event) {
    this->onMouseUpCallBack(event);
}

/**
 * Set the callback function which will handle the mouse up event.
 * @param newCallBack
 */
void BaseWidget::setOnMouseUpCallback(const std::function<void(SDL_Event)> &newCallBack) {
    this->onMouseUpCallBack = newCallBack;
}

/**
 * Invoke the callback function which handles the mouse down event.
 * @param event
 */
void BaseWidget::onMouseDown(SDL_Event event) {
    this->onMouseDownCallBack(event);
}

/**
 * Set the callback function which will handle the mouse down event.
 * @param newCallBack
 */
void BaseWidget::setOnMouseDownCallback(const std::function<void(SDL_Event)> &newCallBack) {
    this->onMouseDownCallBack = newCallBack;
}

/**
 * Set the callback function which will handle the mouse hover event.
 * @param newCallBack
 */
void BaseWidget::setOnMouseHoverCallback(const std::function<void(SDL_Event)> &newCallBack) {
    this->onMouseHoverCallBack = newCallBack;
}

/**
 * Invoke the callback function which handles the mouse hover event.
 * @param event
 */
void BaseWidget::onMouseHover(SDL_Event event) {
    this->onMouseHoverCallBack(event);
}

/**
 * Set the callback function which will handle the mouse hover out event.
 * @param newCallBack
 */
void BaseWidget::setOnMouseHoverOutCallback(const std::function<void(SDL_Event)> &newCallBack) {
    this->onMouseHoverOutCallBack = newCallBack;
}

/**
 * Invoke the callback function which handles the mouse hover out event.
 * @param event
 */
void BaseWidget::onMouseHoverOut(SDL_Event event) {
    this->onMouseHoverOutCallBack(event);
}

/**
 * Get the height of the view.
 * @return int
 */
int BaseWidget::getArrangedSpaceHeight() const {
    return arrangedSpace.h;
}

/**
 * Get the width of the view.
 * @return int
 */
int BaseWidget::getArrangedSpaceWidth() const {
    return arrangedSpace.w;
}

/**
 * Get the x coordinate of the view.
 * @return int
 */
int BaseWidget::getArrangedSpaceX() const {
    return arrangedSpace.x;
}

/**
 * Get the y coordinate of the view.
 * @return int
 */
int BaseWidget::getArrangedSpaceY() const {
    return arrangedSpace.y;
}

/**
 * Determine if the provided x/y coordinate pair is bounded by the widget.
 * @param x
 * @param y
 * @return is the coordinate pair contain within the bounds of the widget?
 */
bool BaseWidget::containsPoint(int x, int y) const {
    bool containsPoint = false;
    if (x >= getContentX() && x < getContentX() + getContentWidth()) {
        if (y >= getContentY() && y < getContentY() + getContentHeight()) {
            containsPoint = true;
        }
    }
    return containsPoint;
}

/**
 * Set the callback function which will handle the key up event.
 * @param newCallBack
 */
void BaseWidget::setOnKeyUpCallback(const std::function<void(SDL_Event)> &newCallBack) {
    this->onKeyUpCallBack = newCallBack;
}

/**
 * Invoke the callback function which handles the key up event.
 * @param event
 */
void BaseWidget::onKeyUp(SDL_Event event) {
    this->onKeyUpCallBack(event);
}

/**
 * Set the callback function which will handle the key down event.
 * @param newCallBack
 */
void BaseWidget::setOnKeyDownCallback(const std::function<void(SDL_Event)> &newCallBack) {
    this->onKeyDownCallBack = newCallBack;
}

/**
 * Invoke the callback function which handles the key down event.
 * @param event
 */
void BaseWidget::onKeyDown(SDL_Event event) {
    this->onKeyDownCallBack(event);
}

/**
 * Advance the animations associated with the widget.
 */
void BaseWidget::advanceAnimations() {
    for (auto& animation : getAnimations()) {
        animation->advance();
    }
}

/**
 * Is the arranged space different from the last time applyArrangedSpace() was called?
 * @return bool
 */
bool BaseWidget::isArrangedSpaceChanged() const {
    return arrangedSpaceChanged;
}

/**
 * Get the application context.
 * @return std::shared_ptr<Context>
 */
std::shared_ptr<Context> BaseWidget::getContext() const {
    return this->context;
}

/**
 * Decorate this widget by applying the properties provided.
 * @param properties
 */
void BaseWidget::decorate(const json& properties) {
    JsonAccessor accessor;

    setIsVisible(
        accessor.optional<bool>(properties, KEY_NAME_VISIBLE, KEY_DEFAULT_VISIBLE)
    );
}

/**
 * Set the layout data for this widget.
 * @param layout the layout data
 */
void BaseWidget::setLayout(Layout layout) {
    this->layout = layout;
}

/**
 * Get the layout data for this widget.
 * @return the layout data
 */
Layout& BaseWidget::getLayout() {
    return layout;
}

/**
 * Get the layout data for this widget. Const friendly.
 * @return the layout data
 */
const Layout& BaseWidget::getLayout() const {
    return layout;
}

/**
 * Log a warning for this widget.
 *
 * Includes the provided message and the widget's ID using a standard format. Meant to be used by subclasses.
 * @param message the warning message
 */
void BaseWidget::logWarning(const std::string& message) const {
    std::cerr << "Warning: (Widget ID = " << getId() << ") " << message << std::endl;
}

/**
 * Use the layout data to determine the width of the widget within the arranged space.
 * @return width of the widget
 */
int BaseWidget::getContentWidth() const {
    int width{0};

    const Padding& padding = getLayout().getPadding();
    int paddingLeft{pixelConverter.convert(padding.getLeft(), getArrangedSpaceWidth())};
    int paddingRight{pixelConverter.convert(padding.getRight(), getArrangedSpaceWidth())};
    int paddingTop{pixelConverter.convert(padding.getTop(), getArrangedSpaceWidth())};
    int paddingBottom{pixelConverter.convert(padding.getBottom(), getArrangedSpaceWidth())};

    int baseWidth{getArrangedSpaceWidth() - paddingLeft - paddingRight};
    int baseHeight{getArrangedSpaceHeight() - paddingTop - paddingBottom};

    const ScaleBehavior scaleBehavior = getLayout().getScaleBehavior();
    if (scaleBehavior == ScaleBehavior::SCALE_NONE) {
        width = getIntrinsicWidth();
    } else if (scaleBehavior == ScaleBehavior::SCALE) {
        width = getIntrinsicWidth() * getMaximumScaleForArrangedSpace(baseWidth, baseHeight);
    } else if (scaleBehavior == ScaleBehavior::STRETCH_BOTH) {
        width = baseWidth;
    } else if (scaleBehavior == ScaleBehavior::STRETCH_HORIZONTAL) {
        width = baseWidth;
    } else if (scaleBehavior == ScaleBehavior::STRETCH_VERTICAL) {
        width = getIntrinsicWidth();
    }

    return width;
}

/**
 * Use the layout data to determine the height of the widget within the arranged space.
 * @return height of the widget
 */
int BaseWidget::getContentHeight() const {
    int height{0};

    const Padding& padding = getLayout().getPadding();
    int paddingLeft{pixelConverter.convert(padding.getLeft(), getArrangedSpaceHeight())};
    int paddingRight{pixelConverter.convert(padding.getRight(), getArrangedSpaceHeight())};
    int paddingTop{pixelConverter.convert(padding.getTop(), getArrangedSpaceHeight())};
    int paddingBottom{pixelConverter.convert(padding.getBottom(), getArrangedSpaceHeight())};

    int baseWidth{getArrangedSpaceWidth() - paddingLeft - paddingRight};
    int baseHeight{getArrangedSpaceHeight() - paddingTop - paddingBottom};

    const ScaleBehavior scaleBehavior = getLayout().getScaleBehavior();
    if (scaleBehavior == ScaleBehavior::SCALE_NONE) {
        height = getIntrinsicHeight();
    } else if (scaleBehavior == ScaleBehavior::SCALE) {
        height = getIntrinsicHeight() * getMaximumScaleForArrangedSpace(baseWidth, baseHeight);
    } else if (scaleBehavior == ScaleBehavior::STRETCH_BOTH) {
        height = baseHeight;
    } else if (scaleBehavior == ScaleBehavior::STRETCH_HORIZONTAL) {
        height = getIntrinsicHeight();
    } else if (scaleBehavior == ScaleBehavior::STRETCH_VERTICAL) {
        height = baseHeight;
    }

    return height;
}

/**
 * Use the layout data to determine the x location of the widget within the arranged space.
 * @return x location of the widget
 */
int BaseWidget::getContentX() const {
    int x{0};

    const Layout& layout = getLayout();
    const Padding& padding = layout.getPadding();
    const Alignment& externalAlignment = layout.getExternalAlignment();

    HorizontalAlignment horizontalAlignment = externalAlignment.getHorizontal();
    if (horizontalAlignment == HorizontalAlignment::LEFT) {
        int paddingLeft{pixelConverter.convert(padding.getLeft(), getArrangedSpaceWidth())};
        x = getArrangedSpaceX() + paddingLeft;
    } else if (horizontalAlignment == HorizontalAlignment::CENTER) {
        x = getArrangedSpaceX() + (getArrangedSpaceWidth() - getContentWidth()) / 2;
    } else if (horizontalAlignment == HorizontalAlignment::RIGHT) {
        int paddingRight{pixelConverter.convert(padding.getRight(), getArrangedSpaceWidth())};
        x = getArrangedSpaceX() + getArrangedSpaceWidth() - getContentWidth() - paddingRight;
    }

    return x;
}

/**
 * Use the layout data to determine the y location of the widget within the arranged space.
 * @return y location of the widget
 */
int BaseWidget::getContentY() const {
    int y{0};

    const Layout& layout = getLayout();
    const Padding& padding = layout.getPadding();
    const Alignment& externalAlignment = layout.getExternalAlignment();

    VerticalAlignment verticalAlignment = externalAlignment.getVertical();
    if (verticalAlignment == VerticalAlignment::TOP) {
        int paddingTop{pixelConverter.convert(padding.getTop(), getArrangedSpaceHeight())};
        y = getArrangedSpaceY() + paddingTop;
    } else if (verticalAlignment == VerticalAlignment::CENTER) {
        y = getArrangedSpaceY() + (getArrangedSpaceHeight() - getContentHeight()) / 2;
    } else if (verticalAlignment == VerticalAlignment::BOTTOM) {
        int paddingBottom{pixelConverter.convert(padding.getBottom(), getArrangedSpaceHeight())};
        y = getArrangedSpaceY() + getArrangedSpaceHeight() - getContentHeight() - paddingBottom;
    }

    return y;
}

/**
 * Return a rectangle representing the bounds of the arranged space.
 * @return the dimensions of the arranged space
 */
SDL_Rect BaseWidget::getArrangedSpace() const {
    return arrangedSpace;
}

/**
 * Return a rectangle representing the bounds of the widget
 * @return the dimensions of the widget
 */
SDL_Rect BaseWidget::getContentSpace() const {
    return SDL_Rect{getContentX(), getContentY(), getContentWidth(), getContentHeight()};
}

/**
 * Compute the maximum scaling factor (horizontal or vertical) which can be applied to both dimensions and still fit
 * within the arranged space.
 * @param baseWidth - arranged width less padding
 * @param baseHeight - arranged height less padding
 * @return maximum scaling factor
 */
double BaseWidget::getMaximumScaleForArrangedSpace(int baseWidth, int baseHeight) const {
    double scaleHorizontal{0.0};
    double scaleVertical{0.0};

    if (getIntrinsicWidth() > 0) {
        scaleHorizontal = static_cast<double>(baseWidth) / static_cast<double>(getIntrinsicWidth());
    }

    if (getIntrinsicHeight() > 0) {
        scaleVertical = static_cast<double>(baseHeight) / static_cast<double>(getIntrinsicHeight());
    }

    return std::min(scaleHorizontal, scaleVertical);
}

/**
 * Apply the provided arranged space to this widget.
 *
 * This is also where layout and arranged space changes are detected in between redraws.
 * @param arrangedSpace the space into which this widget should be drawn
 */
void BaseWidget::applyArrangedSpace(SDL_Rect arrangedSpace) {
    this->layoutChanged = this->layout != oldLayout;
    this->oldLayout = this->layout;
    this->arrangedSpaceChanged = !SDL_RectEquals(&(this->arrangedSpace), &arrangedSpace);
    this->arrangedSpace = arrangedSpace;
}

bool BaseWidget::isLayoutChanged() const {
    return this->layoutChanged;
}
