#include "uitk/arranger/Arranger.hpp"
#include "uitk/utility/factory/ArrangerFactory.hpp"
#include "uitk/widget/Composite.hpp"

/**
 * Constructor.
 * @param context
 */
Composite::Composite(std::shared_ptr<Context> context) : BackgroundColorWidget(std::move(context)) {
    getLayout().setScaleBehavior(STRETCH_BOTH);
}

/**
 * Get the Arranger.
 * @return std::shared_ptr<Arranger>
 */
std::shared_ptr<const Arranger> Composite::getArranger() const {
    return arranger;
}

/**
 * Draw each of the child widgets.
 *
 * If the debug borders are enabled, this will also draw those in their specified color around each child's arranged
 * space.
 * @param renderer
 */
void Composite::draw(SDL_Renderer* renderer) {
    BackgroundColorWidget::draw(renderer);
    for (const auto& child : getChildrenHelper()) {
        if (child->isVisible()) {
            child->draw(renderer);
        }

        if (isDebugBorderVisible()) {
            drawDebugBorder(renderer, child->getArrangedSpace());
        }
    }
}

/**
 * Decorate this widget by applying the properties provided.
 * @param properties
 */
void Composite::decorate(const json& properties) {
    BackgroundColorWidget::decorate(properties);

    JsonAccessor accessor;

    setIntrinsicWidthPercent(
        accessor.optional<double>(properties, KEY_NAME_INTRINSIC_WIDTH_PERCENT, KEY_DEFAULT_INTRINSIC_WIDTH_PERCENT)
    );

    setIntrinsicHeightPercent(
        accessor.optional<double>(properties, KEY_NAME_INTRINSIC_HEIGHT_PERCENT, KEY_DEFAULT_INTRINSIC_HEIGHT_PERCENT)
    );
}

/**
 * Draw a border around the provided dimensions.
 *
 * This is meant only for debugging purposes--not for proper styling. Use properties to style widgets. Use
 * setDebugBorderColor() to specify the color of this border.
 * @param renderer the SDL renderer used for drawing
 * @param arrangedSpace the dimensions to draw the debug border around
 */
void Composite::drawDebugBorder(SDL_Renderer* renderer, SDL_Rect arrangedSpace) {
    ColorUtility colorUtility;

    unsigned int borderColor = getDebugBorderColor();
    unsigned int red = colorUtility.getChannel(borderColor, ColorUtility::COLOR_CHANNEL_RED);
    unsigned int green = colorUtility.getChannel(borderColor, ColorUtility::COLOR_CHANNEL_GREEN);
    unsigned int blue = colorUtility.getChannel(borderColor, ColorUtility::COLOR_CHANNEL_BLUE);
    unsigned int alpha = colorUtility.getChannel(borderColor, ColorUtility::COLOR_CHANNEL_ALPHA);

    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
    SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);

    SDL_Rect borderLine;

    // Top
    borderLine.x = arrangedSpace.x;
    borderLine.y = arrangedSpace.y;
    borderLine.w = arrangedSpace.w;
    borderLine.h = 1;
    SDL_RenderFillRect(renderer, &borderLine);

    // Bottom
    borderLine.x = arrangedSpace.x;
    borderLine.y = arrangedSpace.y + arrangedSpace.h - 1;
    borderLine.w = arrangedSpace.w;
    borderLine.h = 1;
    SDL_RenderFillRect(renderer, &borderLine);

    // Left
    borderLine.x = arrangedSpace.x;
    borderLine.y = arrangedSpace.y;
    borderLine.w = 1;
    borderLine.h = arrangedSpace.h;
    SDL_RenderFillRect(renderer, &borderLine);

    // Right
    borderLine.x = arrangedSpace.x + arrangedSpace.w - 1;
    borderLine.y = arrangedSpace.y;
    borderLine.w = 1;
    borderLine.h = arrangedSpace.h;
    SDL_RenderFillRect(renderer, &borderLine);
}

/**
 * Set the color of the debug border.
 * @param debugBorderColor color of debug border
 */
void Composite::setDebugBorderColor(long debugBorderColor) {
    this->debugBorderColor = debugBorderColor;
}

/**
 * Get the color of the debug border.
 * @return color of debug border
 */
long Composite::getDebugBorderColor() {
    return debugBorderColor;
}

/**
 * Set if the debug border is to be drawn.
 * @param debugBorderVisible defaults to false
 */
void Composite::setDebugBorderVisible(bool debugBorderVisible) {
    this->debugBorderVisible = debugBorderVisible;
}

/**
 * Return true if the debug border is to be drawn.
 * @return is the debug border visible?
 */
bool Composite::isDebugBorderVisible() {
    return debugBorderVisible;
}

/**
 * Return the intrinsic width of the widget.
 *
 * BoxWidgets' intrinsic widths are always equal to the intinsic width percentage of arranged space width minus the left
 * and right padding.
 * @return intrinsic width
 */
int Composite::getIntrinsicWidth() const {
    Padding padding = getLayout().getPadding();
    return getIntrinsicWidthPercent() * (getArrangedSpaceWidth() - padding.getLeft().getValue() - padding.getRight().getValue());
}

/**
 * Return the intrinsic height of the widget.
 *
 * BoxWidgets' intrinsic heights are always equal to the intinsic height percentage of arranged space height minus the
 * top and bottom padding.
 * @return intrinsic height
 */
int Composite::getIntrinsicHeight() const {
    Padding padding = getLayout().getPadding();
    return getIntrinsicHeightPercent() * (getArrangedSpaceHeight() - padding.getTop().getValue() - padding.getBottom().getValue());
}

/**
 * Apply the provided arranged space to this widget.
 *
 * For Composites, all children have their arranged spaces applied given the provided arranged space and the configured
 * arranger.
 * @param arrangedSpace the space into which this widget should be drawn
 */
void Composite::applyArrangedSpace(SDL_Rect arrangedSpace) {
    BaseWidget::applyArrangedSpace(arrangedSpace);

    if (getArranger() == nullptr) {
        throw std::runtime_error(ERROR_ARRANGER_NOT_CONFIGURED);
    }

    for (const auto& child : getChildrenHelper()) {
        SDL_Rect childArrangedSpace = getArranger()->determineChildPlacement(this, child.get());
        if (child->isVisible()) {
            child->applyArrangedSpace(childArrangedSpace);
        }
    }
}

/**
 * Return the percentage of the arranged space width which the composite should occupy.
 *
 * @return instrinsic width percent
 */
double Composite::getIntrinsicWidthPercent() const {
    return intrinsicWidthPercent;
}

/**
 * Return the percentage of the arranged space height which the composite should occupy.
 *
 * @return instrinsic height percent
 */
double Composite::getIntrinsicHeightPercent() const {
    return intrinsicHeightPercent;
}

/**
 * Set the percentage of the arranged space width which the composite should occupy.
 *
 * @param instrinsicWidthPercent percentage of arranged space width
 */
void Composite::setIntrinsicWidthPercent(double intrinsicWidthPercent) {
    this->intrinsicWidthPercent = intrinsicWidthPercent;
}

/**
 * Set the percentage of the arranged space height which the composite should occupy.
 *
 * @param instrinsicHeightPercent percentage of arranged space height
 */
void Composite::setIntrinsicHeightPercent(double intrinsicHeightPercent) {
    this->intrinsicHeightPercent = intrinsicHeightPercent;
}

/**
 * See Composite::getChildrenHelper(), but children are readonly.
 */
const std::vector<std::shared_ptr<const Widget>> Composite::getChildren() const {
    std::vector<std::shared_ptr<const Widget>> readOnlyChildren;

    for (auto& child : getChildrenHelper()) {
        readOnlyChildren.push_back(child);
    }

    return readOnlyChildren;
}

/**
 * Get the arranger type.
 *
 * Should always return a valid type. Defaults to stacking.
 * @return arranger type
 */
std::string Composite::getArrangerType() const {
    std::string arrangerType{""};
    if (arranger != nullptr) {
        arrangerType = arranger->getType();
    }
    return arrangerType;
}

/**
 * Set arranger type.
 * @param arrangerType
 */
void Composite::setArrangerTypeHelper(const std::string& arrangerType) {
    if (getArrangerType() != arrangerType) {
        ArrangerFactory arrangerFactory;
        this->arranger = arrangerFactory.build(arrangerType);
    }
}

/**
 * Get arranger data.
 *
 * This data describes the arranged space itself. ArrangementData, by contrast, describes how a widget instance will fit
 * within an arranged space.
 * @return arranger data
 */
ArrangerData Composite::getArrangerData() const {
    return this->arrangerData;
}

/**
 * Advance animations for each of the child widgets.
 */
void Composite::advanceAnimations() {
    BaseWidget::advanceAnimations();

    for (const auto& child : getChildrenHelper()) {
        child->advanceAnimations();
    }
}

ArrangerData& Composite::getArrangerDataHelper() {
    return this->arrangerData;
}

/**
 * Get list of child widgets.
 * These widgets are drawn based on their arrangement data and on the selected arranger.
 * @return list of child widgets
 */
const std::vector<std::shared_ptr<Widget>>& Composite::getChildrenHelper() const {
    return children;
}

/**
 * Get a child by the given ID.
 * Will return nullptr if a child did not exist with the given ID.
 * @param id of widget to find
 * @return the found widget or nullptr if no match
 */
std::shared_ptr<Widget> Composite::getChildByIdHelper(const std::string& id) const {
    std::shared_ptr<Widget> match{nullptr};
    for (const auto& child : children) {
        if (child->getId() == id) {
            match = child;
            break;
        }
    }
    return match;
}

/**
 * Get any widget in the tree of children widgets which intersects with the provided coordinate.
 * Note that this does not include this widget itself.
 * @param x coordinate
 * @param y coordinate
 * @return the set of widgets under the coordinate pair
 */
std::set<std::shared_ptr<Widget>> Composite::getIntersectingDescendantsHelper(uint32_t x, uint32_t y) {
    std::set<std::shared_ptr<Widget>> intersectingDescendents;

    // Iterate in reverse because widgets listed last are stacked on top
    for (auto childItr = children.crbegin(); childItr != children.crend(); ++childItr) {
        auto child = *childItr;

        // Children which are composites can define how they want point intersection to work. The point could be
        // considered to intersect with PARENT_ONLY, CHILDREN_ONLY, or BOTH depending on the point containment behavior
        // property
        bool considerChildIntersecting = false;

        // Absolutely positioned widgets can have children which don't fall within the bounds of the widget, so each
        // of child's children must be check regardless of whether there is an intersection with child itself
        if (child->isVisible() && checkIfComposite(child)) {
            auto childComposite = std::dynamic_pointer_cast<Composite>(child);

            PointContainmentBehavior pointContainmentBehavior = childComposite->getPointContainmentBehavior();
            if (pointContainmentBehavior != PARENT_ONLY) {
                for (const auto& intersectingDescendent : childComposite->getIntersectingDescendantsHelper(x, y)) {
                    intersectingDescendents.insert(intersectingDescendent);
                }
            }

            if (pointContainmentBehavior != CHILDREN_ONLY) {
                considerChildIntersecting = true;
            }
        } else {
            considerChildIntersecting = true;
        }

        // If there is an intersection with child, and it's visible, then add it
        if (considerChildIntersecting && child->isVisible() && child->containsPoint(x, y)) {
            intersectingDescendents.insert(child);
        }

        // If an intersection has been detected, no need to inspect remaining children
        if (!intersectingDescendents.empty()) {
            break;
        }
    }

    return intersectingDescendents;
}

/**
 * Find the first descendent widget managed by this widget (excluding itself) whose ID matches the provided.
 * @param idToFind
 * @return matching descendent or nullptr if no match
 */
std::shared_ptr<Widget> Composite::getDescendantByIdHelper(const std::string& idToFind) {
    std::shared_ptr<Widget> match = getChildByIdHelper(idToFind);
    if (match == nullptr) {
        for (const auto& child : children) {
            if (checkIfComposite(child)) {
                auto childComposite = std::dynamic_pointer_cast<Composite>(child);
                match = childComposite->getDescendantByIdHelper(idToFind);
                if (match != nullptr) {
                    break;
                }
            }
        }
    }

    return match;
}

/**
 * Helper method which checks to see if the provided widget can have child widgets or not.
 * @param widget to check if composite
 * @return true if composite
 */
bool Composite::checkIfComposite(const std::shared_ptr<Widget>& widget) const {
    return widget->getCompositeType() == READ_ONLY_COMPOSITE || widget->getCompositeType() == READ_WRITE_COMPOSITE;
}

/**
 * See Composite::getDescendantByIdHelper(), but results are readonly.
 */
std::shared_ptr<const Widget> Composite::getDescendantById(const std::string& id) {
    return getDescendantByIdHelper(id);
}

/**
 * See Composite::getIntersectingDescendantsHelper(), but widget results are const.
 */
std::set<std::shared_ptr<const Widget>> Composite::getIntersectingDescendants(uint32_t x, uint32_t y) {
    std::set<std::shared_ptr<const Widget>> intersectingDescendents;
    for (auto& child : getIntersectingDescendantsHelper(x, y)) {
        intersectingDescendents.insert(child);
    }

    return intersectingDescendents;
}

/**
 * See Composite::getChildById(), but result is const.
 */
std::shared_ptr<const Widget> Composite::getChildById(const std::string& id) const {
    return getChildByIdHelper(id);
}

void Composite::setArrangerDataHelper(ArrangerData arrangerData) {
    this->arrangerData = arrangerData;
}

/**
 * Add a new child to this widget.
 */
void Composite::addChildHelper(const std::shared_ptr<Widget>& newChild) {
    children.push_back(newChild);
}

/**
 * Remove the child denoted by the provided ID.
 *
 * Will return nullptr if a child did not exist with the given ID.
 * @param idToRemove
 * @return std::shared_ptr<Widget>
 */
std::shared_ptr<Widget> Composite::removeChildHelper(const std::string& id) {
    std::shared_ptr<Widget> match = getChildByIdHelper(id);

    auto oldChildren = children;

    children.clear();
    for (const auto& child : oldChildren) {
        if (child->getId() != id) {
            children.push_back(child);
        }
    }

    return match;
}

/**
 * Get the point containment behavior.
 *
 * Indicates how point-based events (such as mouse events) will be handled with regards to the composite's children.
 * 
 *  parent_only - this composite will receive point events, but none of its children will
 *  children_only - this composite will not receive point events, but any of its children which contain the point will
 *  both - both this composite and any of its children which contain the point will receive point events
 * @return point containment cehavior
 */
PointContainmentBehavior Composite::getPointContainmentBehavior() {
    return pointContainmentBehavior;
}

/**
 * Set the point containment behavior.
 *
 * See getter for details.
 * @param pointContainmentBehavior 
 */
void Composite::setPointContainmentBehaviorHelper(PointContainmentBehavior pointContainmentBehavior) {
    this->pointContainmentBehavior = pointContainmentBehavior;
}
