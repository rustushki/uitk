#include "uitk/utility/ColorUtility.hpp"
#include "uitk/widget/BackgroundColorWidget.hpp"

/**
 * Constructor.
 * @param context
 */
BackgroundColorWidget::BackgroundColorWidget(std::shared_ptr<Context> context) : BaseWidget(std::move(context)) {}

/**
 * Draws a rectangle to the screen using the color field.
 * @param renderer
 */
void BackgroundColorWidget::draw(SDL_Renderer* renderer) {
    int red = colorUtility.getChannel(backgroundColor, ColorUtility::COLOR_CHANNEL_RED);
    int green = colorUtility.getChannel(backgroundColor, ColorUtility::COLOR_CHANNEL_GREEN);
    int blue = colorUtility.getChannel(backgroundColor, ColorUtility::COLOR_CHANNEL_BLUE);
    int alpha = colorUtility.getChannel(backgroundColor, ColorUtility::COLOR_CHANNEL_ALPHA);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);
    SDL_Rect contentSpace = getContentSpace();
    SDL_RenderFillRect(renderer, &contentSpace);
}

/**
 * Sets the rectangle color.
 * @param newColor
 */
void BackgroundColorWidget::setBackgroundColor(long newColor) {
    this->backgroundColor = newColor;
}

/**
 * Get the rectangle's color.
 * @return
 */
long BackgroundColorWidget::getBackgroundColor() const {
    return this->backgroundColor;
}

/**
 * Decorate this widget by applying the properties provided.
 * @param properties
 */
void BackgroundColorWidget::decorate(const json& properties) {
    BaseWidget::decorate(properties);

    JsonAccessor accessor;

    setBackgroundColor(colorUtility.convertColorStringToLong(
        accessor.optional<std::string>(properties, KEY_NAME_BACKGROUND_COLOR,
            KEY_DEFAULT_BACKGROUND_COLOR)
    ));
}
