#include "uitk/widget/ImageWidget.hpp"

/**
 * Constructor.
 * @param context
 */
ImageWidget::ImageWidget(std::shared_ptr<Context> context) : NonComposite(std::move(context)) {}

/**
 * Draw the image onto the renderer.
 * @param renderer
 */
void ImageWidget::draw(SDL_Renderer* renderer) {
    NonComposite::draw(renderer);

    if (animation != nullptr) {
        if (isStretch()) {
            animation->setWidth(getContentWidth());
            animation->setHeight(getContentHeight());
        } else {
            animation->setWidth(animation->getFrameWidth());
            animation->setHeight(animation->getFrameHeight());
        }

        animation->move(getContentX(), getContentY());
        animation->draw(renderer);
    }
}

/**
 * Set the animation name for the animation that should be displayed by the ImageWidget.
 * @param animationName
 */
void ImageWidget::setAnimationName(const std::string& animationName) {
    if (!animationName.empty()) {
        animation = getContext()->getAnimationFactory()->build(animationName);
    } else {
        animation = nullptr;
    }

    this->animationName = animationName;
}

/**
 * Get the animation name for the animation that is displayed by the ImageWidget.
 * @return std::string
 */
std::string ImageWidget::getAnimationName() const {
    return this->animationName;
}

/**
 * Return the intrinsic width of the widget.
 *
 * ImageWidgets' intrinsic widths are equal to the width of the contained animation.
 * @return intrinsic width
 */
int ImageWidget::getIntrinsicWidth() const {
    int width{0};
    if (animation != nullptr) {
        width = this->animation->getFrameWidth();
    }
    return width;
}

/**
 * Return the intrinsic height of the widget.
 *
 * ImageWidgets' intrinsic heights are equal to the width of the contained animation.
 * @return intrinsic height
 */
int ImageWidget::getIntrinsicHeight() const {
    int height{0};
    if (animation != nullptr) {
        height = this->animation->getFrameHeight();
    }
    return height;
}

/**
 * Decorate this widget by applying the properties provided.
 * @param properties
 */
void ImageWidget::decorate(const json& properties) {
    NonComposite::decorate(properties);

    JsonAccessor accessor;

    setAnimationName(
        accessor.optional<std::string>(properties, KEY_NAME_ANIMATION_NAME, KEY_DEFAULT_ANIMATION_NAME)
    );

    setStretch(
        accessor.optional<bool>(properties, KEY_NAME_STRETCH, KEY_DEFAULT_STRETCH)
    );
}

/**
 * Set if the animation is to be stretched across the face of the image.
 * @param stretch is it?
 */
void ImageWidget::setStretch(bool stretch) {
    this->stretch = stretch;
}

/**
 * Return if the animation is to be stretched across the face of the image.
 * @return is it?
 */
bool ImageWidget::isStretch() const {
    return stretch;
}
