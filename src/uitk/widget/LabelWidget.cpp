#include <sstream>
#include "uitk/utility/PathUtility.hpp"
#include "uitk/widget/LabelWidget.hpp"

/**
 * Constructor.
 * @param context
 */
LabelWidget::LabelWidget(std::shared_ptr<Context> context) : NonComposite(std::move(context)) {}

/**
 * Load a font for usage.
 * @param size
 * @return TTF_Font*
 */
TTF_Font* LabelWidget::getFont(uint8_t size) const {
    return TTF_OpenFont((getContext()->getPathUtility()->getFontsPath() / fontName).c_str(), size);
}

/**
 * Sets the text color of the label.
 * @param color
 */
void LabelWidget::setTextColor(long color) {
    textDirty = true;
    textColor = color;
}

/**
 * Sets the text on the label to the value of the provided string.
 */
void LabelWidget::setText(const std::string& text) {
    textDirty = true;
    this->text = text;
}

/**
 * Builds a texture for the text based on the provided font size and other widget properties.
 * @param renderer
 * @return SDL_Texture*
 */
SDL_Texture* LabelWidget::makeTextTexture(SDL_Renderer* renderer, int fontSize) const {
    if (text.empty() || fontName.empty()) {
        return nullptr;
    }

    // Render and return the text surface which fits in the LabelWidget
    TTF_Font* font = getFont(static_cast<uint8_t>(fontSize));
    unsigned char red = colorUtility.getChannel(textColor, ColorUtility::COLOR_CHANNEL_RED);
    unsigned char green = colorUtility.getChannel(textColor, ColorUtility::COLOR_CHANNEL_GREEN);
    unsigned char blue = colorUtility.getChannel(textColor, ColorUtility::COLOR_CHANNEL_BLUE);
    unsigned char alpha = colorUtility.getChannel(textColor, ColorUtility::COLOR_CHANNEL_ALPHA);
    SDL_Color color = {red, green, blue, alpha};
    SDL_Surface* textSurface = TTF_RenderText_Blended(font, text.c_str(), color);
    TTF_CloseFont(font);

    // Confirm that the text surface was created.  If not something is horribly wrong, so die
    if (textSurface == nullptr) {
        std::stringstream message;
        message << "Error drawing text: " << TTF_GetError();
        throw std::runtime_error(message.str());
    }

    SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
    SDL_FreeSurface(textSurface);
    return textTexture;
}

/**
 * Return a font size which will fit along the given axis and fill as much as possible the given size along that axis.
 * <p>
 * @param axis 0 for x axis, 1 for y axis
 * @param size in pixels
 * @return int font size
 */
int LabelWidget::chooseFontSizeToFitAxis(int axis, int size) const {
    // If the provided size is negative, the sizing logic will fail, so use the smallest font size
    unsigned int mi{1};
    if (size > 0) {
        unsigned int lo{1};
        unsigned int hi{255};
        int measuredAxis{0};
        while (lo <= hi) {
            mi = static_cast<int>((lo + hi) / 2);

            measuredAxis = measureAxisAtFontSize(mi, axis);
            if (measuredAxis > size) {
                hi = mi - 1;
            } else if (measuredAxis < size) {
                lo = mi + 1;
            } else {
                break;
            }
        }

        while (measuredAxis > size && mi != 0) {
            measuredAxis = measureAxisAtFontSize(--mi, axis);
        }

        if (mi == 0) {
            mi = 1;
        }
    }

    return mi;
}

/**
 * Return the length of the text along the given axis at the provided font size.
 * @param fontSize
 * @param axis 1 for x axis, 0 for y axis
 * @return int length of the given axis for the provided font size
 */
int LabelWidget::measureAxisAtFontSize(int fontSize, int axis) const {
    int measuredAxis{0};
    TTF_Font* fnt = getFont(fontSize);
    int w{0};
    int h{0};
    TTF_SizeText(fnt, text.c_str(), &w, &h);
    TTF_CloseFont(fnt);

    if (axis == 0) {
        measuredAxis = h;
    } else {
        measuredAxis = w;
    }

    return measuredAxis;
}

/**
 * Set the font used for the label text.
 * @param fontName label text's font
 */
void LabelWidget::setFontName(const std::string& fontName) {
    this->fontName = fontName;
}

/**
 * Given the renderer, draw a label onto it.
 * <p>
 * Maintenance note: If implementing a mutator which affects text appearance, be sure to set textDirty = true.
 * @param renderer
 */
void LabelWidget::draw(SDL_Renderer* renderer) {
    NonComposite::draw(renderer);

    // Mark for rebuild texture if the arranged space has change
    if (!textDirty) {
        textDirty = isArrangedSpaceChanged();
    }

    // Mark for rebuild texture if the layout has changed
    if (!textDirty) {
        textDirty = isLayoutChanged();
    }

    // If marked for rebuild texture, go ahead and rebuild it
    if (textDirty) {
        SDL_Texture* oldTexture = textTexture;
        textTexture = makeTextTexture(renderer, getEffectiveFontSize());

        // Effective font size calculation is dependent on the old texture
        SDL_DestroyTexture(oldTexture);
        textDirty = false;
    }

    // Draw the text centered within the button, obeying the margin.
    // A nullptr textTexture implies the text is a 0-length string.
    if (textTexture != nullptr) {
        // Consider the content space dimensions to be the edge of the label. The texture dimensions represent the edge
        // of the text content. The following code aligns the text content within the content space
        //
        // TODO: This is where face alignment could be implemented. Right now, it is hard coded to center

        // Determine width and height of the text content itself
        int width{0};
        int height{0};
        SDL_QueryTexture(textTexture, nullptr, nullptr, &width, &height);

        // Align the text content to the center within the content space
        int x{getContentX() + (getContentWidth() - width) / 2};
        int y{getContentY() + (getContentHeight() - height) / 2};

        // Determine the rectangle that the text texture will be drawn onto
        SDL_Rect destination = {x, y, width, height};

        // Draw the text into that rectangle
        SDL_RenderCopy(renderer, textTexture, nullptr, &destination);
    }
}

/**
 * Get the text that appears on the label.
 * @return
 */
std::string LabelWidget::getText() {
    return this->text;
}

/**
 * Set the font size.
 * <p>
 * A font size of 0 will cause the text to be automatically sized to fit within the label.
 * @param fontSize
 */
void LabelWidget::setFontSize(uint32_t fontSize) {
    this->textDirty = true;
    this->fontSize = fontSize;
}

/**
 * Get the font size.
 * <p>
 * A font size of 0 indicates that the text will be automatically sized to fit within the label.
 * @return uint32_t
 */
uint32_t LabelWidget::getFontSize() const {
    return fontSize;
}

/**
 * Get the color of the text.
 * @return long
 */
long LabelWidget::getTextColor() const {
    return textColor;
}

/**
 * Get the name of the font used for the label's text.
 * @return std::string
 */
std::string LabelWidget::getFontName() const {
    return fontName;
}

/**
 * Decorate this widget by applying the properties provided.
 * @param properties
 */
void LabelWidget::decorate(const json& properties) {
    NonComposite::decorate(properties);

    JsonAccessor accessor;

    setText(
        accessor.optional<std::string>(properties, KEY_NAME_TEXT, KEY_DEFAULT_TEXT)
    );

    setFontSize(
        accessor.optional<int>(properties, KEY_NAME_FONT_SIZE, KEY_DEFAULT_FONT_SIZE)
    );

    setFontName(
        accessor.require<std::string>(properties, KEY_NAME_FONT_NAME)
    );

    setTextColor(colorUtility.convertColorStringToLong(
        accessor.optional<std::string>(properties, KEY_NAME_TEXT_COLOR, KEY_DEFAULT_TEXT_COLOR)
    ));

    setScaleFont(
        accessor.optional<bool>(properties, KEY_NAME_SCALE_FONT, KEY_DEFAULT_SCALE_FONT)
    );
}

/**
 * Return the intrinsic width of the widget.
 *
 * LabelWidgets' intrinsic widths are always equal to the width of the texture.
 * @return intrinsic width
 */
int LabelWidget::getIntrinsicWidth() const {
    int intrinsicWidth{0};
    if (textTexture != nullptr) {
        SDL_QueryTexture(textTexture, nullptr, nullptr, &intrinsicWidth, nullptr);
    }
    return intrinsicWidth;
}

/**
 * Return the intrinsic height of the widget.
 *
 * LabelWidgets' intrinsic heights are always equal to the height of the texture.
 * @return intrinsic width
 */
int LabelWidget::getIntrinsicHeight() const {
    int intrinsicHeight{0};
    if (textTexture != nullptr) {
        SDL_QueryTexture(textTexture, nullptr, nullptr, nullptr, &intrinsicHeight);
    }
    return intrinsicHeight;
}

/**
 * Set if the font of the text on the label's face is to be scaled.
 * @param scaleFont is it?
 */
void LabelWidget::setScaleFont(bool scaleFont) {
    this->scaleFont = scaleFont;
    textDirty = true;
}

/**
 * Is the text to be scaled to fit the bounds of the label's face?
 * @return is the font to be scaled?
 */
bool LabelWidget::isScaleFont() const {
    return scaleFont;
}

/**
 * Determine the effective font size of the text on the button face.
 *
 * Usually, this is equal to the font size. However, if the font is to be scaled, this can be different.
 * @return effective font size
 */
int LabelWidget::getEffectiveFontSize() const {
    int effectiveFontSize{0};
    if (isScaleFont()) {
        ScaleBehavior scaleBehavior = getLayout().getScaleBehavior();
        if (scaleBehavior == ScaleBehavior::SCALE_NONE) {
            effectiveFontSize = this->getFontSize();
        } else {
            effectiveFontSize = std::min(
                this->chooseFontSizeToFitAxis(1, getContentWidth()),
                this->chooseFontSizeToFitAxis(0, getContentHeight())
            );
        }
    } else {
        effectiveFontSize = this->getFontSize();
    }

    if (effectiveFontSize <= 0) {
        effectiveFontSize = 1;
    }

    return effectiveFontSize;
}
