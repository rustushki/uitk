#include "uitk/utility/factory/ArrangerDataFactory.hpp"
#include "uitk/widget/ReadWriteComposite.hpp"

/**
 * Constructor.
 * @param context
 */
ReadWriteComposite::ReadWriteComposite(std::shared_ptr<Context> context) : Composite(std::move(context)) {
    // An arranger type is required in order for basic behavior to work. Removes need for decorate() to be called
    setArrangerTypeHelper(KEY_DEFAULT_ARRANGER_TYPE);
}

/**
 * Returns READ_WRITE_COMPOSITE because this widget's children and their layouts may be edited externally.
 * @return READ_WRITE_COMPOSITE
 */
CompositeType ReadWriteComposite::getCompositeType() const {
    return READ_WRITE_COMPOSITE;
}

/**
 * See Composite::getChildrenHelper().
 */
const std::vector<std::shared_ptr<Widget>> ReadWriteComposite::getChildren() const {
    return Composite::getChildrenHelper();
}

/**
 * See Composite::getChildByIdHelper().
 */
std::shared_ptr<Widget> ReadWriteComposite::getChildById(const std::string& id) const {
    return Composite::getChildByIdHelper(id);
}

/**
 * See Composite::getIntersectingDescendantsHelper().
 */
std::set<std::shared_ptr<Widget>> ReadWriteComposite::getIntersectingDescendants(uint32_t x, uint32_t y) {
    return Composite::getIntersectingDescendantsHelper(x, y);
}

/**
 * See Composite::getDescendantByIdHelper().
 */
std::shared_ptr<Widget> ReadWriteComposite::getDescendantById(const std::string& id) {
    return Composite::getDescendantByIdHelper(id);
}

/**
 * See Composite::getArrangerDataHelper().
 */
ArrangerData& ReadWriteComposite::getArrangerData() {
    return Composite::getArrangerDataHelper();
}

/**
 * See Composite::setArrangerTypeHelper().
 */
void ReadWriteComposite::setArrangerType(const std::string& arrangerType) {
    Composite::setArrangerTypeHelper(arrangerType);
}

/**
 * See Composite::setArrangerDataHelper().
 */
void ReadWriteComposite::setArrangerData(ArrangerData arrangerData) {
    Composite::setArrangerDataHelper(arrangerData);
}

/**
 * See Composite::addChildHelper().
 */
void ReadWriteComposite::addChild(const std::shared_ptr<Widget>& newChild) {
    Composite::addChildHelper(newChild);
}

/**
 * See Composite::removeChildHelper().
 */
std::shared_ptr<Widget> ReadWriteComposite::removeChild(const std::string& idToRemove) {
    return Composite::removeChildHelper(idToRemove);
}

/**
 * See Widget::decorate()
 */
void ReadWriteComposite::decorate(const json& properties) {
    Composite::decorate(properties);

    JsonAccessor accessor;
    EnumFactory enumFactory;

    setArrangerTypeHelper(
        accessor.optional<std::string>(properties, KEY_NAME_ARRANGER_TYPE, KEY_DEFAULT_ARRANGER_TYPE)
    );

    setPointContainmentBehavior(enumFactory.buildPointContainmentBehavior(
        accessor.optional<std::string>(properties, KEY_NAME_POINT_CONTAINMENT_BEHAVIOR,
            KEY_DEFAULT_POINT_CONTAINMENT_BEHAVIOR))
    );

    auto arrangerDataJson = accessor.optional<json>(properties, KEY_NAME_ARRANGER_DATA, KEY_DEFAULT_ARRANGER_DATA);
    ArrangerDataFactory arrangerDataFactory;
    setArrangerDataHelper(arrangerDataFactory.build(arrangerDataJson));
}

/**
 * See Composite::setPointContainmentBehaviorHelper().
 */
void ReadWriteComposite::setPointContainmentBehavior(PointContainmentBehavior pointContainmentBehavior) {
    Composite::setPointContainmentBehaviorHelper(pointContainmentBehavior);
}
