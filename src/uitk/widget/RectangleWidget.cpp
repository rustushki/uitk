#include "uitk/widget/RectangleWidget.hpp"

/**
 * Constructor.
 * @param context
 */
RectangleWidget::RectangleWidget(std::shared_ptr<Context> context) : NonComposite(std::move(context)) {}

/**
 * Return the intrinsic width of the widget.
 *
 * RectangleWidget's intrinsic width is equal to a set property value called rectangleWidth.
 * @return intrinsic width
 */
int RectangleWidget::getIntrinsicWidth() const {
    return getRectangleWidth();
}

/**
 * Return the intrinsic height of the widget.
 *
 * RectangleWidget's intrinsic height is equal to a set property value called rectangleHeight.
 * @return intrinsic height
 */
int RectangleWidget::getIntrinsicHeight() const {
    return getRectangleHeight();
}

/**
 * Gets the intrinsic width of the rectangle.
 * @return width of rectangle
 */
int RectangleWidget::getRectangleWidth() const {
    return rectangleWidth;
}

/**
 * Gets the intrinsic height of the rectangle.
 * @return height of rectangle
 */
int RectangleWidget::getRectangleHeight() const {
    return rectangleHeight;
}

/**
 * Sets the intrinsic width of the rectangle.
 * @param rectangleWidth width of rectangle
 */
void RectangleWidget::setRectangleWidth(int rectangleWidth) {
    this->rectangleWidth = rectangleWidth;
}

/**
 * Sets the intrinsic height of the rectangle.
 * @param rectangleHeight height of rectangle
 */
void RectangleWidget::setRectangleHeight(int rectangleHeight) {
    this->rectangleHeight = rectangleHeight;
}

/**
 * Decorate this widget by applying the properties provided.
 * @param properties
 */
void RectangleWidget::decorate(const json& properties) {
    NonComposite::decorate(properties);

    JsonAccessor accessor;

    setRectangleWidth(
        accessor.optional<int>(properties, KEY_NAME_RECTANGLE_WIDTH, KEY_DEFAULT_RECTANGLE_WIDTH)
    );

    setRectangleHeight(
        accessor.optional<int>(properties, KEY_NAME_RECTANGLE_HEIGHT, KEY_DEFAULT_RECTANGLE_HEIGHT)
    );
}
