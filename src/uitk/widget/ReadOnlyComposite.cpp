#include "uitk/widget/ReadOnlyComposite.hpp"

/**
 * Constructor.
 * @param context
 */
ReadOnlyComposite::ReadOnlyComposite(std::shared_ptr<Context> context) : Composite(std::move(context)) {}

/**
 * Returns READ_ONLY_COMPOSITE because this widget's children nor their layouts may not be edited externally.
 * @return READ_ONLY_COMPOSITE
 */
CompositeType ReadOnlyComposite::getCompositeType() const {
    return READ_ONLY_COMPOSITE;
}
