#include <sstream>
#include "uitk/arranger/HorizontalArranger.hpp"
#include "uitk/utility/factory/WidgetFactory.hpp"
#include "uitk/widget/ButtonWidget.hpp"

/**
 * Constructor.
 * @param context the system context
 */
ButtonWidget::ButtonWidget(std::shared_ptr<Context> context) : ReadOnlyComposite(std::move(context)) {
    isDepressed = false;
    setArrangerTypeHelper("horizontal");
    getLayout().setPadding(Padding(DEFAULT_CHILD_PADDING));
    addChildHelper(imageWidget);
    addChildHelper(labelWidget);
    layout();
}

/**
 * Draw the button to the screen.
 * @param renderer
 */
void ButtonWidget::draw(SDL_Renderer* renderer) {
    ReadOnlyComposite::draw(renderer);
}

/**
 * Default behavior, but also inverts the text and background colors.
 * @param event
 */
void ButtonWidget::onMouseDown(SDL_Event event) {
    if (!isDepressed) {
        if (invertOnMouseDown) {
            invertColors();
        }
        isDepressed = true;
    }
    BaseWidget::onMouseDown(event);
}

/**
 * Default behavior, but if the button is depressed, it will un-depress it by inverting the text and background colors
 * back to their originals.
 * @param event
 */
void ButtonWidget::onMouseUp(SDL_Event event) {
    if (isDepressed) {
        if (invertOnMouseDown) {
            invertColors();
        }
        isDepressed = false;
    }
    BaseWidget::onMouseUp(event);
}

/**
 * Default behavior, but if the button is depressed, it will un-depress it by inverting the text and background colors
 * back to their originals.
 * @param event
 */
void ButtonWidget::onMouseHoverOut(SDL_Event event) {
    if (isDepressed && this->invertOnMouseDown) {
        invertColors();
        isDepressed = false;
    }
    BaseWidget::onMouseHover(event);
}

/**
 * Decorate this widget by applying the properties provided.
 * @param properties the JSON data
 */
void ButtonWidget::decorate(const json& properties) {
    ReadOnlyComposite::decorate(properties);

    JsonAccessor accessor;
    EnumFactory enumFactory;

    setText(
        accessor.optional<std::string>(properties, KEY_NAME_TEXT, KEY_DEFAULT_TEXT)
    );

    setFontSize(
        accessor.optional<int>(properties, KEY_NAME_FONT_SIZE, KEY_DEFAULT_FONT_SIZE)
    );

    setFontName(
        accessor.require<std::string>(properties, KEY_NAME_FONT_NAME)
    );

    setTextColor(colorUtility.convertColorStringToLong(
        accessor.optional<std::string>(properties, KEY_NAME_TEXT_COLOR, KEY_DEFAULT_TEXT_COLOR)
    ));

    setIconName(
        accessor.optional<std::string>(properties, KEY_NAME_ICON_NAME, KEY_DEFAULT_ICON_NAME)
    );

    setFaceAlignment(enumFactory.buildHorizontalAlignment(
        accessor.optional<std::string>(properties, KEY_NAME_FACE_ALIGNMENT, KEY_DEFAULT_FACE_ALIGNMENT)
    ));

    setScaleFont(
        accessor.optional<bool>(properties, KEY_NAME_SCALE_FONT, KEY_DEFAULT_SCALE_FONT)
    );

    setInvertOnMouseDown(
        accessor.optional<bool>(properties, KEY_NAME_INVERT_ON_MOUSE_DOWN, KEY_DEFAULT_INVERT_ON_MOUSE_DOWN)
    );
}

/**
 * Get the name of the icon used on the button.
 *
 * An empty string means to not display an icon.
 * @return name of the animation to display
 */
std::string ButtonWidget::getIconName() const {
    return imageWidget->getAnimationName();
}

/**
 * Change the icon that appears on the button.
 *
 * An empty string means to not display an icon.
 * @param iconName name of the animation to display
 */
void ButtonWidget::setIconName(const std::string& iconName) {
    imageWidget->setAnimationName(iconName);
    layout();
}

void ButtonWidget::layout() {
    std::string iconName = getIconName();
    if (iconName.empty()) {
        imageWidget->hide();
        imageWidget->getLayout().getArrangementData().setFill(NO_FILL);
        labelWidget->getLayout().getArrangementData().setFill(FULL_FILL);
    } else {
        imageWidget->show();

        if (labelWidget->getText().empty()) {
            imageWidget->getLayout().getArrangementData().setFill(FULL_FILL);
            labelWidget->getLayout().getArrangementData().setFill(NO_FILL);
        } else {
            imageWidget->getLayout().getArrangementData().setFill(IMAGE_FILL);
            labelWidget->getLayout().getArrangementData().setFill(LABEL_FILL);
        }
    }
}

/**
 * Build the label represnting the button text.
 * @return the built label widget
 */
std::shared_ptr<LabelWidget> ButtonWidget::buildLabel() const {
    std::shared_ptr<WidgetFactory> widgetFactory = getContext()->getWidgetFactory();
    auto widget = std::dynamic_pointer_cast<LabelWidget>(widgetFactory->build(WidgetFactory::TYPE_LABEL));
    widget->getLayout().setScaleBehavior(ScaleBehavior::SCALE_NONE);
    widget->setBackgroundColor(getBackgroundColor());
    widget->getLayout().getExternalAlignment().setVertical(VerticalAlignment::CENTER);
    widget->getLayout().getExternalAlignment().setHorizontal(HorizontalAlignment::CENTER);
    widget->getLayout().setPadding(Padding(DEFAULT_CHILD_PADDING));
    widget->getLayout().getArrangementData().setFill(LABEL_FILL);
    widget->getLayout().getArrangementData().setHorizontalAlignment(HorizontalAlignment::CENTER);
    widget->hide();
    return widget;
}

/**
 * Build the image represnting the button icon.
 * @return the built image widget
 */
std::shared_ptr<ImageWidget> ButtonWidget::buildImage() const {
    Padding padding(DEFAULT_CHILD_PADDING);
    padding.setRight(0);

    std::shared_ptr<WidgetFactory> widgetFactory = getContext()->getWidgetFactory();
    auto widget = std::dynamic_pointer_cast<ImageWidget>(widgetFactory->build(WidgetFactory::TYPE_IMAGE));
    widget->setStretch(true);
    widget->getLayout().getArrangementData().setFill(NO_FILL);
    widget->getLayout().getArrangementData().setHorizontalAlignment(HorizontalAlignment::CENTER);
    widget->getLayout().setScaleBehavior(ScaleBehavior::SCALE);
    widget->getLayout().getExternalAlignment().setVertical(VerticalAlignment::CENTER);
    widget->getLayout().getExternalAlignment().setHorizontal(HorizontalAlignment::CENTER);
    widget->getLayout().setPadding(padding);
    widget->hide();

    return widget;
}

/**
 * Set the button text.
 * @param text the text displayed on the face of the button
 */
void ButtonWidget::setText(const std::string& text) {
    labelWidget->setText(text);
    labelWidget->setIsVisible(!labelWidget->getText().empty());
}

/**
 * Get the button text.
 * @return button text
 */
std::string ButtonWidget::getText() const {
    return labelWidget->getText();
}

/**
 * Set the button text color.
 * @param color of the button text
 */
void ButtonWidget::setTextColor(long color) {
    labelWidget->setTextColor(color);
}

/**
 * Get the button text color.
 * @return color of the button text
 */
long ButtonWidget::getTextColor() const {
    return labelWidget->getTextColor();
}

/**
 * Set the button text's font's name.
 * @param font name of the button text
 */
void ButtonWidget::setFontName(const std::string& fontName) {
    labelWidget->setFontName(fontName);
}

/**
 * Get the button text's font's name.
 * @return font name of the button text
 */
std::string ButtonWidget::getFontName() const {
    return labelWidget->getFontName();
}

/**
 * Set the font size of the button text.
 * @param font size of the button text
 */
void ButtonWidget::setFontSize(uint32_t fontSize) {
    labelWidget->setFontSize(fontSize);
}

/**
 * Get the font size of the button text.
 * @return font size of the button text
 */
uint32_t ButtonWidget::getFontSize() const {
    return labelWidget->getFontSize();
}

/**
 * Set the background color of the button.
 * @param background color
 */
void ButtonWidget::setBackgroundColor(long backgroundColor) {
    BackgroundColorWidget::setBackgroundColor(backgroundColor);
    labelWidget->setBackgroundColor(backgroundColor);
    imageWidget->setBackgroundColor(backgroundColor);
}

/**
 * Set the alignment of the text and icon on the face of the button.
 * @param faceAlignment the alignment of the text and icon on the button
 */
void ButtonWidget::setFaceAlignment(HorizontalAlignment faceAlignment) {
    this->faceAlignment = faceAlignment;
    labelWidget->getLayout().getExternalAlignment().setHorizontal(faceAlignment);
    imageWidget->getLayout().getExternalAlignment().setHorizontal(HorizontalAlignment::CENTER);
    labelWidget->getLayout().getExternalAlignment().setVertical(VerticalAlignment::CENTER);
    imageWidget->getLayout().getExternalAlignment().setVertical(VerticalAlignment::CENTER);
}

/**
 * Get the alignment of the text and icon on the face of the button.
 * @return the alignment of the text and icon on the button
 */
HorizontalAlignment ButtonWidget::getFaceAlignment() const {
    return this->faceAlignment;
}

/**
 * Is the text to be scaled to fit the bounds of the button face?
 * @return is the font to be scaled?
 */
bool ButtonWidget::isScaleFont() const {
    return labelWidget->isScaleFont();
}

/**
 * Set if the font of the text on the button face is to be scaled.
 * @param scaleFont is it?
 */
void ButtonWidget::setScaleFont(bool scaleFont) {
    labelWidget->setScaleFont(scaleFont);

    labelWidget->getLayout().setScaleBehavior(SCALE_NONE);

    ScaleBehavior scaleBehavior = getLayout().getScaleBehavior();
    if (scaleFont) {
        if (scaleBehavior == SCALE || scaleBehavior == STRETCH_BOTH) {
            labelWidget->getLayout().setScaleBehavior(STRETCH_BOTH);
        }
    }
}

/**
 * Determine the effective font size of the text on the button face.
 *
 * Usually, this is equal to the font size. However, if the font is to be scaled, this can be different.
 * @return effective font size
 */
int ButtonWidget::getEffectiveFontSize() const {
    return labelWidget->getEffectiveFontSize();
}

/**
 * Is the color inversion behavior enabled?
 *
 * When enabled, the foreground and background colors of the button will invert when the button is mouse-downed.
 * @return color inversion enabled?
 */
bool ButtonWidget::isInvertOnMouseDown() const {
    return invertOnMouseDown;
}

/**
 * Enable or disable the color inversion behavior.
 *
 * When enabled, the foreground and background colors of the button will invert when the button is mouse-downed.
 * @param invertOnMouseDown color inversion enabled?
 */
void ButtonWidget::setInvertOnMouseDown(bool invertOnMouseDown) {
    if (this->invertOnMouseDown != invertOnMouseDown && isDepressed) {
        invertColors();
    }
    this->invertOnMouseDown = invertOnMouseDown;
}

/**
 * Helper method to invert the foreground and background colors.
 */
void ButtonWidget::invertColors() {
    this->setBackgroundColor(colorUtility.invert(this->getBackgroundColor()));
    this->setTextColor(colorUtility.invert(this->getTextColor()));
}
