#include "uitk/arranger/AbsoluteArranger.hpp"
#include "uitk/utility/ColorUtility.hpp"
#include "uitk/widget/BoxWidget.hpp"

/**
 * Constructor.
 * @param context
 */
BoxWidget::BoxWidget(std::shared_ptr<Context> context) : ReadWriteComposite(std::move(context)) {}
