#include <algorithm>
#include "uitk/arranger/GridArranger.hpp"
#include "uitk/utility/factory/WidgetFactory.hpp"
#include "uitk/widget/MenuWidget.hpp"

/**
 * Constructor.
 * @param context
 */
MenuWidget::MenuWidget(std::shared_ptr<Context> context) : ReadOnlyComposite(std::move(context)) {
    addChildHelper(nextArrowImage);
    addChildHelper(precedingArrowImage);
    setArrangerTypeHelper("grid");
    layoutMenu();
}

/**
 * Add a new option to the menu (with icon).
 *
 * The provided ID can be used by setOnOptionSelect() to connect a behavior with the selection of this option. The text
 * provided is displayed on the menu option button.
 * @param optionText text appearing for the option
 * @param optionIconName name of the animation to use for the icon
 */
void MenuWidget::addOptionWithIcon(const std::string& optionId, const std::string& optionText,
        const std::string& optionIconName) {
    std::shared_ptr<WidgetFactory> widgetFactory = getContext()->getWidgetFactory();
    auto image = std::dynamic_pointer_cast<ImageWidget>(widgetFactory->build(WidgetFactory::TYPE_IMAGE));
    image->setAnimationName(optionIconName);
    image->getLayout().setScaleBehavior(ScaleBehavior::SCALE);
    image->setStretch(true);
    image->show();
    images.push_back(image);

    auto button = std::dynamic_pointer_cast<ButtonWidget>(widgetFactory->build(WidgetFactory::TYPE_BUTTON));
    button->setText(optionText);
    button->setFontName(getOptionFontName());
    button->setFontSize(0);
    button->setTextColor(getOptionTextColor());
    button->setFontSize(getOptionFontSize());
    button->setInvertOnMouseDown(false);
    button->show();
    buttons.push_back(button);

    auto box = std::dynamic_pointer_cast<BoxWidget>(widgetFactory->build(WidgetFactory::TYPE_BOX));
    box->setArrangerType("horizontal");
    box->setBackgroundColor(getOptionBackgroundColor());
    box->addChild(image);
    box->addChild(button);
    options.push_back(box);
    addChildHelper(box);

    idToBoxMap[optionId] = box;

    layoutMenu();

    // If the button list was empty before adding this option, the following method call will set the cursor color of
    // the newly added button
    setCursorIndex(getCursorIndex());
}

/**
 * Add a new option to the menu (no icon).
 *
 * The provided ID can be used by setOnOptionSelect() to connect a behavior with the selection of this option. The text
 * provided is displayed on the menu option button.
 * @param optionId ID for finding the option later
 * @param optionText text appearing for the option
 */
void MenuWidget::addOption(const std::string& optionId, const std::string& optionText) {
    addOptionWithIcon(optionId, optionText, "");
}

/**
 * Determine which images to display for the arrows based on menu orientation (up/down vs left/right).
 */
void MenuWidget::reassignArrowImages() {
    if (getOrientation() == Orientation::VERTICAL) {
        if (!upArrowAnimationName.empty()) {
            precedingArrowImage->setAnimationName(upArrowAnimationName);
        }

        if (!downArrowAnimationName.empty()) {
            nextArrowImage->setAnimationName(downArrowAnimationName);
        }
    } else {
        if (!leftArrowAnimationName.empty()) {
            precedingArrowImage->setAnimationName(leftArrowAnimationName);
        }
        if (!rightArrowAnimationName.empty()) {
            nextArrowImage->setAnimationName(rightArrowAnimationName);
        }
    }

}

/**
 * Reassign Grid Slots.
 *
 * The first and last grid slots will respectively be the preceding and next arrows. The slots inbetween will be the
 * options.
 *
 * If the visible option count <= 0, then all options will be made visible and receive a grid position. Otherwise, only
 * the options within the visible window of options will be made visible and receive a grid position. Grid positions are
 * assigned in the order the options were added to the menu.
 */
void MenuWidget::reassignGridSlots() {
    // Grid Size is calculated number of visible options + 1 back arrow + 1 forward arrow

    bool arrowsPresent = false;
    if (precedingArrowImage != nullptr && nextArrowImage != nullptr) {
        arrowsPresent = !precedingArrowImage->getAnimationName().empty();
        arrowsPresent &= !nextArrowImage->getAnimationName().empty();
    }

    int gridSize = getWindowSize();

    if (arrowsPresent) {
        gridSize += 2;
    }

    if (getOrientation() == Orientation::VERTICAL) {
        getArrangerDataHelper().setGridWidth(1);
        getArrangerDataHelper().setGridHeight(gridSize);
    } else {
        getArrangerDataHelper().setGridWidth(gridSize);
        getArrangerDataHelper().setGridHeight(1);
    }

    int visibleOptionCount = getVisibleOptionCount();
    if (visibleOptionCount == 0) {
        visibleOptionCount = options.size();
    }

    int position = 0;
    if (arrowsPresent) {
        assignGridSlot(precedingArrowImage, position);
        position++;
    }


    int windowTopIndex = getWindowTopIndex();
    int windowBottomIndex = std::min((int) options.size(), windowTopIndex + visibleOptionCount);

    for (const auto& option : options) {
        option->hide();
    }

    for (int index = windowTopIndex; index < windowBottomIndex; index++) {
        assignGridSlot(options[index], position++);
        options[index]->show();
    }

    if (arrowsPresent) {
        assignGridSlot(nextArrowImage, position);
    }
}

/**
 * Assign a widget to the grid slot denoted by the position argument.
 *
 * The position will be treated as an x or y depending on menu orientation.
 * @param widget the widget to assign to grid slot
 * @param position the position to assign the widget
 */
void MenuWidget::assignGridSlot(const std::shared_ptr<Widget>& widget, int position) const {
    if (getOrientation() == Orientation::VERTICAL) {
        widget->getLayout().getArrangementData().setGridX(0);
        widget->getLayout().getArrangementData().setGridY(position);
    } else {
        widget->getLayout().getArrangementData().setGridX(position);
        widget->getLayout().getArrangementData().setGridY(0);
    }
}

/**
 * Draw each of the visible buttons on the menu.
 * @param renderer
 * @param arrangedSpace
 */
void MenuWidget::draw(SDL_Renderer* renderer) {
    bool optionTextResizeRequired{false};
    optionTextResizeRequired |= isOptionScaleFont() && isArrangedSpaceChanged();
    optionTextResizeRequired |= oldOrientation != getOrientation();
    if (optionTextResizeRequired) {
        reassignButtonFontSizes();
    }
        reassignButtonFontSizes();

    // Re-position menu elements if the layout has changed
    if (isLayoutChanged() || isArrangedSpaceChanged()) {
        layoutMenu();
    }

    BackgroundColorWidget::draw(renderer);
    ReadOnlyComposite::draw(renderer);
    oldOrientation = getOrientation();
}

/**
 * Set the background color of each of the menu options.
 * @param color
 */
void MenuWidget::setOptionBackgroundColor(long color) {
    this->optionBackgroundColor = color;
    for (const auto& option : options) {
        option->setBackgroundColor(optionBackgroundColor);
    }
}

/**
 * Set the text color of each of the menu options.
 * @param color
 */
void MenuWidget::setOptionTextColor(long color) {
    this->optionTextColor = color;
    for (const auto& button : buttons) {
        button->setTextColor(getOptionTextColor());
    }
}

/**
 * Get the background color of each of the menu options.
 * @return
 */
long MenuWidget::getOptionBackgroundColor() const {
    return optionBackgroundColor;
}

/**
 * Get the text color of each of the menu options.
 * @return
 */
long MenuWidget::getOptionTextColor() const {
    return optionTextColor;
}

/**
 * Set the cursor when the mouse hovers over an option.
 * <p>
 * Afterwards, perform any user defined hover actions.
 * @param event
 */
void MenuWidget::onMouseHover(SDL_Event event) {
    uint16_t buttonIndex = 0;
    auto x = static_cast<uint16_t>(event.motion.x);
    auto y = static_cast<uint16_t>(event.motion.y);
    for (const auto& option : options) {
        if (option->isVisible() && option->containsPoint(x, y)) {
            setCursorIndex(buttonIndex);
            break;
        }

        buttonIndex++;
    }

    BaseWidget::onMouseHover(event);
}

/**
 * Set the cursor index, changing the color of that option and resetting the previously set option's color.
 * <p>
 * The option list starts at index 0.
 * @param newCursorIndex
 */
void MenuWidget::setCursorIndex(uint16_t newCursorIndex) {
    if (!buttons.empty()) {
        buttons[getCursorIndex()]->setTextColor(getOptionTextColor());
    }

    this->cursorIndex = newCursorIndex;

    if (!buttons.empty()) {
        buttons[getCursorIndex()]->setTextColor(getOptionCursorTextColor());
    }
}

/**
 * Get the cursor index.
 * <p>
 * The option list starts at index 0.
 * @return
 */
uint16_t MenuWidget::getCursorIndex() const {
    return cursorIndex;
}

/**
 * Set the color of the option on which the cursor is currently pointing.
 * @param color
 */
void MenuWidget::setOptionCursorTextColor(long color) {
    optionCursorTextColor = color;
    if (!buttons.empty()) {
        buttons[getCursorIndex()]->setTextColor(getOptionCursorTextColor());
    }
}

/**
 * Move the cursor to the next option, wrapping around if on last.
 * <p>
 * If incrementing the cursor causes the cursor to fall outside of the visible window of options, also increment the
 * visible window of options.
 */
void MenuWidget::incrementCursor() {
    long newCursorIndex = getCursorIndex() + 1;
    if (newCursorIndex >= buttons.size()) {
        newCursorIndex = 0;
    }

    setCursorIndex(static_cast<uint16_t>(newCursorIndex));

    if (isCursorOutsideWindow()) {
        incrementWindowTopIndex();
    }

    layoutMenu();
}

/**
 * Move the cursor to the previous option, wrapping around if on first.
 * <p>
 * If decrementing the cursor causes the cursor to fall outside of the visible window of options, also decrement the
 * visible window of options.
 */
void MenuWidget::decrementCursor() {
    long newCursorIndex = getCursorIndex() - 1;
    if (newCursorIndex < 0) {
        newCursorIndex = static_cast<long>(buttons.size()) - 1;
    }

    setCursorIndex(static_cast<uint16_t>(newCursorIndex));

    if (isCursorOutsideWindow()) {
        decrementWindowTopIndex();
    }

    layoutMenu();
}

/**
 * Key Up Handler.
 * <p>
 *  Enter - invokes the current cursor's option's select callback
 *  Arrow Up - Move decrements the cursor
 *  Arrow Down - Increments the cursor
 * @param event
 */
void MenuWidget::onKeyUp(SDL_Event event) {
    // Enter activates the internal button
    if (event.key.keysym.sym == SDLK_RETURN) {
        buttons[getCursorIndex()]->onMouseUp(event);

    // Decrement the Cursor on UP
    } else if (event.key.keysym.sym == SDLK_UP) {
        decrementCursor();

    // Increment the Cursor on DOWN
    } else if (event.key.keysym.sym == SDLK_DOWN) {
        incrementCursor();
    }

    BaseWidget::onKeyUp(event);
}

/**
 * Sets the count of visible options.
 * <p>
 * If 0, it will attempt to display all options on the menu.
 * <p>
 * This triggers the options to be resized to fit inside the area of the menu.
 * @param count
 */
void MenuWidget::setVisibleOptionCount(uint16_t count) {
    visibleOptionCount = count;
    layoutMenu();
}

/**
 * Get the count of visible options.
 * @return uint16_t
 */
uint16_t MenuWidget::getVisibleOptionCount() const {
    return visibleOptionCount;
}

/**
 * Create the menu's preceding arrow.
 */
std::shared_ptr<ImageWidget> MenuWidget::createPrecedingArrow() {
    std::shared_ptr<WidgetFactory> widgetFactory = getContext()->getWidgetFactory();
    auto widget = std::dynamic_pointer_cast<ImageWidget>(widgetFactory->build(WidgetFactory::TYPE_IMAGE));
    widget->setBackgroundColor(getOptionBackgroundColor());
    widget->setStretch(false);
    widget->getLayout().setScaleBehavior(ScaleBehavior::SCALE);
    widget->getLayout().getExternalAlignment().setHorizontal(getOptionAlignment());
    widget->getLayout().getExternalAlignment().setVertical(VerticalAlignment::CENTER);
    widget->setOnMouseUpCallback([this](SDL_Event event) {
        decrementCursor();
    });
    widget->show();
    return widget;
}

/**
 * Create the menu's next arrow.
 */
std::shared_ptr<ImageWidget> MenuWidget::createNextArrow() {
    std::shared_ptr<WidgetFactory> widgetFactory = getContext()->getWidgetFactory();
    auto widget = std::dynamic_pointer_cast<ImageWidget>(widgetFactory->build(WidgetFactory::TYPE_IMAGE));
    widget->setBackgroundColor(getOptionBackgroundColor());
    widget->setStretch(false);
    widget->getLayout().setScaleBehavior(ScaleBehavior::SCALE);
    widget->getLayout().getExternalAlignment().setHorizontal(getOptionAlignment());
    widget->getLayout().getExternalAlignment().setVertical(VerticalAlignment::CENTER);
    widget->setOnMouseUpCallback([this](SDL_Event event) {
        incrementCursor();
    });
    widget->show();
    return widget;
}

/**
 * Returns true if scrolling isn't possible downwards.
 * @return boolean
 */
bool MenuWidget::cantScrollDown() const {
    return isScrollingEverNecessary() && (getWindowTopIndex() + 1 > buttons.size() - getWindowSize());
}

/**
 * Returns true if scrolling isn't possible upwards.
 * @return boolean
 */
bool MenuWidget::cantScrollUp() const {
    return isScrollingEverNecessary() && (getWindowTopIndex() - 1 < 0);
}

/**
 * Returns true if the count of options exceeds the number of visible options at a time.
 * <p>
 * The only exception is if the count of visible options is 0. In this case, all options will be displayed and fit
 * within the vertical space allotted.
 * @return boolean
 */
bool MenuWidget::isScrollingEverNecessary() const {
    bool isScrollingEverNecessary = true;

    // If the count of visible options is 0 or less, then all options will be fit into the area
    if (getVisibleOptionCount() <= 0) {
        isScrollingEverNecessary = false;
    }

    // If the count of visible options is more than the options available
    if (getWindowSize() >= buttons.size()) {
        isScrollingEverNecessary = false;
    }

    return isScrollingEverNecessary;
}

/**
 * Set the top index of the visible window of options.
 * <p>
 * This index denotes the first visible option to be displayed on the menu.
 * @param index
 */
void MenuWidget::setWindowTopIndex(uint16_t index) {
    this->windowTopIndex = index;
}

/**
 * Decrement the top index of the visible window of options.
 * <p>
 * If decrementing causes the top index to be negative, wrap around to the last position of the menu which still has a
 * full window-size worth of options remaining.
 */
void MenuWidget::decrementWindowTopIndex() {
    if (isScrollingEverNecessary()) {
        unsigned long newWindowTopIndex = 0;
        if (cantScrollUp()) {
            newWindowTopIndex = buttons.size() - getWindowSize();
        } else {
            newWindowTopIndex = getWindowTopIndex() - 1;
        }

        setWindowTopIndex(static_cast<uint16_t>(newWindowTopIndex));
    }
}

/**
 * Increment the top index of the visible window of options.
 * <p>
 * If incrementing causes the top index to fall after the last option in the option list, then wrap around to the first
 * option in the option list.
 */
void MenuWidget::incrementWindowTopIndex() {
    if (isScrollingEverNecessary()) {
        long newWindowTopIndex = 0;
        if (cantScrollDown()) {
            newWindowTopIndex = 0;
        } else {
            newWindowTopIndex = getWindowTopIndex() + 1;
        }

        setWindowTopIndex(static_cast<uint16_t>(newWindowTopIndex));
    }
}

/**
 * Get the top index of the visible window of options.
 * <p>
 * This index denotes the first visible option to be displayed on the menu.
 * @return uint16_t
 */
uint16_t MenuWidget::getWindowTopIndex() const {
    return windowTopIndex;
}

/**
 * Determine if the cursor is currently outside of the visible options.
 * @return boolean
 */
bool MenuWidget::isCursorOutsideWindow() {
    return getCursorIndex() < getWindowTopIndex() || getCursorIndex() >= getWindowTopIndex() + getWindowSize();
}

/**
 * Return the actual count of visible options.
 * <p>
 * Note that the visible option count could be 0. If this is the case, then this function will return the count of all
 * the options that could be displayed.
 * @return uint16_t
 */
uint16_t MenuWidget::getWindowSize() const {
    long visibleOptionCount = getVisibleOptionCount();
    if (visibleOptionCount == 0) {
        visibleOptionCount = buttons.size();
    }
    return static_cast<uint16_t>(visibleOptionCount);
}

/**
 * Set the font size of each of the menu options.
 * @param optionFontSize
 */
void MenuWidget::setOptionFontSize(uint8_t optionFontSize) {
    this->optionFontSize = optionFontSize;
    reassignButtonFontSizes();
}

/**
 * Get the font size of each of the options in the menu.
 * @return uint8_t
 */
uint8_t MenuWidget::getOptionFontSize() const {
    return optionFontSize;
}

/**
 * Layout the visual items of the menu.
 *
 * Internally, this assigns the arrows and buttons to the appropriate grids slots for use with the box's grid arranger.
 * It also decides which arrow images to display (up/down vs left/right). Both of these decisions are made based on
 * the menu's orientation.
 */
void MenuWidget::layoutMenu() {
    alignOptions();
    reassignArrowImages();
    reassignGridSlots();
    reassignChildScaleBehaviors();
    reassignImageScaling();
}

/**
 * Connect a callback function to an option by its ID.
 * @param optionId
 * @param callback
 */
void MenuWidget::setOnOptionSelect(const std::string& optionId, const std::function<void(SDL_Event)> &callback) {
    if (idToBoxMap.find(optionId) != idToBoxMap.end()) {
        idToBoxMap[optionId]->setOnMouseUpCallback(callback);
    }
}

/**
 * Get the cursor text color.
 * @return long
 */
long MenuWidget::getOptionCursorTextColor() const {
    return optionCursorTextColor;
}

/**
 * Set the name of the animation which should be used for the up arrow.
 * @param animationName
 */
void MenuWidget::setUpArrowImage(const std::string& animationName) {
    this->upArrowAnimationName = animationName;

    if (getOrientation() == Orientation::VERTICAL) {
        precedingArrowImage->setAnimationName(animationName);
    }
}

/**
 * Set the name of the animation which should be used for the down arrow.
 * @param animationName
 */
void MenuWidget::setDownArrowImage(const std::string& animationName) {
    this->downArrowAnimationName = animationName;

    if (getOrientation() == Orientation::VERTICAL) {
        nextArrowImage->setAnimationName(animationName);
    }
}

/**
 * Set the name of the animation which should be used for the left arrow.
 * @param animationName
 */
void MenuWidget::setLeftArrowImage(const std::string& animationName) {
    this->leftArrowAnimationName = animationName;

    if (getOrientation() == Orientation::HORIZONTAL) {
        precedingArrowImage->setAnimationName(animationName);
    }
}

/**
 * Set the name of the animation which should be used for the right arrow.
 * @param animationName
 */
void MenuWidget::setRightArrowImage(const std::string& animationName) {
    this->rightArrowAnimationName = animationName;

    if (getOrientation() == Orientation::HORIZONTAL) {
        nextArrowImage->setAnimationName(animationName);
    }
}


/**
 * Set the name of the font which should be used for the menu options.
 * @param fontName
 */
void MenuWidget::setOptionFontName(const std::string& optionFontName) {
    this->optionFontName = optionFontName;
    for (const auto& button : buttons) {
        button->setFontName(getOptionFontName());
    }
}

/**
 * Get the name of the font which is used for the menu options.
 * @param std::string
 */
std::string MenuWidget::getOptionFontName() const {
    return this->optionFontName;
}

/**
 * Get the name of the up arrow image.
 * @return up arrow image name
 */
std::string MenuWidget::getUpArrowImage() const {
    return upArrowAnimationName;
}

/**
 * Get the name of the down arrow image.
 * @return down arrow image name
 */
std::string MenuWidget::getDownArrowImage() const {
    return downArrowAnimationName;
}

/**
 * Get the name of the left arrow image.
 * @return left arrow image name
 */
std::string MenuWidget::getLeftArrowImage() const {
    return leftArrowAnimationName;
}

/**
 * Get the name of the right arrow image.
 * @return right arrow image name
 */
std::string MenuWidget::getRightArrowImage() const {
    return rightArrowAnimationName;
}

/**
 * Decorate this widget by applying the properties provided.
 * @param properties
 */
void MenuWidget::decorate(const json& properties) {
    ReadOnlyComposite::decorate(properties);

    JsonAccessor accessor;
    EnumFactory enumFactory;

    setOptionBackgroundColor(colorUtility.convertColorStringToLong(
        accessor.optional<std::string>(properties, KEY_NAME_OPTION_BACKGROUND_COLOR,
            KEY_DEFAULT_OPTION_BACKGROUND_COLOR)
    ));

    setOptionFontSize(
        accessor.optional<uint8_t>(properties, KEY_NAME_OPTION_FONT_SIZE,
            KEY_DEFAULT_OPTION_FONT_SIZE)
    );

    setOptionTextColor(colorUtility.convertColorStringToLong(
        accessor.optional<std::string>(properties, KEY_NAME_OPTION_TEXT_COLOR,
            KEY_DEFAULT_OPTION_TEXT_COLOR)
    ));

    setOptionCursorTextColor(colorUtility.convertColorStringToLong(
        accessor.optional<std::string>(properties, KEY_NAME_OPTION_CURSOR_TEXT_COLOR,
            KEY_DEFAULT_OPTION_CURSOR_TEXT_COLOR)
    ));

    setOptionAlignment(enumFactory.buildHorizontalAlignment(
        accessor.optional<std::string>(properties, KEY_NAME_OPTION_ALIGNMENT, KEY_DEFAULT_OPTION_ALIGNMENT)
    ));

    setOptionFontName(
        accessor.require<std::string>(properties, KEY_NAME_OPTION_FONT_NAME)
    );

    setOptionScaleFont(
        accessor.optional<bool>(properties, KEY_NAME_OPTION_SCALE_FONT, KEY_DEFAULT_OPTION_SCALE_FONT)
    );

    setOptionScaleIcon(
        accessor.optional<bool>(properties, KEY_NAME_OPTION_SCALE_ICON, KEY_DEFAULT_OPTION_SCALE_ICON)
    );

    setVisibleOptionCount(
        accessor.require<int>(properties, KEY_NAME_VISIBLE_OPTION_COUNT)
    );

    setUpArrowImage(
        accessor.optional<std::string>(properties, KEY_NAME_UP_ARROW_IMAGE, KEY_DEFAULT_UP_ARROW_IMAGE)
    );

    setDownArrowImage(
        accessor.optional<std::string>(properties, KEY_NAME_DOWN_ARROW_IMAGE,
            KEY_DEFAULT_DOWN_ARROW_IMAGE)
    );

    setLeftArrowImage(
        accessor.optional<std::string>(properties, KEY_NAME_LEFT_ARROW_IMAGE, KEY_DEFAULT_LEFT_ARROW_IMAGE)
    );

    setRightArrowImage(
        accessor.optional<std::string>(properties, KEY_NAME_RIGHT_ARROW_IMAGE,
            KEY_DEFAULT_RIGHT_ARROW_IMAGE)
    );

    setOrientation(enumFactory.buildOrientation(
        accessor.optional<std::string>(properties, KEY_NAME_ORIENTATION, KEY_DEFAULT_ORIENTATION)
    ));

    auto options = accessor.require<json>(properties, KEY_NAME_OPTIONS);

    for (const json& option : options) {
        auto id = accessor.require<std::string>(option, KEY_NAME_OPTIONS_ID);
        auto text = accessor.require<std::string>(option, KEY_NAME_OPTIONS_TEXT);
        auto iconName = accessor.optional<std::string>(option, KEY_NAME_OPTIONS_ICON_NAME,
                KEY_DEFAULT_OPTIONS_ICON_NAME);
        addOptionWithIcon(id, text, iconName);
    }
}

/**
 * Get the horizontal alignment of the menu options.
 * @return horizontal alignment of the menu options
 */
HorizontalAlignment MenuWidget::getOptionAlignment() const {
    return optionAlignment;
}

/**
 * Set the horizontal alignment of the menu options.
 * @param optionAlignment horizontal alignment of the menu options
 */
void MenuWidget::setOptionAlignment(HorizontalAlignment optionAlignment) {
    this->optionAlignment = optionAlignment;
    alignOptions();
}

void MenuWidget::alignOptions() {
    bool showImage = true;
    HorizontalAlignment oppositeAlignment = HorizontalAlignment::RIGHT;
    if (getOptionAlignment() == HorizontalAlignment::RIGHT) {
        oppositeAlignment = HorizontalAlignment::LEFT;
    } else if (getOptionAlignment() == HorizontalAlignment::CENTER) {
        showImage = false;
    }

    int position = 0;
    for (const auto& option : options) {
        auto image = std::dynamic_pointer_cast<Widget>(images[position]);
        auto button = std::dynamic_pointer_cast<Widget>(buttons[position]);

        image = option->removeChild(image->getId());
        button = option->removeChild(button->getId());

        if (getOptionAlignment() == HorizontalAlignment::LEFT) {
            option->addChild(image);
            option->addChild(button);
        } else {
            option->addChild(button);
            option->addChild(image);
        }

        position++;
    }

    for (const auto& button : buttons) {
        Measure fill{100, PERCENT_POINT};
        if (showImage) {
            fill = Measure{80, PERCENT_POINT};
        }

        button->getLayout().getArrangementData().setFill(fill);
        button->setFaceAlignment(getOptionAlignment());
        button->getLayout().getExternalAlignment().setVertical(VerticalAlignment::CENTER);
        button->getLayout().getArrangementData().setHorizontalAlignment(HorizontalAlignment::LEFT);
    }

    for (const auto& image : images) {
        Measure fill{0, PERCENT_POINT};
        if (showImage) {
            fill = Measure{20, PERCENT_POINT};
        }

        image->getLayout().getArrangementData().setFill(fill);
        image->getLayout().getExternalAlignment().setHorizontal(oppositeAlignment);
        image->getLayout().getExternalAlignment().setVertical(VerticalAlignment::CENTER);
        image->getLayout().getArrangementData().setHorizontalAlignment(HorizontalAlignment::LEFT);
        image->setIsVisible(showImage);
    }

    precedingArrowImage->getLayout().getExternalAlignment().setHorizontal(getOptionAlignment());
    nextArrowImage->getLayout().getExternalAlignment().setHorizontal(getOptionAlignment());
}

/**
 * Get the orientation of the menu.
 * @return orientation of menu
 */
Orientation MenuWidget::getOrientation() const {
    return orientation;
}

/**
 * Set the orientation of the menu.
 * @param orientation orientation of menu
 */
void MenuWidget::setOrientation(Orientation orientation) {
    this->orientation = orientation;
    layoutMenu();
}

/**
 * Ensure that each of the buttons and arrows have the correct scale behaviors.
 */
void MenuWidget::reassignChildScaleBehaviors() const {
    for (const auto& button : buttons) {
        button->getLayout().setScaleBehavior(ScaleBehavior::STRETCH_BOTH);
    }

    int gridHeight = getArrangerData().getGridHeight();

    if (gridHeight > 0) {
        if (precedingArrowImage->getIntrinsicHeight() > getContentHeight() / gridHeight) {
            precedingArrowImage->getLayout().setScaleBehavior(ScaleBehavior::SCALE);
            precedingArrowImage->setStretch(true);
        } else {
            precedingArrowImage->getLayout().setScaleBehavior(ScaleBehavior::SCALE_NONE);
            precedingArrowImage->setStretch(false);
        }

        if (nextArrowImage->getIntrinsicHeight() > getContentHeight() / gridHeight) {
            nextArrowImage->getLayout().setScaleBehavior(ScaleBehavior::SCALE);
            nextArrowImage->setStretch(true);
        } else {
            nextArrowImage->getLayout().setScaleBehavior(ScaleBehavior::SCALE_NONE);
            nextArrowImage->setStretch(false);
        }
    }

}

/**
 * Is the option text to be scaled to fit the bounds of the menu?
 * @return scale the text?
 */
bool MenuWidget::isOptionScaleFont() const {
    return optionScaleFont;
}

/**
 * Set if the option text to be scaled to fit the bounds of the menu.
 *
 * When set, the effective font size of the option with the smallest font will be used for all the options. Does not
 * replace the set fixed font size, but does override it.
 * @param optionScaleFont whether to scale or not
 */
void MenuWidget::setOptionScaleFont(bool optionScaleFont) {
    this->optionScaleFont = optionScaleFont;
    reassignButtonFontSizes();
}

/**
 * Are the option icons to be scaled to fit the bounds of the menu?
 * @return scale the icons?
 */
bool MenuWidget::isOptionScaleIcon() const {
    return optionScaleIcon;
}

/**
 * Set if the option icons are to be scaled to fit the bounds of the menu.
 *
 * @param optionScaleIcon whether to scale or not
 */
void MenuWidget::setOptionScaleIcon(bool optionScaleIcon) {
    this->optionScaleIcon = optionScaleIcon;
    reassignImageScaling();
}

/**
 * Determine the effective option font size for the options.
 *
 * When optionScaleFont is set: This font size is the minimum font size of the options as if each had been scaled.
 * @return effective font size. Otherwise, this is equal to optionFontSize.
 */
int MenuWidget::getOptionEffectiveFontSize() const {
    int fontSize = getOptionFontSize();
    if (optionScaleFont) {
        int minFontSize{0};
        int position = 0;
        for (const auto& option : options) {
            auto button = buttons[position];
            if (option->isVisible() && !button->getText().empty()) {
                button->setScaleFont(true);
                int buttonEffectiveFontSize = button->getEffectiveFontSize();
                button->setScaleFont(false);
                if (minFontSize == 0) {
                    minFontSize = buttonEffectiveFontSize;
                } else {
                    minFontSize = std::min(minFontSize, buttonEffectiveFontSize);
                }
            }
        }

        fontSize = minFontSize;
    }

    return fontSize;
}

/**
 * Set each option's font size equal to the fontSize property.
 */
void MenuWidget::reassignButtonFontSizes() {
    int effectiveFontSize = getOptionEffectiveFontSize();
    for (const auto& button : buttons) {
        button->setFontSize(effectiveFontSize);
        button->setScaleFont(false);
    }
}

/**
 * Set each option icon to scale or not scale according to the option icon scale property.
 */
void MenuWidget::reassignImageScaling() {
    ScaleBehavior scaleBehavior{ScaleBehavior::SCALE_NONE};
    bool stretch{false};

    if (isOptionScaleIcon()) {
        scaleBehavior = ScaleBehavior::SCALE;
        stretch = true;
    }

    for (const auto& image : images) {
        image->getLayout().setScaleBehavior(scaleBehavior);
        image->setStretch(stretch);
    }
}
