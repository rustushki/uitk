#pragma once

#include <string>
#include "uitk/type/Unit.hpp"

class Measure {
    public:
        Measure() = default;
        Measure(double value);
        Measure(double value, Unit unit);

        double getValue() const;
        void setValue(double value);

        Unit getUnit() const;
        void setUnit(Unit unit);

        bool operator==(const Measure& that) const;
        bool operator!=(const Measure& that) const;

        void operator=(double rhs);
        void operator=(const std::string& rhs);

        Measure operator-(double rhs);
        Measure operator+(double rhs);
        Measure operator/(double rhs);
        Measure operator*(double rhs);

        void operator-=(double rhs);
        void operator+=(double rhs);
        void operator/=(double rhs);
        void operator*=(double rhs);

    private:
        double value{0};
        Unit unit{PIXEL};

        [[nodiscard]]
        bool isEqual(const Measure& that) const;
};
