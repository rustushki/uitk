#pragma once

class ArrangerData {
    public:
        [[nodiscard]]
        int getGridWidth() const;

        [[nodiscard]]
        int getGridHeight() const;

        void setGridWidth(int gridWidth);

        void setGridHeight(int gridHeight);

    private:
        int gridHeight;
        int gridWidth;
};
