#pragma once

enum PointContainmentBehavior {
    PARENT_ONLY,
    CHILDREN_ONLY,
    BOTH
};
