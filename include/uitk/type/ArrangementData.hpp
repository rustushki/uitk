#pragma once

#include "uitk/type/HorizontalAlignment.hpp"
#include "uitk/type/Measure.hpp"
#include "uitk/type/VerticalAlignment.hpp"

class ArrangementData {
    public:
        [[nodiscard]]
        int getX() const;

        [[nodiscard]]
        int getY() const;

        [[nodiscard]]
        int getWidth() const;

        [[nodiscard]]
        int getHeight() const;

        [[nodiscard]]
        int getGridX() const;

        [[nodiscard]]
        int getGridY() const;

        [[nodiscard]]
        int getMaxFill() const;

        [[nodiscard]]
        int getMinFill() const;

        [[nodiscard]]
        Measure getFill() const;

        [[nodiscard]]
        VerticalAlignment getVerticalAlignment() const;

        [[nodiscard]]
        HorizontalAlignment getHorizontalAlignment() const;

        void setX(int x);
        void setY(int y);
        void setWidth(int width);
        void setHeight(int height);
        void setGridX(int gridX);
        void setGridY(int gridY);
        void setMaxFill(int maxFill);
        void setMinFill(int minFill);
        void setFill(Measure fill);
        void setVerticalAlignment(VerticalAlignment verticalAlignment);
        void setHorizontalAlignment(HorizontalAlignment horizontalAlignment);

        bool operator==(const ArrangementData& that) const;
        bool operator!=(const ArrangementData& that) const;

    private:
        int x{0};
        int y{0};
        int width{0};
        int height{0};
        int gridX{0};
        int gridY{0};
        int maxFill{0};
        int minFill{0};
        Measure fill;
        VerticalAlignment verticalAlignment;
        HorizontalAlignment horizontalAlignment;

        [[nodiscard]]
        bool isEqual(const ArrangementData& that) const;
};
