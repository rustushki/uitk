#pragma once
#include <vector>
#include "uitk/type/SpriteSheet.hpp"

class Animation {

    public:
        Animation(const std::shared_ptr<SpriteSheet>& spriteSheet, std::vector<int> frameList, int stillsPerSecond,
                int frameWidth, int frameHeight);

        // Actions
        bool advance();
        void draw(SDL_Renderer* renderer);
        void move(long x, long y);
        void play();
        void stop();

        // Queries
        [[nodiscard]]
        bool isAnimating() const;

        // Getters
        [[nodiscard]]
        int getFrameWidth() const;

        [[nodiscard]]
        int getFrameHeight() const;

        [[nodiscard]]
        int getWidth() const;

        [[nodiscard]]
        int getHeight() const;

        // Setters
        void setWidth(int width);
        void setHeight(int height);

    private:
        SpriteSheet* sheet{nullptr};
        bool shouldAdvance{false};
        int advanceCount{0};
        int currentStill{0};
        int frameHeight{0};
        int frameWidth{0};
        int framesPerStill{0};
        int height{0};
        int sps{0};
        int width{0};
        long x{0};
        long y{0};
        std::vector<int> frameList;
};
