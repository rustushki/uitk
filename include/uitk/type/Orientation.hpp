#pragma once

enum class Orientation : int {
    HORIZONTAL = 0,
    VERTICAL = 1,
};

