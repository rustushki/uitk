#pragma once

enum class HorizontalAlignment : int {
    LEFT = 0,
    RIGHT = 1,
    CENTER = 2
};
