#pragma once

enum ScaleBehavior {
    STRETCH_VERTICAL = 0,
    STRETCH_HORIZONTAL = 1,
    STRETCH_BOTH = 2,
    SCALE = 3,
    SCALE_NONE = 4,
};
