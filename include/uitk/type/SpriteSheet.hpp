#pragma once
#include <SDL2/SDL.h>
#include <filesystem>
#include <map>

class SpriteSheet {
    public:
        explicit SpriteSheet(const std::filesystem::path& path);
        ~SpriteSheet();

        void drawStill(SDL_Renderer* renderer, SDL_Rect& source, SDL_Rect &destination);

    private:
        SDL_Surface* surface{nullptr};
        std::map<SDL_Renderer*, SDL_Texture*> rendererToTextureMap;
};
