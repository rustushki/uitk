#pragma once

#include "uitk/type/Measure.hpp"

class Padding {
    public:
        Padding() = default;
        Padding(Measure padding);
        Padding(Measure top, Measure right, Measure bottom, Measure left);

        [[nodiscard]]
        Measure getTop() const;

        [[nodiscard]]
        Measure getBottom() const;

        [[nodiscard]]
        Measure getLeft() const;

        [[nodiscard]]
        Measure getRight() const;

        bool operator==(const Padding& that) const;
        bool operator!=(const Padding& that) const;

        void setTop(Measure measure);
        void setBottom(Measure measure);
        void setLeft(Measure measure);
        void setRight(Measure measure);

    private:
        [[nodiscard]]
        bool isEqual(const Padding& that) const;

        Measure top{0};
        Measure bottom{0};
        Measure left{0};
        Measure right{0};
};
