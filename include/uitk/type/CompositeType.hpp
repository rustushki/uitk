#pragma once

enum CompositeType {
    NON_COMPOSITE,
    READ_ONLY_COMPOSITE,
    READ_WRITE_COMPOSITE
};
