#pragma once

#include "uitk/type/HorizontalAlignment.hpp"
#include "uitk/type/VerticalAlignment.hpp"

class Alignment {
    public:
        [[nodiscard]]
        HorizontalAlignment getHorizontal() const;

        [[nodiscard]]
        VerticalAlignment getVertical() const;

        void setHorizontal(HorizontalAlignment horizontal);

        void setVertical(VerticalAlignment vertical);

        bool operator==(const Alignment& that) const;
        bool operator!=(const Alignment& that) const;

    private:
        HorizontalAlignment horizontal{HorizontalAlignment::LEFT};
        VerticalAlignment vertical{VerticalAlignment::TOP};

        [[nodiscard]]
        bool isEqual(const Alignment& that) const;
};
