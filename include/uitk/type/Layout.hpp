#pragma once

#include "uitk/type/Alignment.hpp"
#include "uitk/type/ArrangementData.hpp"
#include "uitk/type/Padding.hpp"
#include "uitk/type/ScaleBehavior.hpp"

class Layout {
    public:
        [[nodiscard]]
        Padding& getPadding();

        [[nodiscard]]
        const Padding& getPadding() const;

        [[nodiscard]]
        ScaleBehavior& getScaleBehavior();

        [[nodiscard]]
        const ScaleBehavior& getScaleBehavior() const;

        [[nodiscard]]
        Alignment& getExternalAlignment();

        [[nodiscard]]
        const Alignment& getExternalAlignment() const;

        [[nodiscard]]
        ArrangementData& getArrangementData();

        [[nodiscard]]
        const ArrangementData& getArrangementData() const;

        bool operator==(const Layout& that) const;
        bool operator!=(const Layout& that) const;

        void setPadding(Padding padding);
        void setScaleBehavior(ScaleBehavior scaleBehavior);
        void setExternalAlignment(Alignment externalAlignment);
        void setArrangementData(ArrangementData arrangementData);

    private:
        [[nodiscard]]
        bool isEqual(const Layout& that) const;

        Alignment externalAlignment;
        ArrangementData arrangementData;
        Padding padding;
        ScaleBehavior scaleBehavior{ScaleBehavior::SCALE_NONE};
};
