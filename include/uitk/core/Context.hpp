#pragma once
#include <filesystem>
class AnimationFactory;
class SpriteSheetRegistry;
class PathUtility;
class WidgetFactory;

class Context {
    public:
        explicit Context(const std::filesystem::path& resourcesDirectory);

        std::shared_ptr<PathUtility> getPathUtility();
        std::shared_ptr<AnimationFactory> getAnimationFactory();
        std::shared_ptr<SpriteSheetRegistry> getSpriteSheetRegistry();
        std::shared_ptr<WidgetFactory> getWidgetFactory();

    private:
        void setWidgetFactory(const std::shared_ptr<WidgetFactory>& widgetFactory);

        std::shared_ptr<AnimationFactory> animationFactory;
        std::shared_ptr<SpriteSheetRegistry> spriteSheetRegistry;
        std::shared_ptr<PathUtility> pathUtility;
        std::shared_ptr<WidgetFactory> widgetFactory;

        friend class Display;
};
