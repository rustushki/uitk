#pragma once
#include <set>
#include "uitk/utility/WidgetTree.hpp"
#include "uitk/utility/factory/WidgetFactory.hpp"

class Display {
    public:
        Display(std::shared_ptr<Context> context, const std::string& windowTitle);
        ~Display();

        // Actions
        bool present();
        void load(const std::string& viewId);

        // Queries
        [[nodiscard]]
        std::set<std::shared_ptr<Widget>> getWidgetsUnderCoordinate(uint32_t x, uint32_t y) const;

        [[nodiscard]]
        std::shared_ptr<Widget> getWidgetById(const std::string& id) const;

        // Getters
        [[nodiscard]]
        uint16_t getHeight() const;

        [[nodiscard]]
        uint16_t getWidth() const;

        [[nodiscard]]
        std::string getFocus() const;

        // Setters
        void setFocus(std::string id);
        void setOnFrameCallback(const std::function<bool()> &onFrameCallback);

    private:
        void setContext(std::shared_ptr<Context> context);
        void initializeDependencies() const;
        void reset();
        void draw() const;
        void advanceAnimations();
        bool processInput();

        SDL_Window* createWindow(const std::string& windowTitle);
        SDL_Renderer* createRenderer(SDL_Window* window);

        SDL_Renderer* renderer;
        SDL_Window* window;
        std::function<bool()> onFrameCallback{nullptr};
        std::shared_ptr<Context> context;
        std::shared_ptr<WidgetTree> widgetTree;
        std::string focusedWidgetId;
        std::set<std::string> hoverWidgetIds;

        const std::string KEY_NAME_ROOT_WIDGET{"rootWidget"};
        const std::string KEY_NAME_DEFAULT_FOCUS_ID{"defaultFocusId"};

        const std::string KEY_DEFAULT_DEFAULT_FOCUS_ID;
};
