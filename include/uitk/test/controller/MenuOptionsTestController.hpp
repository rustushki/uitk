#pragma once

#include "uitk/test/controller/TestController.hpp"

class MenuOptionsTestController : public TestController {
    public:
        MenuOptionsTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};
