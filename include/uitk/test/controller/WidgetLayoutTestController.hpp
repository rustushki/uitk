#pragma once

#include <string>
#include "uitk/test/controller/TestController.hpp"

class WidgetLayoutTestController : public TestController {
    public:
        WidgetLayoutTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;

    private:
        const std::string VIEW_ID{"testWidgetLayoutView"};
        const std::string WIDGET_ID_BOX{"box"};
        const std::string PROPERTY_VALUE_FONT_NAME{"linux_libertine_regular.ttf"};
        const unsigned int PROPERTY_VALUE_BACKGROUND_COLOR{0xFF888888};
        const unsigned int PROPERTY_VALUE_TEXT_COLOR{0xFFFFFFFF};
        const unsigned int PROPERTY_VALUE_OPTION_TEXT_COLOR{0xFF000000};
        const unsigned int PROPERTY_VALUE_FONT_SIZE{12};
        const bool PROPERTY_VALUE_SCALE_STRETCH{false};
        const unsigned int LAYOUT_VALUE_PADDING{15};

        static const int WIDGET_TYPE_BOX{0};
        static const int WIDGET_TYPE_RECTANGLE{1};
        static const int WIDGET_TYPE_IMAGE{2};
        static const int WIDGET_TYPE_LABEL{3};
        static const int WIDGET_TYPE_BUTTON{4};
        static const int WIDGET_TYPE_MENU{5};
        static const int WIDGET_TYPE_COUNT{6};

        static const int ROW_SCALE_BEHAVIOR_STRETCH_HORIZONTAL{0};
        static const int ROW_SCALE_BEHAVIOR_STRETCH_VERTICAL{1};
        static const int ROW_SCALE_BEHAVIOR_STRETCH_BOTH{2};
        static const int ROW_SCALE_BEHAVIOR_SCALE{3};
        static const int ROW_SCALE_BEHAVIOR_SCALE_NONE{4};
        static const int ROW_SCALE_BEHAVIOR_COUNT{5};

        static const int COL_EXTERNAL_ALIGNMENT_0{0};
        static const int COL_EXTERNAL_ALIGNMENT_1{1};
        static const int COL_EXTERNAL_ALIGNMENT_2{2};
        static const int COL_EXTERNAL_ALIGNMENT_3{3};
        static const int COL_EXTERNAL_ALIGNMENT_4{4};
        static const int COL_EXTERNAL_ALIGNMENT_5{5};
        static const int COL_EXTERNAL_ALIGNMENT_COUNT{6};

        int currentWidgetType{0};
        std::shared_ptr<Context> context;

        [[nodiscard]]
        std::shared_ptr<Widget> buildWidget(int widget, const std::string& id) const;

        void applyLayout(const std::shared_ptr<Widget>& widget, int slotX, int slotY) const;

        void createWidgetTestCases(SDL_Event event);

        void adjustPadding(Padding& padding, int amount);

        [[nodiscard]]
        std::shared_ptr<Widget> buildBox() const;

        [[nodiscard]]
        std::shared_ptr<Widget> buildRectangle() const;

        [[nodiscard]]
        std::shared_ptr<Widget> buildImage() const;

        [[nodiscard]]
        std::shared_ptr<Widget> buildLabel() const;

        [[nodiscard]]
        std::shared_ptr<Widget> buildButton() const;

        [[nodiscard]]
        std::shared_ptr<Widget> buildMenu() const;

};
