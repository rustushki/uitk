#pragma once

#include "uitk/test/controller/TestController.hpp"

class StackingArrangerTestController : public TestController {
    public:
        StackingArrangerTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};
