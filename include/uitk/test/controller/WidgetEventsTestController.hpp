#pragma once

#include "uitk/test/controller/TestController.hpp"

class WidgetEventsTestController : public TestController {
    public:
        WidgetEventsTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
    private:
        std::shared_ptr<BoxWidget> box;
        std::shared_ptr<BoxWidget> boxForLabel;
        std::shared_ptr<LabelWidget> hoveredIdsLabel;
        std::shared_ptr<RectangleWidget> rectangle;
        std::shared_ptr<LabelWidget> pointContainmentBehaviorLabel;
        std::set<std::string> hoveredIds;

        void addHoverId(const std::shared_ptr<Widget>& widget);
        void deleteHoverId(const std::shared_ptr<Widget>& widget);
        void updateHoveredIdsLabelText();
        void updatePointContainmentBehaviorLabelText();
        PointContainmentBehavior cyclePointContainmentBehavior(PointContainmentBehavior current);
        std::string convertBehaviorToString(PointContainmentBehavior pointContainmentBehavior);
};
