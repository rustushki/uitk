#pragma once

#include "uitk/test/controller/TestController.hpp"

class ColorTestController : public TestController {
    public:
        ColorTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;

    private:
        [[nodiscard]]
        int getNextChannel(int currentChannel) const;

        [[nodiscard]]
        std::string buildLabelTextFromChannel(int currentChannel) const;

        int currentChannel{ColorUtility::COLOR_CHANNEL_ALPHA};
        ColorUtility colorUtility;
};
