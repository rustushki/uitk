#pragma once

#include <memory>
#include "uitk/core/Display.hpp"
#include "uitk/test/controller/Controller.hpp"

class TestController : public Controller {
    public:
        TestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        const std::shared_ptr<Display> getDisplay() const;
        const std::shared_ptr<Context> getContext() const;

    private:
        std::shared_ptr<Display> display;
        std::shared_ptr<Context> context;
};

