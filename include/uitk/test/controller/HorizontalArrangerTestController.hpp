#pragma once

#include "uitk/test/controller/TestController.hpp"

class HorizontalArrangerTestController : public TestController {
    public:
        HorizontalArrangerTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};
