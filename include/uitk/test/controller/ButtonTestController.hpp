#pragma once

#include "uitk/test/controller/TestController.hpp"

class ButtonTestController : public TestController {
    public:
        ButtonTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};

