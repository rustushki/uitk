#pragma once

class Controller {
    public:
        virtual void execute() = 0;
};
