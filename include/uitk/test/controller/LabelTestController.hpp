#pragma once

#include "uitk/test/controller/TestController.hpp"

class LabelTestController : public TestController {
    public:
        LabelTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};
