#pragma once

#include "uitk/test/controller/TestController.hpp"

class GridArrangerTestController : public TestController {
    public:
        GridArrangerTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};
