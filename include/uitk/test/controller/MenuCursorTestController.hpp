#pragma once

#include "uitk/test/controller/TestController.hpp"

class MenuCursorTestController : public TestController {
    public:
        MenuCursorTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};
