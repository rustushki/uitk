#pragma once

#include "uitk/test/controller/TestController.hpp"

class AbsoluteArrangerTestController : public TestController {
    public:
        AbsoluteArrangerTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};

