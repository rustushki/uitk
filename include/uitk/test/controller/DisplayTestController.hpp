#pragma once

#include "uitk/test/controller/TestController.hpp"

class DisplayTestController : public TestController {
    public:
        DisplayTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};

