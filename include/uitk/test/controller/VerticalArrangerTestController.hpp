#pragma once

#include "uitk/test/controller/TestController.hpp"

class VerticalArrangerTestController : public TestController {
    public:
        VerticalArrangerTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};
