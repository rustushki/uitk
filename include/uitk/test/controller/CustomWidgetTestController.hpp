#pragma once

#include "uitk/test/controller/TestController.hpp"

class CustomWidgetTestController : public TestController {
    public:
        CustomWidgetTestController(std::shared_ptr<Display> display, std::shared_ptr<Context> context);
        void execute() override;
};

class MyCustomLabelWidget : public LabelWidget {
    public:
        MyCustomLabelWidget(std::shared_ptr<Context> context);

        void draw(SDL_Renderer* renderer) override;
};
