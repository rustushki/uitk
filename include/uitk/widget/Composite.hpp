#pragma once

#include "uitk/type/ArrangerData.hpp"
#include "uitk/widget/BackgroundColorWidget.hpp"
#include "uitk/type/PointContainmentBehavior.hpp"

class Arranger;

class Composite : public BackgroundColorWidget {
    public:
        explicit Composite(std::shared_ptr<Context> context);

        // Actions
        void draw(SDL_Renderer* renderer) override;
        void decorate(const json& properties) override;
        void advanceAnimations() override;
        void applyArrangedSpace(SDL_Rect arrangedSpace) override;

        // Queries
        [[nodiscard]]
        std::shared_ptr<const Widget> getDescendantById(const std::string& id);

        [[nodiscard]]
        std::set<std::shared_ptr<const Widget>> getIntersectingDescendants(uint32_t x, uint32_t y);

        [[nodiscard]]
        std::shared_ptr<const Widget> getChildById(const std::string& id) const;

        // Getters
        [[nodiscard]]
        const std::vector<std::shared_ptr<const Widget>> getChildren() const;

        [[nodiscard]]
        std::string getArrangerType() const;

        [[nodiscard]]
        ArrangerData getArrangerData() const;

        [[nodiscard]]
        std::shared_ptr<const Arranger> getArranger() const;

        [[nodiscard]]
        double getIntrinsicWidthPercent() const;

        [[nodiscard]]
        double getIntrinsicHeightPercent() const;

        [[nodiscard]]
        int getIntrinsicWidth() const override;

        [[nodiscard]]
        int getIntrinsicHeight() const override;

        [[nodiscard]]
        long getDebugBorderColor();

        [[nodiscard]]
        PointContainmentBehavior getPointContainmentBehavior();

        [[nodiscard]]
        bool isDebugBorderVisible();

        // Setters
        void setIntrinsicWidthPercent(double instrinsicWidthPercent);
        void setIntrinsicHeightPercent(double instrinsicHeightPercent);
        void setDebugBorderColor(long debugBorderColor);
        void setDebugBorderVisible(bool debugBorderVisible);

    protected:
        // Actions
        void addChildHelper(const std::shared_ptr<Widget>& newChild);

        [[nodiscard]]
        std::shared_ptr<Widget> removeChildHelper(const std::string& idToRemove);

        // Queries
        [[nodiscard]]
        const std::vector<std::shared_ptr<Widget>>& getChildrenHelper() const;

        [[nodiscard]]
        std::shared_ptr<Widget> getChildByIdHelper(const std::string& idToFind) const;

        [[nodiscard]]
        std::set<std::shared_ptr<Widget>> getIntersectingDescendantsHelper(uint32_t x, uint32_t y);

        [[nodiscard]]
        std::shared_ptr<Widget> getDescendantByIdHelper(const std::string& idToFind);

        // Getters
        [[nodiscard]]
        ArrangerData& getArrangerDataHelper();

        // Setters
        void setArrangerTypeHelper(const std::string& arrangerType);

        void setArrangerDataHelper(ArrangerData arrangerData);

        void setPointContainmentBehaviorHelper(PointContainmentBehavior pointContainmentBehavior);

    private:
        const std::string ERROR_ARRANGER_NOT_CONFIGURED{"Box requires arranger in order to layout child widgets"};

        const std::string KEY_NAME_INTRINSIC_WIDTH_PERCENT{"intrinsicWidthPercent"};
        const std::string KEY_NAME_INTRINSIC_HEIGHT_PERCENT{"intrinsicHeightPercent"};

        const double KEY_DEFAULT_INTRINSIC_WIDTH_PERCENT{1.0};
        const double KEY_DEFAULT_INTRINSIC_HEIGHT_PERCENT{1.0};

        std::vector<std::shared_ptr<Widget>> children;
        ArrangerData arrangerData;
        std::shared_ptr<Arranger> arranger{nullptr};
        double intrinsicWidthPercent{KEY_DEFAULT_INTRINSIC_WIDTH_PERCENT};
        double intrinsicHeightPercent{KEY_DEFAULT_INTRINSIC_HEIGHT_PERCENT};
        bool debugBorderVisible{false};
        long debugBorderColor{0xFFFFFFFF};
        PointContainmentBehavior pointContainmentBehavior{BOTH};

        [[nodiscard]]
        bool checkIfComposite(const std::shared_ptr<Widget>& widget) const;

        void drawDebugBorder(SDL_Renderer* renderer, SDL_Rect arrangedSpace);

        friend class WidgetFactory;
};
