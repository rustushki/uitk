#pragma once
#include "uitk/core/Context.hpp"
#include "uitk/type/Animation.hpp"
#include "uitk/utility/JsonAccessor.hpp"
#include "uitk/utility/PixelConverter.hpp"
#include "uitk/utility/factory/EnumFactory.hpp"
#include "uitk/widget/Widget.hpp"

class BaseWidget : public Widget {
    public:
        explicit BaseWidget(std::shared_ptr<Context> context);

        // Actions
        void decorate(const json& properties) override;
        void applyArrangedSpace(SDL_Rect arrangedSpace) override;
        void show() override;
        void hide() override;
        void advanceAnimations() override;

        // Queries
        [[nodiscard]]
        bool containsPoint(int x, int y) const override;

        [[nodiscard]]
        bool isArrangedSpaceChanged() const override;

        [[nodiscard]]
        bool isLayoutChanged() const override;

        [[nodiscard]]
        int getArrangedSpaceWidth() const override;

        [[nodiscard]]
        int getArrangedSpaceHeight() const override;

        [[nodiscard]]
        int getArrangedSpaceX() const override;

        [[nodiscard]]
        int getArrangedSpaceY() const override;

        [[nodiscard]]
        SDL_Rect getArrangedSpace() const override;

        [[nodiscard]]
        SDL_Rect getContentSpace() const override;

        // Event Callbacks
        void onMouseUp(SDL_Event event) override;
        void onMouseDown(SDL_Event event) override;
        void onMouseHover(SDL_Event event) override;
        void onMouseHoverOut(SDL_Event event) override;
        void onKeyUp(SDL_Event event) override;
        void onKeyDown(SDL_Event event) override;

        // Event Callback Setters
        void setOnMouseUpCallback(const std::function<void(SDL_Event)> &newCallBack) final;
        void setOnMouseDownCallback(const std::function<void(SDL_Event)> &newCallBack) final;
        void setOnMouseHoverCallback(const std::function<void(SDL_Event)> &newCallBack) final;
        void setOnMouseHoverOutCallback(const std::function<void(SDL_Event)> &newCallBack) final;
        void setOnKeyUpCallback(const std::function<void(SDL_Event)> &newCallBack) final;
        void setOnKeyDownCallback(const std::function<void(SDL_Event)> &newCallBack) final;

        // Getters
        [[nodiscard]]
        std::string getId() const override;

        [[nodiscard]]
        bool isVisible() const override;

        [[nodiscard]]
        std::shared_ptr<Context> getContext() const override;

        [[nodiscard]]
        Layout& getLayout() override;

        [[nodiscard]]
        const Layout& getLayout() const override;

        [[nodiscard]]
        int getContentWidth() const override;

        [[nodiscard]]
        int getContentHeight() const override;

        [[nodiscard]]
        int getContentX() const override;

        [[nodiscard]]
        int getContentY() const override;


        // Setters
        void setId(std::string id) override;
        void setIsVisible(bool isVisible) override;
        void setLayout(Layout layout) override;

    protected:
        void addAnimation(std::unique_ptr<Animation> animation);

        void logWarning(const std::string& message) const;

    private:
        std::vector<std::unique_ptr<Animation>>& getAnimations();

        [[nodiscard]]
        double getMaximumScaleForArrangedSpace(int baseWidth, int baseHeight) const;

        EnumFactory enumFactory;
        JsonAccessor accessor;
        Layout layout;
        Layout oldLayout;
        PixelConverter pixelConverter;
        SDL_Rect arrangedSpace{0, 0, 0, 0};
        bool visible;
        bool layoutChanged{true};
        bool arrangedSpaceChanged{true};
        std::function <void (SDL_Event)> onKeyDownCallBack = [] (SDL_Event event) {};
        std::function <void (SDL_Event)> onKeyUpCallBack = [] (SDL_Event event) {};
        std::function <void (SDL_Event)> onMouseDownCallBack = [] (SDL_Event event) {};
        std::function <void (SDL_Event)> onMouseHoverCallBack = [] (SDL_Event event) {};
        std::function <void (SDL_Event)> onMouseHoverOutCallBack = [] (SDL_Event event) {};
        std::function <void (SDL_Event)> onMouseUpCallBack = [] (SDL_Event event) {};
        std::shared_ptr<Context> context{nullptr};
        std::string id;
        std::vector<std::unique_ptr<Animation>> animations;

        const std::string KEY_NAME_VISIBLE{"visible"};

        const bool KEY_DEFAULT_VISIBLE{true};

        const std::string UNIQUE_ID_EXPECTED{"WidgetFactory - Unique Widget ID expected on build (id = "};
};
