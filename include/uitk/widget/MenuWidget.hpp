#pragma once
#include <map>
#include "uitk/core/Context.hpp"
#include "uitk/widget/BoxWidget.hpp"
#include "uitk/widget/ButtonWidget.hpp"
#include "uitk/widget/ImageWidget.hpp"
#include "uitk/widget/ReadOnlyComposite.hpp"

class MenuWidget : public ReadOnlyComposite {
    public:
        explicit MenuWidget(std::shared_ptr<Context> context);

        // Actions
        void decorate(const json& properties) override;
        void draw(SDL_Renderer* renderer) override;
        void incrementCursor();
        void decrementCursor();
        void addOption(const std::string& optionId, const std::string& optionText);
        void addOptionWithIcon(const std::string& optionId, const std::string& optionText,
                const std::string& optionIconName);

        // Queries

        // Event Callbacks
        void onMouseHover(SDL_Event event) override;
        void onKeyUp(SDL_Event event) override;

        // Event Callback Setters
        void setOnOptionSelect(const std::string& optionId, const std::function<void(SDL_Event)> &callback);

        // Getters
        [[nodiscard]]
        uint8_t getOptionFontSize() const;

        [[nodiscard]]
        uint16_t getVisibleOptionCount() const;

        [[nodiscard]]
        long getOptionBackgroundColor() const;

        [[nodiscard]]
        long getOptionTextColor() const;

        [[nodiscard]]
        long getOptionCursorTextColor() const;

        [[nodiscard]]
        HorizontalAlignment getOptionAlignment() const;

        [[nodiscard]]
        bool isOptionScaleFont() const;

        [[nodiscard]]
        bool isOptionScaleIcon() const;

        [[nodiscard]]
        Orientation getOrientation() const;

        [[nodiscard]]
        uint16_t getCursorIndex() const;

        [[nodiscard]]
        std::string getOptionFontName() const;

        [[nodiscard]]
        std::string getUpArrowImage() const;

        [[nodiscard]]
        std::string getDownArrowImage() const;

        [[nodiscard]]
        std::string getLeftArrowImage() const;

        [[nodiscard]]
        std::string getRightArrowImage() const;

        [[nodiscard]]
        int getOptionEffectiveFontSize() const;

        // Setters
        void setOptionFontSize(uint8_t optionFontSize);
        void setVisibleOptionCount(uint16_t count);
        void setOptionBackgroundColor(long color);
        void setOptionTextColor(long color);
        void setOptionCursorTextColor(long color);
        void setOptionAlignment(HorizontalAlignment optionAlignment);
        void setOrientation(Orientation orientation);
        void setCursorIndex(uint16_t index);
        void setUpArrowImage(const std::string& animationName);
        void setDownArrowImage(const std::string& animationName);
        void setLeftArrowImage(const std::string& animationName);
        void setRightArrowImage(const std::string& animationName);
        void setOptionFontName(const std::string& fontName);
        void setOptionScaleFont(bool optionScaleFont);
        void setOptionScaleIcon(bool optionScaleIcon);

    private:
        [[nodiscard]]
        uint16_t getWindowSize() const;

        [[nodiscard]]
        uint16_t getWindowTopIndex() const;

        [[nodiscard]]
        std::shared_ptr<ImageWidget> createPrecedingArrow();

        [[nodiscard]]
        std::shared_ptr<ImageWidget> createNextArrow();

        [[nodiscard]]
        bool cantScrollDown() const;

        [[nodiscard]]
        bool cantScrollUp() const;

        [[nodiscard]]
        bool isScrollingEverNecessary() const;

        [[nodiscard]]
        bool isCursorOutsideWindow();

        void setWindowTopIndex(uint16_t index);
        void layoutMenu();
        void reassignArrowImages();
        void reassignGridSlots();
        void reassignChildScaleBehaviors() const;
        void reassignButtonFontSizes();
        void reassignImageScaling();
        void incrementWindowTopIndex();
        void decrementWindowTopIndex();
        void assignGridSlot(const std::shared_ptr<Widget>& widget, int position) const;
        void alignOptions();

        const std::string KEY_NAME_OPTION_BACKGROUND_COLOR{"optionBackgroundColor"};
        const std::string KEY_NAME_OPTION_TEXT_COLOR{"optionTextColor"};
        const std::string KEY_NAME_OPTION_CURSOR_TEXT_COLOR{"optionCursorTextColor"};
        const std::string KEY_NAME_OPTION_ALIGNMENT{"optionAlignment"};
        const std::string KEY_NAME_OPTION_FONT_NAME{"optionFontName"};
        const std::string KEY_NAME_OPTION_SCALE_FONT{"optionScaleFont"};
        const std::string KEY_NAME_OPTION_SCALE_ICON{"optionScaleIcon"};
        const std::string KEY_NAME_VISIBLE_OPTION_COUNT{"visibleOptionCount"};
        const std::string KEY_NAME_UP_ARROW_IMAGE{"upArrowImage"};
        const std::string KEY_NAME_DOWN_ARROW_IMAGE{"downArrowImage"};
        const std::string KEY_NAME_LEFT_ARROW_IMAGE{"leftArrowImage"};
        const std::string KEY_NAME_RIGHT_ARROW_IMAGE{"rightArrowImage"};
        const std::string KEY_NAME_OPTIONS{"options"};
        const std::string KEY_NAME_OPTIONS_ID{"id"};
        const std::string KEY_NAME_OPTIONS_TEXT{"text"};
        const std::string KEY_NAME_OPTIONS_ICON_NAME{"iconName"};
        const std::string KEY_NAME_ORIENTATION{"orientation"};
        const std::string KEY_NAME_OPTION_FONT_SIZE{"optionFontSize"};

        const std::string KEY_DEFAULT_OPTION_BACKGROUND_COLOR{"0x000000"};
        const std::string KEY_DEFAULT_OPTION_TEXT_COLOR{"0xFFFFFF"};
        const std::string KEY_DEFAULT_OPTION_CURSOR_TEXT_COLOR{"0xBBBBBB"};
        const std::string KEY_DEFAULT_OPTION_ALIGNMENT{"center"};
        const bool KEY_DEFAULT_OPTION_SCALE_FONT{false};
        const bool KEY_DEFAULT_OPTION_SCALE_ICON{false};
        const std::string KEY_DEFAULT_UP_ARROW_IMAGE;
        const std::string KEY_DEFAULT_DOWN_ARROW_IMAGE;
        const std::string KEY_DEFAULT_LEFT_ARROW_IMAGE;
        const std::string KEY_DEFAULT_RIGHT_ARROW_IMAGE;
        const std::string KEY_DEFAULT_OPTIONS_ICON_NAME;
        const std::string KEY_DEFAULT_ORIENTATION{"vertical"};
        const uint8_t KEY_DEFAULT_OPTION_FONT_SIZE{0};

        ColorUtility colorUtility;
        uint16_t cursorIndex{0};
        uint16_t visibleOptionCount{0};
        uint16_t windowTopIndex{0};
        long optionBackgroundColor{0};
        long optionCursorTextColor{0};
        long optionTextColor{0};
        uint8_t optionFontSize{KEY_DEFAULT_OPTION_FONT_SIZE};
        bool optionScaleFont{KEY_DEFAULT_OPTION_SCALE_FONT};
        bool optionScaleIcon{KEY_DEFAULT_OPTION_SCALE_ICON};
        HorizontalAlignment optionAlignment{HorizontalAlignment::CENTER};
        Orientation oldOrientation;
        Orientation orientation{Orientation::VERTICAL};
        std::function<void(SDL_Event)> onOptionSelectCallback;
        std::shared_ptr<ImageWidget> nextArrowImage{createNextArrow()};
        std::shared_ptr<ImageWidget> precedingArrowImage{createPrecedingArrow()};
        std::string downArrowAnimationName;
        std::string optionFontName;
        std::string leftArrowAnimationName;
        std::string rightArrowAnimationName;
        std::string upArrowAnimationName;
        std::vector<std::shared_ptr<BoxWidget>> options;
        std::vector<std::shared_ptr<ButtonWidget>> buttons;
        std::vector<std::shared_ptr<ImageWidget>> images;
        std::map<std::string, std::shared_ptr<BoxWidget>> idToBoxMap;
};
