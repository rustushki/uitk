#pragma once

#include "uitk/widget/Composite.hpp"

class ReadOnlyComposite : public Composite {
    public:
        explicit ReadOnlyComposite(std::shared_ptr<Context> context);

        // Getters
        [[nodiscard]]
        CompositeType getCompositeType() const final;
};

