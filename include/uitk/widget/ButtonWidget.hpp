#pragma once
#include "uitk/type/Measure.hpp"
#include "uitk/widget/ImageWidget.hpp"
#include "uitk/widget/LabelWidget.hpp"
#include "uitk/widget/ReadOnlyComposite.hpp"

class ButtonWidget : public ReadOnlyComposite {
    public:
        explicit ButtonWidget(std::shared_ptr<Context> context);
        ~ButtonWidget() override = default;

        // Actions
        void decorate(const json& properties) override;
        void draw(SDL_Renderer* renderer) override;

        // Queries

        // Event Callbacks
        void onMouseDown(SDL_Event event) final;
        void onMouseUp(SDL_Event event) final;
        void onMouseHoverOut(SDL_Event event) final;

        // Event Callback Setters

        // Getters
        [[nodiscard]]
        std::string getText() const;

        [[nodiscard]]
        long getTextColor() const;

        [[nodiscard]]
        std::string getIconName() const;

        [[nodiscard]]
        uint32_t getFontSize() const;

        [[nodiscard]]
        std::string getFontName() const;

        [[nodiscard]]
        HorizontalAlignment getFaceAlignment() const;

        [[nodiscard]]
        bool isScaleFont() const;

        [[nodiscard]]
        int getEffectiveFontSize() const;

        [[nodiscard]]
        bool isInvertOnMouseDown() const;

        // Setters
        void setIconName(const std::string& iconName);
        void setText(const std::string& text);
        void setTextColor(long color);
        void setBackgroundColor(long backgroundColor) override;
        void setFontName(const std::string& fontName);
        void setFontSize(uint32_t fontSize);
        void setFaceAlignment(HorizontalAlignment faceAlignment);
        void setScaleFont(bool scaleFont);
        void setInvertOnMouseDown(bool invertOnMouseDown);

    private:
        [[nodiscard]]
        std::shared_ptr<LabelWidget> buildLabel() const;

        [[nodiscard]]
        std::shared_ptr<ImageWidget> buildImage() const;

        void invertColors();
        void layout();

        const std::string KEY_NAME_ICON_NAME{"iconName"};
        const std::string KEY_NAME_TEXT{"text"};
        const std::string KEY_NAME_FONT_SIZE{"fontSize"};
        const std::string KEY_NAME_FONT_NAME{"fontName"};
        const std::string KEY_NAME_TEXT_COLOR{"textColor"};
        const std::string KEY_NAME_FACE_ALIGNMENT{"faceAlignment"};
        const std::string KEY_NAME_SCALE_FONT{"scaleFont"};
        const std::string KEY_NAME_INVERT_ON_MOUSE_DOWN{"invertOnMouseDown"};

        const std::string KEY_DEFAULT_TEXT;
        const std::string KEY_DEFAULT_TEXT_COLOR{"0xFFFFFF"};
        const int KEY_DEFAULT_FONT_SIZE{0};
        const std::string KEY_DEFAULT_ICON_NAME;
        const std::string KEY_DEFAULT_FACE_ALIGNMENT{"center"};
        const bool KEY_DEFAULT_SCALE_FONT{false};
        const bool KEY_DEFAULT_INVERT_ON_MOUSE_DOWN{true};

        const Measure LABEL_FILL{70, PERCENT_POINT};
        const Measure IMAGE_FILL{30, PERCENT_POINT};
        const Measure FULL_FILL{100, PERCENT_POINT};
        const Measure NO_FILL{0, PERCENT_POINT};
        const int DEFAULT_CHILD_PADDING = 5;

        ColorUtility colorUtility;
        HorizontalAlignment faceAlignment;
        bool isDepressed;
        std::shared_ptr<ImageWidget> imageWidget{buildImage()};
        std::shared_ptr<LabelWidget> labelWidget{buildLabel()};
        std::string iconName{""};
        int intrinsicHeight{0};
        int intrinsicWidth{0};
        bool invertOnMouseDown{KEY_DEFAULT_INVERT_ON_MOUSE_DOWN};
};
