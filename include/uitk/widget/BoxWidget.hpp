#pragma once
#include "uitk/widget/ReadWriteComposite.hpp"

class BoxWidget : public ReadWriteComposite {
    public:
        explicit BoxWidget(std::shared_ptr<Context> context);
};
