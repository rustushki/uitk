#pragma once
#include <memory>
#include "uitk/core/Context.hpp"
#include "uitk/widget/NonComposite.hpp"

class RectangleWidget : public NonComposite {
    public:
        explicit RectangleWidget(std::shared_ptr<Context> context);
        ~RectangleWidget() override = default;

        // Actions
        void decorate(const json& properties) override;

        // Queries

        // Event Callbacks

        // Event Callback Setters

        // Getters
        [[nodiscard]]
        int getIntrinsicWidth() const override;

        [[nodiscard]]
        int getIntrinsicHeight() const override;

        [[nodiscard]]
        int getRectangleWidth() const;

        [[nodiscard]]
        int getRectangleHeight() const;


        // Setters
        void setRectangleWidth(int rectangleWidth);
        void setRectangleHeight(int rectangleHeight);

    private:
        int rectangleHeight{0};
        int rectangleWidth{0};

        const std::string KEY_NAME_RECTANGLE_WIDTH{"rectangleWidth"};
        const std::string KEY_NAME_RECTANGLE_HEIGHT{"rectangleHeight"};
        const int KEY_DEFAULT_RECTANGLE_WIDTH{0};
        const int KEY_DEFAULT_RECTANGLE_HEIGHT{0};
};
