#pragma once

#include "uitk/widget/Composite.hpp"

class ReadWriteComposite : public Composite {
    public:
        explicit ReadWriteComposite(std::shared_ptr<Context> context);

        // Actions
        void addChild(const std::shared_ptr<Widget>& newChild);
        void decorate(const json& properties) override;

        [[nodiscard]]
        std::shared_ptr<Widget> removeChild(const std::string& idToRemove);

        // Queries
        [[nodiscard]]
        std::shared_ptr<Widget> getChildById(const std::string& id) const;

        [[nodiscard]]
        std::set<std::shared_ptr<Widget>> getIntersectingDescendants(uint32_t x, uint32_t y);

        [[nodiscard]]
        std::shared_ptr<Widget> getDescendantById(const std::string& id);

        // Getters
        [[nodiscard]]
        CompositeType getCompositeType() const final;

        [[nodiscard]]
        ArrangerData& getArrangerData();

        [[nodiscard]]
        const std::vector<std::shared_ptr<Widget>> getChildren() const;

        // Setters
        void setArrangerType(const std::string& arrangerType);

        void setArrangerData(ArrangerData arrangerData);

        void setPointContainmentBehavior(PointContainmentBehavior pointContainmentBehavior);

    private:
        const std::string KEY_NAME_ARRANGER_TYPE{"arrangerType"};
        const std::string KEY_NAME_ARRANGER_DATA{"arrangerData"};
        const std::string KEY_NAME_POINT_CONTAINMENT_BEHAVIOR{"pointContainmentBehavior"};

        const std::string KEY_DEFAULT_ARRANGER_TYPE{"stacking"};
        const json KEY_DEFAULT_ARRANGER_DATA{json()};
        const std::string KEY_DEFAULT_POINT_CONTAINMENT_BEHAVIOR{"both"};
};
