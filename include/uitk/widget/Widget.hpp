#pragma once
#include <set>
#include <SDL.h>
#include <nlohmann/json.hpp>
#include "uitk/core/Context.hpp"
#include "uitk/type/ArrangementData.hpp"
#include "uitk/type/CompositeType.hpp"
#include "uitk/type/Layout.hpp"
#include "uitk/type/Padding.hpp"

using namespace nlohmann;

class Animation;
class Context;

class Widget {
    public:
        virtual ~Widget() = default;

        // Actions
        virtual void decorate(const json& properties) = 0;
        virtual void draw(SDL_Renderer* renderer) = 0;
        virtual void applyArrangedSpace(SDL_Rect arrangedSpace) = 0;
        virtual void show() = 0;
        virtual void hide() = 0;
        virtual void advanceAnimations() = 0;

        // Queries
        [[nodiscard]]
        virtual CompositeType getCompositeType() const = 0;

        [[nodiscard]]
        virtual bool containsPoint(int x, int y) const = 0;

        [[nodiscard]]
        virtual bool isArrangedSpaceChanged() const = 0;

        [[nodiscard]]
        virtual bool isLayoutChanged() const = 0;

        [[nodiscard]]
        virtual int getArrangedSpaceWidth() const = 0;

        [[nodiscard]]
        virtual int getArrangedSpaceHeight() const = 0;

        [[nodiscard]]
        virtual int getArrangedSpaceX() const = 0;

        [[nodiscard]]
        virtual int getArrangedSpaceY() const = 0;

        [[nodiscard]]
        virtual SDL_Rect getArrangedSpace() const = 0;

        [[nodiscard]]
        virtual int getContentWidth() const = 0;

        [[nodiscard]]
        virtual int getContentHeight() const = 0;

        [[nodiscard]]
        virtual int getContentX() const = 0;

        [[nodiscard]]
        virtual int getContentY() const = 0;

        [[nodiscard]]
        virtual SDL_Rect getContentSpace() const = 0;

        [[nodiscard]]
        virtual int getIntrinsicWidth() const = 0;

        [[nodiscard]]
        virtual int getIntrinsicHeight() const = 0;

        // Event Callbacks
        virtual void onMouseUp(SDL_Event event) = 0;
        virtual void onMouseDown(SDL_Event event) = 0;
        virtual void onMouseHover(SDL_Event event) = 0;
        virtual void onMouseHoverOut(SDL_Event event) = 0;
        virtual void onKeyUp(SDL_Event event) = 0;
        virtual void onKeyDown(SDL_Event event) = 0;

        // Event Callback Setters
        virtual void setOnMouseUpCallback(const std::function<void(SDL_Event)> &newCallBack) = 0;
        virtual void setOnMouseDownCallback(const std::function<void(SDL_Event)> &newCallBack) = 0;
        virtual void setOnMouseHoverCallback(const std::function<void(SDL_Event)> &newCallBack) = 0;
        virtual void setOnMouseHoverOutCallback(const std::function<void(SDL_Event)> &newCallBack) = 0;
        virtual void setOnKeyUpCallback(const std::function<void(SDL_Event)> &newCallBack) = 0;
        virtual void setOnKeyDownCallback(const std::function<void(SDL_Event)> &newCallBack) = 0;

        // Getters
        [[nodiscard]]
        virtual std::string getId() const = 0;

        [[nodiscard]]
        virtual bool isVisible() const = 0;

        [[nodiscard]]
        virtual std::shared_ptr<Context> getContext() const = 0;

        [[nodiscard]]
        virtual Layout& getLayout() = 0;

        [[nodiscard]]
        virtual const Layout& getLayout() const = 0;

        // Setters
        virtual void setId(std::string id) = 0;
        virtual void setIsVisible(bool isVisible) = 0;
        virtual void setLayout(Layout layout) = 0;
};
