#pragma once

#include "uitk/widget/BackgroundColorWidget.hpp"

class NonComposite : public BackgroundColorWidget {
    public:
        explicit NonComposite(std::shared_ptr<Context> context);

        // Getters
        [[nodiscard]]
        CompositeType getCompositeType() const final;
};
