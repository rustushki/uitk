#pragma once
#include <SDL_ttf.h>
#include "uitk/type/Animation.hpp"
#include "uitk/utility/ColorUtility.hpp"
#include "uitk/widget/NonComposite.hpp"

class LabelWidget : public NonComposite {
    public:
        explicit LabelWidget(std::shared_ptr<Context> context);

        // Actions
        void decorate(const json& properties) override;
        void draw(SDL_Renderer* renderer) override;

        // Queries

        // Event Callbacks

        // Event Callback Setters

        // Getters
        [[nodiscard]]
        uint32_t getFontSize() const;

        [[nodiscard]]
        long getTextColor() const;

        [[nodiscard]]
        virtual std::string getText();

        [[nodiscard]]
        std::string getFontName() const;

        [[nodiscard]]
        int getIntrinsicWidth() const override;

        [[nodiscard]]
        int getIntrinsicHeight() const override;

        [[nodiscard]]
        bool isScaleFont() const;

        [[nodiscard]]
        int getEffectiveFontSize() const;

        // Setters
        void setFontName(const std::string& fontName);
        void setFontSize(uint32_t fontSize);
        void setText(const std::string& text);
        void setTextColor(long color);
        void setScaleFont(bool scaleFont);

    private:
        [[nodiscard]]
        TTF_Font* getFont(uint8_t size) const;

        [[nodiscard]]
        SDL_Texture* makeTextTexture(SDL_Renderer* renderer, int fontSize) const;

        [[nodiscard]]
        int chooseFontSizeToFitAxis(int axis, int size) const;

        [[nodiscard]]
        int measureAxisAtFontSize(int fontSize, int axis) const;

        ColorUtility colorUtility;
        SDL_Texture* textTexture{nullptr};
        bool textDirty{true};
        std::string fontName;
        std::string text;
        uint16_t marginHorizontal{0};
        uint16_t marginVertical{0};
        uint32_t fontSize{0};
        long textColor{0};
        bool scaleFont{false};
        int oldEffectiveFontSize{0};

        const std::string KEY_NAME_TEXT{"text"};
        const std::string KEY_NAME_FONT_SIZE{"fontSize"};
        const std::string KEY_NAME_FONT_NAME{"fontName"};
        const std::string KEY_NAME_TEXT_COLOR{"textColor"};
        const std::string KEY_NAME_SCALE_FONT{"scaleFont"};

        const std::string KEY_DEFAULT_TEXT;
        const std::string KEY_DEFAULT_TEXT_COLOR{"0xFFFFFF"};
        const int KEY_DEFAULT_FONT_SIZE{0};
        const bool KEY_DEFAULT_SCALE_FONT{false};
};
