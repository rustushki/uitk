#pragma once
#include <memory>
#include "uitk/core/Context.hpp"
#include "uitk/utility/ColorUtility.hpp"
#include "uitk/widget/BaseWidget.hpp"

class BackgroundColorWidget : public BaseWidget {
    public:
        explicit BackgroundColorWidget(std::shared_ptr<Context> context);
        ~BackgroundColorWidget() override = default;

        // Actions
        void decorate(const json& properties) override;
        void draw(SDL_Renderer* renderer) override;

        // Queries

        // Event Callbacks

        // Event Callback Setters

        // Getters
        [[nodiscard]]
        virtual long getBackgroundColor() const;

        // Setters
        virtual void setBackgroundColor(long backgroundColor);

    private:
        ColorUtility colorUtility;

        const std::string KEY_NAME_BACKGROUND_COLOR{"backgroundColor"};
        const std::string KEY_DEFAULT_BACKGROUND_COLOR{"0x000000"};

        unsigned long backgroundColor{0};
};
