#pragma once
#include "uitk/type/Animation.hpp"
#include "uitk/utility/factory/AnimationFactory.hpp"
#include "uitk/widget/NonComposite.hpp"

class ImageWidget : public NonComposite {
    public:
        explicit ImageWidget(std::shared_ptr<Context> context);

        // Actions
        void decorate(const json& properties) override;
        void draw(SDL_Renderer* renderer) override;

        // Queries

        // Event Callbacks

        // Event Callback Setters

        // Getters
        [[nodiscard]]
        std::string getAnimationName() const;

        [[nodiscard]]
        int getIntrinsicWidth() const override;

        [[nodiscard]]
        int getIntrinsicHeight() const override;

        [[nodiscard]]
        bool isStretch() const;

        // Setters
        void setAnimationName(const std::string& animationName);
        void setStretch(bool stretch);

    private:
        [[nodiscard]]
        int getHorizontalOffset() const;

        [[nodiscard]]
        int getVerticalOffset() const;

        std::string animationName;
        std::unique_ptr<Animation> animation{nullptr};
        bool stretch{false};

        const std::string WARNING_INTRINSIC_DIMENSIONS{"Intrinsic dimensions may not be set for an Image"};

        const std::string KEY_NAME_ANIMATION_NAME{"animationName"};
        const std::string KEY_NAME_STRETCH{"stretch"};

        const std::string KEY_DEFAULT_ANIMATION_NAME;
        const bool KEY_DEFAULT_STRETCH{false};
};
