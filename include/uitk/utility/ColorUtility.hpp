#pragma once

#include <string>

class ColorUtility {
    public:
        ColorUtility() = default;

        [[nodiscard]]
        int getChannel(long color, int channel) const;

        [[nodiscard]]
        long buildColorFromChannels(int alpha, int red, int green, int blue) const;

        [[nodiscard]]
        long replaceColorChannel(long color, int channel, int newValue) const;

        [[nodiscard]]
        long convertColorStringToLong(const std::string& colorString) const;

        [[nodiscard]]
        std::string convertColorLongToString(long color) const;

        [[nodiscard]]
        long invert(long color) const;

        static constexpr int COLOR_CHANNEL_BLUE{0};
        static constexpr int COLOR_CHANNEL_GREEN{1};
        static constexpr int COLOR_CHANNEL_RED{2};
        static constexpr int COLOR_CHANNEL_ALPHA{3};

        static constexpr int CHANNEL_MAX_VALUE{0xFF};
        static constexpr int CHANNEL_MIN_VALUE{0x00};
        static constexpr int TRANSPARENCY_OPAQUE{CHANNEL_MAX_VALUE};
        static constexpr int TRANSPARENCY_TRANSLUCENT{CHANNEL_MIN_VALUE};

    private:
        static constexpr int BITS_PER_BYTE{8};
        static constexpr int BASE_HEXADECIMAL{16};
};
