#pragma once
#include "uitk/widget/Widget.hpp"

class WidgetTree {
    public:
        [[nodiscard]]
        std::shared_ptr<Widget> getWidgetById(const std::string& id) const;

        [[nodiscard]]
        std::shared_ptr<const Widget> getWidgetByIdInternal(const std::string& id) const;

        [[nodiscard]]
        bool isWidgetIdTaken(const std::string& id) const;

        [[nodiscard]]
        std::string generateId() const;

        [[nodiscard]]
        std::set<std::shared_ptr<Widget>> getWidgetsUnderCoordinate(uint32_t x, uint32_t y) const;

        [[nodiscard]]
        std::set<std::shared_ptr<const Widget>> getWidgetsUnderCoordinateInternal(uint32_t x, uint32_t y) const;

    private:
        [[nodiscard]]
        std::shared_ptr<Widget> getRootWidget() const;

        void clearRootWidget();

        void setRootWidget(const std::shared_ptr<Widget>& rootWidget);

        [[nodiscard]]
        bool isRootWidgetSet() const;

        [[nodiscard]]
        bool isWidgetIdTakenAmongChildren(const std::shared_ptr<Widget>& current, const std::string& id) const;

        std::shared_ptr<Widget> rootWidget{nullptr};

        friend class Display;
};
