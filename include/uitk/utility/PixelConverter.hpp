#pragma once

#include "uitk/type/Measure.hpp"

class PixelConverter {
    public:
        PixelConverter() = default;

        [[nodiscard]]
        int convert(const Measure& measure, int relative) const;

    private:
        [[nodiscard]]
        int convertAbsolute(const Measure& measure) const;

        [[nodiscard]]
        int convertRelative(const Measure& measure, int relative) const;

};
