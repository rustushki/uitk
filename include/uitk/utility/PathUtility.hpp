#pragma once
#include <filesystem>

class PathUtility {
    public:
        explicit PathUtility(std::string resourcesPathString);

        [[nodiscard]]
        std::filesystem::path getResourcesPath() const;

        [[nodiscard]]
        std::filesystem::path getViewsPath() const;

        [[nodiscard]]
        std::filesystem::path getImagesPath() const;

        [[nodiscard]]
        std::filesystem::path getFontsPath() const;

        [[nodiscard]]
        std::filesystem::path getRegistriesPath() const;

    private:
        std::filesystem::path resourcesPath;
        std::filesystem::path viewsPath;
        std::filesystem::path imagesPath;
        std::filesystem::path fontsPath;
        std::filesystem::path registriesPath;
};
