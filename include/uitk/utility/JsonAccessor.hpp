#pragma once
#include "nlohmann/json.hpp"

using json = nlohmann::json;

class JsonAccessor {
    public:
        JsonAccessor() = default;

        /**
         * Return true if the provided name exists as a key in the provided JSON element.
         * @param element
         * @param name
         * @return bool
         */
        [[nodiscard]]
        bool isFieldPresent(const json& element, const std::string& name) const {
            return element.count(name) > 0;
        }

        /**
         * Require that the provided name exists as a key in the provided JSON element.
         * <p>
         * If it does not, throw a runtime_error. Otherwise, return the value associated with the key.
         * @param element
         * @param name
         * @return T
         */
        template<class T>
        T require(const json& element, const std::string& name) const {
            if (!isFieldPresent(element, name)) {
                throw std::runtime_error("Required field missing. name = " + name);
            }
            return get<T>(element, name);
        }

        /**
         * Lookup the value associated with the provided key name in the provided element.
         * <p>
         * If the name does not exist as a key, then return the provided default value. Otherwise, return the discovered
         * value.
         * @param element
         * @param name
         * @param defaultValue
         * @return T
         */
        template<class T>
        T optional(const json& element, const std::string& name, T defaultValue) const {
            T value = defaultValue;
            if (isFieldPresent(element, name)) {
                value = get<T>(element, name);
            }
            return value;
        }

        /**
         * Lookup the provided name in the provided element.
         * <p>
         * If the value's type does not match the provided type T, then throw a runtime_error.
         * @param element
         * @param name
         * @return T
         */
        template<class T>
        T get(const json& element, const std::string& name) const {
            try {
                return element[name].get<T>();
            } catch (const nlohmann::detail::type_error& e) {
                throw std::runtime_error("optional field has wrong type. name = " + name);
            }
        }
};
