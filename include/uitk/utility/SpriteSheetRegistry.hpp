#pragma once
#include <map>
#include "uitk/type/SpriteSheet.hpp"

class SpriteSheetRegistry {
    public:
        void registerSpriteSheet(std::string id, std::shared_ptr<SpriteSheet> spriteSheet);

        [[nodiscard]]
        std::shared_ptr<SpriteSheet> getSpriteSheet(std::string id) const;

    private:
        std::map<std::string, std::shared_ptr<SpriteSheet>> idToSpriteSheetMap;

};
