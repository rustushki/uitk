#pragma once

#include "nlohmann/json.hpp"
#include "uitk/type/Padding.hpp"
#include "uitk/utility/JsonAccessor.hpp"

using json = nlohmann::json;

class PaddingFactory {
    public:
        PaddingFactory() = default;

        [[nodiscard]]
        Padding build(const json& element) const;

    private:
        JsonAccessor accessor;

        const std::string KEY_NAME_TOP{"top"};
        const std::string KEY_NAME_BOTTOM{"bottom"};
        const std::string KEY_NAME_LEFT{"left"};
        const std::string KEY_NAME_RIGHT{"right"};

        const std::string KEY_DEFAULT_TOP{"0px"};
        const std::string KEY_DEFAULT_BOTTOM{"0px"};
        const std::string KEY_DEFAULT_LEFT{"0px"};
        const std::string KEY_DEFAULT_RIGHT{"0px"};
};
