#pragma once

#include <string>
#include "uitk/type/Unit.hpp"

class UnitFactory {
    public:
        [[nodiscard]]
        Unit build(const std::string& string) const;
};
