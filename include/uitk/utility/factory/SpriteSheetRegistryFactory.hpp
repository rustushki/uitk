#pragma once
#include <memory>
#include "uitk/utility/JsonAccessor.hpp"
#include "uitk/utility/SpriteSheetRegistry.hpp"

class SpriteSheetRegistryFactory {
    public:
        SpriteSheetRegistryFactory() = default;
        std::shared_ptr<SpriteSheetRegistry> build(const std::filesystem::path& imagesPath,
                const std::filesystem::path& registryPath);

    private:
        JsonAccessor accessor;

        const std::string FIELD_SPRITE_SHEETS{"spriteSheets"};
        const std::string FIELD_ID{"id"};
        const std::string FIELD_FILE_NAME{"fileName"};
};
