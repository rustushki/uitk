#pragma once

#include <string>
#include "uitk/type/HorizontalAlignment.hpp"
#include "uitk/type/Orientation.hpp"
#include "uitk/type/PointContainmentBehavior.hpp"
#include "uitk/type/ScaleBehavior.hpp"
#include "uitk/type/VerticalAlignment.hpp"

class EnumFactory {
    public:
        EnumFactory() = default;

        [[nodiscard]]
        VerticalAlignment buildVerticalAlignment(const std::string& source) const;

        [[nodiscard]]
        HorizontalAlignment buildHorizontalAlignment(const std::string& source) const;

        [[nodiscard]]
        Orientation buildOrientation(const std::string& source) const;

        [[nodiscard]]
        ScaleBehavior buildScaleBehavior(const std::string& source) const;

        [[nodiscard]]
        PointContainmentBehavior buildPointContainmentBehavior(const std::string& source) const;

    private:
        const std::string KEY_VALUE_HORIZONTAL_ALIGNMENT_LEFT{"left"};
        const std::string KEY_VALUE_HORIZONTAL_ALIGNMENT_CENTER{"center"};
        const std::string KEY_VALUE_HORIZONTAL_ALIGNMENT_RIGHT{"right"};

        const std::string KEY_VALUE_VERTICAL_ALIGNMENT_TOP{"top"};
        const std::string KEY_VALUE_VERTICAL_ALIGNMENT_CENTER{"center"};
        const std::string KEY_VALUE_VERTICAL_ALIGNMENT_BOTTOM{"bottom"};

        const std::string KEY_VALUE_ORIENTATION_HORIZONTAL{"horizontal"};
        const std::string KEY_VALUE_ORIENTATION_VERTICAL{"vertical"};

        const std::string KEY_VALUE_SCALE_BEHAVIOR_SCALE_NONE{"scale_none"};
        const std::string KEY_VALUE_SCALE_BEHAVIOR_SCALE{"scale"};
        const std::string KEY_VALUE_SCALE_BEHAVIOR_STRETCH_BOTH{"stretch_both"};
        const std::string KEY_VALUE_SCALE_BEHAVIOR_STRETCH_HORIZONTAL{"stretch_horizontal"};
        const std::string KEY_VALUE_SCALE_BEHAVIOR_STRETCH_VERTICAL{"stretch_vertical"};

        const std::string KEY_VALUE_POINT_CONTAINMENT_BEHAVIOR_PARENT_ONLY{"parent_only"};
        const std::string KEY_VALUE_POINT_CONTAINMENT_BEHAVIOR_CHILDREN_ONLY{"children_only"};
        const std::string KEY_VALUE_POINT_CONTAINMENT_BEHAVIOR_BOTH{"both"};
};
