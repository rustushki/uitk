#pragma once

#include <string>
#include "uitk/type/Measure.hpp"

class MeasureFactory {
    public:
        [[nodiscard]]
        Measure build(const std::string& string) const;
};
