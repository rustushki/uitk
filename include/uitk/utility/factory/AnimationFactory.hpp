#pragma once
#include <filesystem>
#include <memory>
#include "uitk/type/Animation.hpp"
#include "uitk/utility/JsonAccessor.hpp"
#include "uitk/utility/SpriteSheetRegistry.hpp"

class AnimationFactory {
    public:
        AnimationFactory(std::filesystem::path registriesPath,
                std::shared_ptr<SpriteSheetRegistry> spriteSheetRegistry);

        [[nodiscard]]
        std::unique_ptr<Animation> build(const std::string& animationName) const;

    private:
        JsonAccessor accessor;
        std::filesystem::path registriesPath;
        std::shared_ptr<SpriteSheetRegistry> spriteSheetRegistry;

        const std::string FIELD_ANIMATIONS{"animations"};
        const std::string FIELD_ID{"id"};
        const std::string FIELD_SPRITESHEET{"spriteSheet"};
        const std::string FIELD_FRAME_LIST{"frameList"};
        const std::string FIELD_STILLS_PER_SECOND{"stillsPerSecond"};
        const std::string FIELD_FRAME_WIDTH{"frameWidth"};
        const std::string FIELD_FRAME_HEIGHT{"frameHeight"};

};
