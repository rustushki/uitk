#pragma once
#include "nlohmann/json.hpp"
#include "uitk/type/ArrangerData.hpp"
#include "uitk/utility/JsonAccessor.hpp"

using json = nlohmann::json;

class ArrangerDataFactory {
    public:
        ArrangerDataFactory() = default;

        [[nodiscard]]
        ArrangerData build(const json& element) const;

    private:
        const std::string KEY_NAME_GRID_WIDTH{"gridWidth"};
        const std::string KEY_NAME_GRID_HEIGHT{"gridHeight"};

        const int KEY_DEFAULT_GRID_WIDTH{0};
        const int KEY_DEFAULT_GRID_HEIGHT{0};
};

