#pragma once
#include "nlohmann/json.hpp"
#include "uitk/type/ArrangementData.hpp"
#include "uitk/utility/JsonAccessor.hpp"
#include "uitk/utility/factory/EnumFactory.hpp"

using json = nlohmann::json;

class ArrangementDataFactory {
    public:
        ArrangementDataFactory() = default;

        [[nodiscard]]
        ArrangementData build(const json& element) const;

    private:
        JsonAccessor accessor;
        EnumFactory enumFactory;

        const std::string KEY_NAME_X{"x"};
        const std::string KEY_NAME_Y{"y"};
        const std::string KEY_NAME_WIDTH{"width"};
        const std::string KEY_NAME_HEIGHT{"height"};
        const std::string KEY_NAME_GRID_X{"gridX"};
        const std::string KEY_NAME_GRID_Y{"gridY"};
        const std::string KEY_NAME_MAX_FILL{"maxFill"};
        const std::string KEY_NAME_MIN_FILL{"minFill"};
        const std::string KEY_NAME_FILL{"fill"};
        const std::string KEY_NAME_VERTICAL_ALIGNMENT{"verticalAlignment"};
        const std::string KEY_NAME_HORIZONTAL_ALIGNMENT{"horizontalAlignment"};

        const std::string KEY_DEFAULT_VERTICAL_ALIGNMENT{"top"};
        const std::string KEY_DEFAULT_HORIZONTAL_ALIGNMENT{"left"};
        const std::string KEY_DEFAULT_FILL{"0%"};
};
