#pragma once

#include "nlohmann/json.hpp"
#include "uitk/type/Alignment.hpp"
#include "uitk/utility/factory/EnumFactory.hpp"
#include "uitk/utility/JsonAccessor.hpp"

class AlignmentFactory {
    public:
        AlignmentFactory() = default;

        [[nodiscard]]
        Alignment build(const json& element) const;

    private:
        JsonAccessor accessor;
        EnumFactory enumFactory;

        const std::string KEY_NAME_HORIZONTAL{"horizontal"};
        const std::string KEY_NAME_VERTICAL{"vertical"};

        const std::string KEY_DEFAULT_HORIZONTAL{"left"};
        const std::string KEY_DEFAULT_VERTICAL{"top"};
};
