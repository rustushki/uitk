#pragma once
#include "nlohmann/json.hpp"
#include "uitk/type/HorizontalAlignment.hpp"
#include "uitk/type/VerticalAlignment.hpp"
#include "uitk/utility/JsonAccessor.hpp"
#include "uitk/utility/WidgetTree.hpp"
#include "uitk/utility/factory/ArrangementDataFactory.hpp"
#include "uitk/widget/BoxWidget.hpp"
#include "uitk/widget/ImageWidget.hpp"
#include "uitk/widget/LabelWidget.hpp"
#include "uitk/widget/MenuWidget.hpp"
#include "uitk/widget/RectangleWidget.hpp"

class Context;
class Widget;

using json = nlohmann::json;

class WidgetFactory {
    public:
        WidgetFactory(const std::shared_ptr<Context>& context, const std::weak_ptr<WidgetTree>& widgetTree);

        [[nodiscard]]
        std::shared_ptr<Widget> build(const std::string& type) const;

        [[nodiscard]]
        std::shared_ptr<Widget> build(const std::string& type, const std::string& id) const;

        [[nodiscard]]
        std::shared_ptr<Widget> build(const json& element) const;

        [[nodiscard]]
        bool isWidgetIdTaken(const std::string& id) const;

        void registerWidgetType(const std::string& typeName, std::function<std::shared_ptr<Widget>()> builder);

        inline static const std::string TYPE_BOX{"box"};
        inline static const std::string TYPE_BUTTON{"button"};
        inline static const std::string TYPE_IMAGE{"image"};
        inline static const std::string TYPE_LABEL{"label"};
        inline static const std::string TYPE_MENU{"menu"};
        inline static const std::string TYPE_RECTANGLE{"rectangle"};

    private:
        std::map<std::string, std::function<std::shared_ptr<Widget>()> > builderMap;

        std::weak_ptr<WidgetTree> widgetTree;

        const std::string KEY_NAME_TYPE{"type"};
        const std::string KEY_NAME_CHILDREN{"children"};
        const std::string KEY_NAME_ID{"id"};
        const std::string KEY_NAME_ARRANGEMENT_DATA{"arrangementData"};
        const std::string KEY_NAME_LAYOUT{"layout"};
        const std::string KEY_NAME_PROPERTIES{"properties"};
};
