#pragma once

#include "nlohmann/json.hpp"
#include "uitk/type/Layout.hpp"
#include "uitk/utility/JsonAccessor.hpp"
#include "uitk/utility/factory/AlignmentFactory.hpp"
#include "uitk/utility/factory/ArrangementDataFactory.hpp"
#include "uitk/utility/factory/EnumFactory.hpp"
#include "uitk/utility/factory/PaddingFactory.hpp"

using json = nlohmann::json;

class LayoutFactory {
    public:
        LayoutFactory() = default;

        [[nodiscard]]
        Layout build(const json& element) const;

    private:
        JsonAccessor accessor;
        EnumFactory enumFactory;
        ArrangementDataFactory arrangementDataFactory;
        AlignmentFactory alignmentFactory;
        PaddingFactory paddingFactory;

        const std::string KEY_NAME_ARRANGEMENT_DATA{"arrangementData"};
        const std::string KEY_NAME_PADDING{"padding"};
        const std::string KEY_NAME_EXTERNAL_ALIGNMENT{"externalAlignment"};
        const std::string KEY_NAME_SCALE_BEHAVIOR{"scaleBehavior"};

        const json KEY_DEFAULT_PADDING;
        const json KEY_DEFAULT_EXTERNAL_ALIGNMENT;
        const std::string KEY_DEFAULT_SCALE_BEHAVIOR{"scale_none"};
};
