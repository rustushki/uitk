#pragma once
#include "nlohmann/json.hpp"
#include "uitk/utility/JsonAccessor.hpp"

class Arranger;

using json = nlohmann::json;

class ArrangerFactory {
    public:
        ArrangerFactory();

        [[nodiscard]]
        std::shared_ptr<Arranger> build(const std::string& type) const;

        void registerArrangerType(const std::string& typeName, std::function<std::shared_ptr<Arranger>()> builder);

    private:
        std::map<std::string, std::function<std::shared_ptr<Arranger>()> > builderMap;

        const std::string TYPE_ABSOLUTE{"absolute"};
        const std::string TYPE_GRID{"grid"};
        const std::string TYPE_HORIZONTAL{"horizontal"};
        const std::string TYPE_STACKING{"stacking"};
        const std::string TYPE_VERTICAL{"vertical"};
};
