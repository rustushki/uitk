#pragma once
#include "uitk/arranger/Arranger.hpp"

class HorizontalArranger : public Arranger {
    public:
        SDL_Rect determineChildPlacement(Composite* parent, Widget* child) const override;
        std::string getType() override;

    private:
        int getChildX(Composite* parent, const Widget* child) const;
        int getChildWidth(Composite* parent, const Widget* child) const;
        int getStartingPoint(Composite* parent, const Widget* child) const;
        int getFillPixelTotal(Composite* parent, HorizontalAlignment alignment) const;
};
