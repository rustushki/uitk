#pragma once
#include "uitk/arranger/Arranger.hpp"

class GridArranger : public Arranger {
    public:
        ~GridArranger() override = default;
        SDL_Rect determineChildPlacement(Composite* parent, Widget* child) const override;
        std::string getType() override;

    private:
        [[nodiscard]]
        int modulo(int x, int n) const;
};
