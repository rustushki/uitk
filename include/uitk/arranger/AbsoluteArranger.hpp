#pragma once
#include "uitk/arranger/Arranger.hpp"

class AbsoluteArranger : public Arranger {
    public:
        SDL_Rect determineChildPlacement(Composite* parent, Widget* child) const override;
        std::string getType() override;
};
