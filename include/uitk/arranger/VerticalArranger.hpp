#pragma once
#include "uitk/arranger/Arranger.hpp"

class VerticalArranger : public Arranger {
    public:
        SDL_Rect determineChildPlacement(Composite* parent, Widget* child) const override;
        std::string getType() override;

    private:
        int getChildY(Composite* parent, const Widget* child) const;
        int getChildHeight(Composite* parent, const Widget* child) const;
        int getStartingPoint(Composite* parent, const Widget* child) const;
        int getFillPixelTotal(Composite* parent, VerticalAlignment alignment) const;
};
