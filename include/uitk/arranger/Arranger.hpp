#pragma once
#include "uitk/widget/Widget.hpp"
#include "uitk/widget/Composite.hpp"

class Arranger {
    public:
        virtual ~Arranger() = default;
        virtual SDL_Rect determineChildPlacement(Composite* parent, Widget* child) const = 0;
        virtual std::string getType() = 0;
};
