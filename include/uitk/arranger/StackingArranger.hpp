#pragma once
#include "uitk/arranger/Arranger.hpp"

class StackingArranger : public Arranger {
    public:
        SDL_Rect determineChildPlacement(Composite* parent, Widget* child) const override;
        std::string getType() override;
};
